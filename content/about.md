---
title: "About"
draft: false
date: 2019-11-02
summary: "About this site."
---

This site was created using Hugo static site generator with the 'blank' template. 

There are two themes, light and dark. The theme you see will be matched to your operating system settings. I think the dark theme is better.

Some upcoming plans for the site.

1. Might take all the CSS stuff off and move to CSS Geek (possibly)
2. Move all the articles to posts
3. OR split them into groups: design, dev, CSS, Hugo ??
4. Interaction to header links. Bland to non-existent at present.
5. Go over articles/posts and improve them. 
   1. CSS filters needs finishing and images
   2. VS Code needs sorting out maybe splitting up
   3. Git & Github need tidying up, maybe splitting


<!-- 

My skills include:
- HTML
- CSS w a little bit of Stylus
- Photoshop
- Illustrator
- Photography
- Git
- VS Code
- CLI - basic bash

 -->
