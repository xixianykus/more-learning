---
title: "Subgrid"
date: 2019-12-29T08:28:44Z
draft: false
categories: ["css"]
tags: ["css", "playground"]
summary: "First play with subgrid which is now live in Firefox 71. Whooo!"
cssfile: subgrid
---


<div class="header">
    <h1>Subgrid</h1>
    <ul>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
    </ul>
</div>

## How it works:

In the above there is a div (<code>.header</code>) containing an <code>h1</code> (says ‘Subgrid’) and a <code>ul</code> with <code>li</code>'s containing the links.</p>

Like this:</p>

```html
<div class="header">
    <h1>Subgrid</h1>
    <ul>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
        <li><a href="#">item</a></li>
    </ul>
</div>
```

Normal <code>display: grid;</code> is applied to this .header div with 12 columns: <code>grid-template-columns: repeat(12, 1fr);</code></p>

TO USE SUBGRID on the list of links you:</p>

<ol>
    <li>Apply grid to the unordered list: <code>display: grid;</code></li>
    <li><code>grid-template-columns: subgrid;</code></li>
</ol>

```css
.header > ul {
    grid-column: -1 / -5;
    display: grid;
    grid-template-columns: subgrid;
    list-style-type: none;
    padding: 0;
}
```



## Sub-sub-grid?

Grid container is green (set to display: grid)

Grid items of above (.g-item) have dotted borders (set to display: grid).

The children of .g-item are .sub and have a blue background (set to display: grid).

The children of .sub are the h3 and p elements which also behave as grid items, sub-sub grid items.

<div class="grid">
    <div class="g-item">
        <div class="sub">
            <h3>subsub</h3>
            <p>Lorem ipsum dolor sit amet consectetur.</p>
        </div>
        <div class="sub">
            <h3>subsub</h3>
            <p>Distinctio asperiores tempora corporis!</p>
        </div>
        <div class="sub">
            <h3>subsub</h3>
            <p>Impedit saepe eaque accusamus.</p>
        </div>
    </div>
    <div class="g-item">
        <div class="sub">
            <h3>subsub</h3>
            <p>Natus incidunt um officiis est?</p>
        </div>
        <div class="sub">
            <h3>subsub</h3>
            <p>Cumque fuga aliquam ac magnam exercitationem officiis odio dolore?</p>
        </div>
        <div class="sub">
            <h3>subsub</h3>
            <p>Incidunt veritatis dolore us iure, rerum odio.</p>
        </div>
    </div>
    
</div>
