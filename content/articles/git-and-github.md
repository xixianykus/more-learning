---
title: "Git and Github"
date: 2019-10-30T05:51:03Z
draft: false
summary: "A good list of common git commands and important info for using GitHub."
tags: ["git", "github"]
categories: ["tutorial", "static sites"]
---


- [Intro to git](#intro-to-git)
- [Git](#git)
  - [.gitignore](#gitignore)
- [GitHub](#github)
  - [Setting up a new repo](#setting-up-a-new-repo)
  - [Create a new repo from the terminal](#create-a-new-repo-from-the-terminal)
- [Problems uploading and downloading](#problems-uploading-and-downloading)
- [Clone another repo](#clone-another-repo)
- [To invite someone](#to-invite-someone)
- [Join someone else's repo](#join-someone-elses-repo)
- [Line endings](#line-endings)
- [Git configuration](#git-configuration)
- [GitHub pages](#github-pages)
- [Alternatives to GitHub](#alternatives-to-github)
- [GitBash](#gitbash)
  - [Navigating](#navigating)
  - [Updating](#updating)
- [Links](#links)



## Intro to git

<iframe src="https://player.vimeo.com/video/41493906" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/41493906">Get Going (Git-SCM) &bull; Git Basics #3</a> from <a href="https://vimeo.com/github">GitHub</a> on <a href="https://vimeo.com">Vimeo</a>.</p>


## Useful git commands
1. **git help** gives general guidance *but* **git help add** gives specific help on the *add* command.
2. **git init** will create a repo in your project folder. **git init project1** will create a new folder called project1 with git repo.
3. **git status** to see what git has noticed. It won't have done anything with the files yet, just noticed them.
4. **git add _filename.html_** will add the file to the *staging environment* aka *index*. **git add -A** will add all files in the directory and sub-directories. **git add .** adds all files in the current directory including it's subfolders.
5. **git status** just to see that git has added the file/s (unnecessary step)
6. **git reset HEAD -- myfile.txt** will remove that file from the staging area. The two hyphens may not be necessary. Used on it's own it will unstage everything.
7. **git commit -m "Your message about the commit"** will make the commit.  the **-m** stands for message you can make, added between quotes. You can also use **-a** for all files though to leave a message you can combine them into **git commit -am**.

    Next let’s make a new branch:
8. **git log** shows a record of your commit/s along with author and email address. Press `q` to quit git log.
9. **git shortlog** lists the messages of each commit.
10. **git branch** will show a list of available (local) branches and indicate which branch you are working on with an asterisk. To see *all* branches including the remotes use **git branch -a**
11. **git checkout _master_** will switch you back to the master branch or whichever branch you name.
12. **git checkout -b _first-branch_** combines the *branch* command (the -b bit) with the *checkout* command. In this example it creates a new branch named‘first-branch’ though you can name it whatever you like. It will then ‘check you out’ on this new branch. In other words you'll now be working on the new branch. (see the [Atlassian page](https://www.atlassian.com/git/tutorials/using-branches/git-checkout))
13. **git rm** is to remove a file **git rm index.html**. Most useful as `git rm --cached index.md` which doesn't delete the file only the reference to it.
14. **git add -A && git commit -m "Add About page"** the *&&* is used to run two commands one after the other.
15. **git diff master** If you're in a different branch from the master one you can use this to see the changes made between the current and the master. With only one branch defined git compares it to the current branch. Otherwise use ```git diff branch-1 branch-2```
16. **git merge first-branch** If carried out from the master branch the first branch will merge into the master.
17. **git push** on it's own will merge the current branch with the same branch on the remote one. No need to add anything else. THIS IS IMPORTANT because it means just two words could be used to update an entire web site. If you want to choose which local branch updates which remote to push to you just add these to the command, with the remote first `git push <remote repo> <local branch>`. Typically this could be `git push origin master` though if you're in the master branch then *origin* is the default remote choice anyway.

    A typical workflow might be work on a new branch locally. Then when done merge with the master. Then **git push** to update the live site.

18. **git branch -d first-branch** will delete the branch. BUT this will only work if the branch has been merged first. If you want to delete it **anyway** you need a capital D: **git branch -D first-branch**.

    **git branch** on it’s own can be used to check whether it’s still there.

19. **git branch first-branch** would create a new branch but not switch to it the way that **git checkout -b** would.
20. **git checkout -f** forces git to to undo changes back to the last commit on the branch. This is ALL changes on the branch since the last commit.
21. **git remote** on it's own will just tell you whether there is a remote and what's it's called. To find out more...
22. **git remote show origin** You'll need a password here. Yo see the fetch and push urls, the HEAD branch, and some other info.
23. **git submodule add https://github.com/chaconinc/DbConnector** This installs the theme (or whatever it is) and adds a **.gitmodules** file to the root directory which contains a local path and a url to the theme/library/whatever. To add a submodule (typically a library or a theme.) CD into the directory you want the theme to be first (not sure this is necessary).
24. To remove a submodule you have to delete from the git config as well as deleting the files: **rm -rf .git/modules/solar-theme-hugo**
25. Use **git submodule** to see which submodules are configured.



### .gitignore

If you want git to ignore certain files in your project you can create a special file listing those files. The file is named `.gitignore` and is usually placed in the root of your project. You then simply list the files you want ignored with each one placed on a new line like so:

```git
# This is comment, starting with a hash
README.md
# You need a path to file if it's not in the same place as your .gitignore file
/layouts/choss.html
```

If your file was previously committed you have to remove it using `git rm`.

```bash
git rm --cached README.md
```

The `git rm` command is the opposite of `git add`. It removes files from the staging but without the `--cached` flag it will delete the file as well.

In VS Code file explorer panel the file is now marked with a `D` to show it's been deleted from the list.

 The first line in file is the path of url from the parent folder: **~/learnenough/.gitignore**. After that you simply list the files you don’t want tracked on each line. You can add .gitignore to the staging area and commit it, which also removes it from the **git status** list.

You can also add whole folders where they’re named, followed by a backslash: `images/`. And you can use the wildcard * so for instance *.jpg would ignore all jpeg files.

For more details on `.gitignore` check out the page on [Atlassian](https://www.atlassian.com/git/tutorials/saving-changes/gitignore)


## GitHub
First create a new repo via the web interface of GitHub. (It is possible to do this via the CLI using `curl` but easier to use the web interface). I did following the instructions but the line below may be enough.

Create a new folder then use `git init` to make it a repo.

A prerequisite, one time thing is:

```
Listing 1.1: One-time global configuration settings.

$ git config --global user.name "Your Name"
$ git config --global user.email your.email@example.com
```

If you leave out *--global* the default is to configure git for the current project only. You can also use *--local* to do the same thing.

### Setting up a new repo
1. Login to your GitHub account and create a new repository. Give a name and description.
2. On your local machine create a folder (may as well give the same name as your repo). Add a file.
3. Back on the website you can follow the commands specifically for setting up. They are like the following.
   1. **git remote add origin https://github.com/xixianykus/flexbox.git** IMPORTANT   
    `git remote add origin git@gitlab.com:xixianykus/steveandfran.git`

    You will be prompted for your username or email and password. The value is stored locally in the git config file (You can edit it there if you want to).
4. `git push -u origin master` will upload your code. Presumably you could have put **git push -u origin first-branch** to upload from there.

 >>  “You might be wondering what that "origin" word means in the command above. What happens is that when you clone a remote repository to your local machine, git creates an alias for you. In nearly all cases this alias is called "origin." It's essentially shorthand for the remote repository's URL. So, to push your changes to the remote repository, you could've used either the command: git push git@github.com:git/git.git yourbranchname or git push origin yourbranchname” [hubspot.com](https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners)

To delete a repository you need to click on the *settings* tab, scroll down to the bottom to the *danger zone* to find the delete button.

9. **git clone** is used to download something from a repository. On GitHub you'll see the clone/download green button. Click and the url of the repository will be there to copy. From your terminal type

    **git clone https://github.com/giraffeacademy/ga-hugo-theme.git myfolder**.

If you want it in the current folder then you don’t need to specify the folder at the end.

### Create a new repo from the terminal
```
git init
git add README.md
git commit -m "first commit"
git remote add origin git@github.com:alexpchin/<reponame>.git
git push -u origin master
```
To push a repo from the terminal:
```git remote add origin git@github.com:alexpchin/<reponame>.git
git push -u origin master
```

## Problems uploading and downloading

Sometimes there can be a clash of what’s on your local machine and what’s on the GitHub server. You may get a message saying the histories are unrelated.

A way round this is to use the following addition to a command:

`--allow-unrelated-histories`

## Clone another repo

Get the url from GitHub. They from the command line CD into the parent directory of where you want your new project to be and then type:

`git clone https://github.com/xixianykus/flexbox.git myFolderName`

This creates the subfolder with a name after the command.
****

NB. *To collaborate w someone they need to add you as a collaborator first.*


## To invite someone

In your Github repo go to the settings section. The second section down is *Manage Access*. Click this and you'll probably be prompted for your password. In this section is a button on the right which says *Invite a collaborator*. Press this and start entering the Github user's account name. This found automatically. From here you have the option to invite the person. They will get an email which they have to respond to within 7 days.

## Join someone else's repo

To pull content from someone else's repo first accept their invitation (above). Here get the address of the repository. It will be either an https address or an SSH one:

`git@github.com:githubuser/repo-name.git`

Create a new folder on your computer where you want to `pull` the project to.

From within this project create a git repo `git init`.

Next create an alias for the the address you have... We'll use the common alias name `origin` though you can call it what you want.

`git remote add origin git@github.com:githubuser/repo-name.git`

If you want to check the address use `git remote -v`

Next you have to tell git to `pull` the repo from the server AND specify which branch to pull (even if only one branch exists!).

**NB.** Unlike your local repo the default name on Github is *main* rather than *master*.

`git pull origin main`

This should download the entire repo into your empty folder AND update your `.git` folder.





## Line endings

You may encounter messages like `fatal: LF would be replaced by CRLF`. This is saying the LF line ending would be changed to CRLF. Unfortunately this does not help much. The fix is to change the config of `core.safecrlf` from `true` to `warn`:

To check the setting just type: `git config core.safecrlf`

If you need to change it to warn type: `git config core.safecrlf warn`

You should probably also have `git config core.autocrlf` set to `true` too.

The above will only change things on your local repo. If you want this to be the default behaviour then use the `--global` flag:

```git
git config --global core.safecrlf warn
```

An alternative is to have your code editor (eg. VS Code) set to handle LF and then have `core.autocrlf` set to `false`.

[Read more](https://exceptionshub.com/trying-to-commit-git-files-but-getting-fatal-lf-would-be-replaced-by-crlf-in-some-file-in-repo.html)

## Git configuration

You can see and edit your configuration settings directly in the text editor of your choice. To see if a text editor is already set:

```git
git config --global core.editor
```

This should give you the path to an editor if this is set:

```bash
"C:\Users\admin\AppData\Local\atom\app-1.35.1\atom.exe"
```

However if the path to your editor is set you can just use the name directly. For VS Code the program file is called `code.exe` so all that's required is `code`.

To change default editor you type the above config command and add the path or editor name:

```git
git config -global core.editor code
```

Once that is done you can view and edit these settings directly in that editor by typing:

```git
git config --edit
```

The config settings open in your editor. The default is for the local (ie. the project you are in) settings. To see your default or global config just add `--global` to the end:


```git
git config --edit --global
```

## GitHub pages

On GitHub you can only have one subdomain on your GitHub url eg. https://xixianykus.github.io

However you can have more than one site if it’s built off GitHub pages. It's said that to do this you need to create a branch of your project called gh-pages. I've found this to be unnecessary. Whichever branch it does need to be set to *public* (unless you have a paid account). This is easily done by going into the settings of your repository, scrolling down to GitHub pages section and using the first drop down. Once done a link will appear and it will say:

“Your site is ready to be published at: `https://xixianykus.github.io/flexbox/)`”

To start a new site on GH pages:
1. Create a new repo on GitHub.
2. Get the clone link and clone the new repo to a NEW local folder.
3. Create a new branch called gh-pages
4. If you have an existing project you can copy the files here. Otherwise begin creating your project.
    ```$ git push origin gh-pages```
5. You can stay on the master branch *if* you uploading to your one site for *User pages* rather than *Project pages*



## Alternatives to GitHub

GitHub is used by 26 million people making it by far the most popular service. But it’s been around the longest and people are sheep-like and tend to follow the herd. It was also bought up by Microsoft which might mean they’re about to fuck it in up some way in the near future.

There are two alternatives (at least). These are [GitLab.com](https://gitlab.com/) and [Bitbucket.org](https://bitbucket.org/). Interestingly [Slant recommends GitLab for most people](https://www.slant.co/versus/532/4860/~github_vs_gitlab) compared to GitHub. Bitbucket is not covered in this article.

## GitBash

For Windows users installing the git bash app is a good way to go. By default this will open in the c:\users\ folder. However if you want to change this to a different folder, like where your git project are it’s easy to do.

1. Press the Windows key followed by *gi* as if about to start git bash. 
2. Right click on it with the mouse and choose *Open file location*. This will take you to the shortcut for the app.
3. Right click on the short cut (git bash) and choose properties.
4. In the box that opens the **Target** value should be something like ```"C:\Program Files\Git\git-bash.exe"--cd-to-home```. Delete the *--cd-to-home* part.
5. Change the value of the **Start In** field to the path you want the app to start in. For example, ```E:\Projects\```.

### Navigating
1. Use CD followed by the folder name to change directory
2. CD .. to go up one level or CD ../.. to go up 2 levels etc..
3. Use an absolute path: eg. CD /D/Websites/javascript where D is the partition. You can do this from anywhere.
4. CD / takes you to the root of git bash. 

### Updating

To manually update type: `git update-git-for-windows`. You can also set Git-Bash to check for updates everyday. Alternatively downgrade, unzip and install from the [main site](https://gitforwindows.org/). You can use the same command to check if your version is up to date. If you just want to see which version you have just type: `git version`.


## Links

1. [Codetype Git for Beginners](https://setcodetype.com/articles/how-to-use-git-for-beginners/)
1. [Learn Enough Git to be Dangerous](https://www.learnenough.com/git-tutorial/) great, in depth written tutorial.
2. [Git How To.com](https://githowto.com/)
1. [Official site](https://git-scm.com/) with [documentation](https://git-scm.com/doc), [videos](https://git-scm.com/videos) and [downloads](https://git-scm.com/downloads)
1. [GitHub: working with SSH](https://help.github.com/en/articles/connecting-to-github-with-ssh)
1. [The Git Handbook on GitHub](https://guides.github.com/introduction/git-handbook/)
1. [Git & GitHub tutorial for beginners](https://product.hubspot.com/blog/git-and-github-tutorial-for-beginners) - nice and basic
2. [Learn Git with Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) - comprehensive tutorial from Atlassian.com
3. [Jekyll site on GitHub pages](http://jmcglone.com/guides/github-pages/) - Great tutorial, well written and easy to follow.
7. [Thinkful Guide to GitHub Pages](https://www.thinkful.com/learn/a-guide-to-using-github-pages/start/existing-project/project-page/new-repo/) is a great step by step guide.
8. [Git Cheatsheets](https://overapi.com/git) at OverApi.com.
dogworm
