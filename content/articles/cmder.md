---
title: "Cmder"
date: 2019-10-30T06:07:28Z
draft: false
tags: ["Cmder", "cli", "shell"]
categories: ["development"]
summary: "Some stuff about the great free command line tool Cmder"
---

- [Adding the portable version to the path in Windows 10](#adding-the-portable-version-to-the-path-in-windows-10)
- [Adding a shortcut to the terminal window](#adding-a-shortcut-to-the-terminal-window)
- [Using Cmder as integrated shell for VS Code](#using-cmder-as-integrated-shell-for-vs-code)
- [Links](#links)

To set up the portable version just unzip to any folder.

After you can add a shortcut to to the R mouse click in Windows Explorer. To do this open a cmd prompt with elevated (admin) rights. Navigate to the Cmder folder and type <kbd>\.cmder.exe /REGISTER ALL</kbd>

To add an alias to any command type __alias l=ls $*__ . This will mean you can now use the ls command by just typing l.

1. **The cURL** is for retrieving things from the internet, like a web page. An example is <kbd>curl https://github.com/html5/index.html</kbd>. This would �capture� the page and print the source code to the terminal screen. If you want to download the page as a file (or anything else with a link) you use the *output* attribute: ```curl -o```. This outputs to a file rather than the screen. Next your write the path and file name to use then you -OL for output and location you want to save the file, and finally the full url to be downloaded:

```
curl -o images/new-car.jpg -OL images/cars/newcar.jpg
```
**Includes some Linux commands like:**

* ls - list, ls -l long form of listing.
* pwd - shows current location on the computer
* touch - creates files
* rm - remove a full delete.
* vim
* cp - means copy. ```cp filename.html newfile.html``` will create a duplicate file of filename.html called newfile.html
* cURL [website](https://curl.haxx.se/docs/manual.html)

## Adding the portable version to the path in Windows 10

1. Press the Windows key and type **env** to get the *environment variables* dialogue box from the control panel
2. Click the **Environment Variables** button
3. Under the “System Variables” section (the lower half), find the row with “Path” in the first column, and click **edit**. 
4. Add the the path to Cmder location. (That is to the folder that contains it rather than the program itself)

## Adding a shortcut to the terminal window
In the folder where the portable version of Cmder resides look in the *config* folder and open a file *user_aliases.cmd* . 


Add the line:
```VSCodePortable.exe=vs``` (For some reason this doesn’t open files)


## Open in a specific folder

Press the Windows key and type Git. R click the short cut and choose *open file location*. R click on the icon and choose *properties*.

In the ‘Start in’ field replace:

```%HOMEDRIVE%%HOMEPATH%```

with the path you want the app to start in. [More](https://superuser.com/questions/254525/shortcut-to-command-prompt-pointing-to-specific-folder#254527)

## Using Cmder as integrated shell for VS Code

OK not done this yet but [here’s how](https://winsmarts.com/using-cmder-as-integrated-shell-in-vscode-c3340714fe3c)

## Links
1. [Adding a path to environment variables](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/)
2. [Tony Sneed article](https://blog.tonysneed.com/2015/01/31/cmder-making-the-command-line-your-best-friend/)
3. [Basic Unix Commands](http://mally.stanford.edu/~sr/computing/basic-unix.html) many of which feature in in Cmder.
4. [Learn Enough Command Line to be Dangerous](https://www.learnenough.com/command-line-tutorial/basics)
dogworm
