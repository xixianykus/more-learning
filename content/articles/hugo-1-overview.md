---
title: "Hugo 1 Overview"
date: 2019-11-08T10:07:38Z
draft: false
tags: ["hugo"]
categories: ["tutorial", "static sites", "hugo"]
summary: "Brief and unfinished description of Hugo Static Site generator."
---

The current state of static site generators is something like the wild west. There is such a growing and bewildering array of different apps that it can be hard to know where to even begin.

One of the main divisions seems to me between those that are based on Javascript frameworks like Vue and React and those that are not. Gatsby is a very popular though from reading through various posts scattered around the web it seems like to get the best from it you really ought know React.

As I don't know any React at all, scratching off these JS framework based generators was good way to narrow the field for me. 

An obvious next choice from there was Jekyll. Jeykll is one of the oldest around, has great docs and plugins and Jekyll sites can be run from GitHub which seemed like a bonus (though not as much as it once might have been). Jekyll is built on Ruby which, if like me you're on Windows you have to install first. This is not too hard to do but my Ruby folder now contains over 55,000 files and is over a gigabyte in size. This seemed overkill to create websites that could end up being only a 200kb to finish with. Despite this I used Jekyll for a while and I do quite like it. It works well, had great documentation and an every growing collections of plugins to choose from. 

Another popular choice is Eleventy. I haven't used Eleventy much at all. One of it's strong points is apparently ease of use, which seemed mostly true from my limited experience except for getting the bastard css files to work. I did eventually manage it though and I think I should play with it a bit more sometime. One thing I wasn't so keen on was the all the clutter it generated. Rather than install it globablly it's recommended to install fresh on a per project basis. The idea is when Eleventy updates the new version won't break your site which is still using the earlier version. However this ends up generating  some 13,000 files adding 100mb to the node modules folder. This wouldn't be so bad if you do it once but of course you get that with every new site you create.

Compared to these hulking space hungry, monster apps Hugo was a breath of fresh air. It was really easy to install. Simply download the zip file which is less than 12mb and extract somewhere, anywhere on your system. When unpacked there are just 3 files only one of which, hugo.exe, you actually need. (The other two are tiny text files: a readme and a licene file.). From there just add it to the system path, something that is a doddle in Windows 10. It's just 3 clicks from the start button (details follow in next section).

However when it comes to using Hugo it's certainly not all roses.