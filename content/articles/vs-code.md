---
title: "VS Code"
date: 2019-10-30T17:37:15Z
draft: false
summary: "Shortcuts, tips and tricks for using the Visual Studio Code text editor."
categories: ["Development"]
tags: ["vs code"]
---

- [Full Official Cheatsheet](#full-official-cheatsheet)
- [Quick selection expansion and reduction](#quick-selection-expansion-and-reduction)
- [Quick copying](#quick-copying)
- [Moving without cut’n’paste](#moving-without-cutnpaste)
- [Multiple cursors](#multiple-cursors)
- [Edit open and close tags simultaneously](#edit-open-and-close-tags-simultaneously)
- [Coloured matching brackets](#coloured-matching-brackets)
- [Find and Replace](#find-and-replace)
- [Page Navigation](#page-navigation)
- [Project Navigation](#project-navigation)
- [Opening Files](#opening-files)
  - [Shortcut](#shortcut)
  - [From the command line](#from-the-command-line)
  - [From the exlporer panel](#from-the-exlporer-panel)
  - [Shortcut to open folders](#shortcut-to-open-folders)
- [Comparing files](#comparing-files)
- [Symbols](#symbols)
- [Keyboard shortcuts](#keyboard-shortcuts)
- [The user interface](#the-user-interface)
- [Split panes](#split-panes)
- [Zen mode](#zen-mode)
- [Mousewheel Zooming](#mousewheel-zooming)
- [Autosaving](#autosaving)
- [Beautifying aka formatting](#beautifying-aka-formatting)
- [Using VS Code from the Command Line](#using-vs-code-from-the-command-line)
- [Settings](#settings)
  - [settings.json syntax](#settingsjson-syntax)
- [Extensions](#extensions)
- [Links](#links)

There’s plenty of stuff to help you get started with VS Code.



## Full Official Cheatsheet

From inside VS Code press `Ctrl + k, Ctrl + r` or just click [here](https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf) (pdf)

Here’s a few things that are harder to find.

## Quick selection expansion and reduction

If the cursor is in a word press `Alt + Shft + r arrow` to select the whole word. Press again to expand to all text between tags. And again to include tags. You can carry on to make bigger logical selections.

`Alt + Shft + L arrow` will reduce a selection in the same way.

## Quick copying

Select lines and press `Alt + Shft + down arrow` to copy the line or lines below.

## Moving without cut’n’paste

Select text and use `Alt + Arrow` keys (up or down) to move the text.

## Multiple cursors

Use `Ctrl + F` to find a word or string. Press `Alt + Enter` to create a cursor at every instance of the string on the page.

You can **Alt + click** to set another cursor anywhere.

Hold **Ctrl + Alt** and use the up or down arrow keys to add another cursor on the line above or below.

## Edit open and close tags simultaneously

You can set VS code to edit both tag pairs together with this setting.

`Editor: Linked Editing`

Press `Ctrl + Shft + P` or just `F1` function key and type settings to get the settings then type the above (just *linked* should do) and make sure the checkbox is ticked.

Another way to do this if this setting is not enabled is to click inside one of your tags and the press the `F2` key. You can then edit both tags together.

## Coloured matching brackets

You can have the opening and closing brackets have a matching colour to make editing simpler. This is good for languages like javascript where there are a lot of brackets inside one another. Not so useful for HTML and sometimes useful for CSS (such as using calc function with custom properties or some grid declarations).

In the user settings (`Ctrl + ,`) search for *bracket pair* to get `Editor: Bracket pair colorization`.

## Find and Replace

To find something in a page use `Ctrl + F` to bring up the find dialogue. Put in the word or phrase and press enter. This will *highlight* every instance. To select those instances press `Alt + Enter`. This will also put a cursor at every instance. To deselect but keep the cursor use the left or right arrow keys.

To find all versions of the current word press `Ctrl + d` while the search dialogue is open. To change all simultaneously then press `Alt + Enter`. Or use `Ctrl + f2` to put a cursor at the end of every instance of the word or selection.

To find and replace use `Ctrl + H`.

To find and replace in all documents in the current project use the shift key too: `Ctrl + Shft + H`. This will provide a list in the sidebar. Press enter to commit then press OK to make the changes.

To add a line break (new line) in the search box use `Ctrl + Enter`.

### Limiting your search

You may wish to limit your search to just one folder. Open the explorer pane `Ctrl + Shft + E` and right click on the folder you wish to search. Choose `Find in folder..` or press `Alt + Shft + F` to start the search.

To search only files you have open first press `Ctrl + Shft + F`. Press the little ellipsis icon to get the files to include dialogue. At the end of this press the little open book icon to search in open files only.

There are more ways to limit or filter search results using this dialogue including a list of files, a path or using regex.

## Page Navigation

Quickly go to another tab by holding down **Alt** and pressing the number of the tab as it appears from L to R.

Open a new file using the *command palette* `Ctrl + p` and typing part of the file’s name.

To open the *file explorer* on the LH side **either** press `Ctrl + b` to open and close the LH pane. If the last used panel was not the explorer tree just use: `Ctrl + Shft + e` from there.

To open a new window `Ctrl + Shft + n` and to open a specific folder then type `Ctrl + k, Ctrl + o`.

To navigate to a certain line press `Ctrl + g` then type the line number. (or press `Ctrl + p` then a colon and the line number.)

## Project Navigation

Each time you open a folder it is saved as a workspace. To switch between these press `Ctrl + r` for a drop down list of workspaces.


## Opening Files

### Shortcut

Press `Ctrl + p` and start typing the file name until it appears.

### From the command line

Start with the prefix **code**. For instance `code index.html`. (But this doesn’t work in the portable version.)

Open a specific directory in the same way: `code html5css3`.

Finally navigate to the folder you want to work on and use `code .` to open it.

### From the exlporer panel

To open the explorer panel press `Ctrl + Shft + E`. To close the entire panel `Ctrl + b` (This won't work from inside a markdown file if using *Markdown All in One* extention.)

When opening from the file open pane the file name on the tab appears in italics denoting that if you click another file it will replace the one already opened.

To open a file in the RH pane simply click on it (in the file tree) while holding the `alt` key down.

Worth seeing if quotes close automatically too. { the [setting] for this (behaviour) is in the settings. So let's see: "quotes don't auto close in "markdown" How about brackets (work well) [works] and {works} So pretty ({).

### Shortcut to open folders

`Ctrl + r` gives a list of folders recently opened. You can begin typing the name for quick access to it.

## Comparing files

In the file explorer click and Ctrl click the files you wish to compare. Right click and choose `compare`.

## Symbols

In CSS sybmols refer to selectors. In JS it’s variable names. To bring up a list of *symbols* in a page press `Ctrl + shft + o`.

To go to the next same symbol in a project make sure it's selected and press `f7`. To go to the previous instance of it: `Shft + f7`

You can rename all symbols at once. Put the cursor in the one you wish to change and hit `f2` and type your new name.

You can do the same with a word or any selection using `Ctrl + f2`.


## Keyboard shortcuts

`Ctrl + k, Ctrl + s` will bring up the keyboard shortcuts panel. Otherwise use `Ctrl + Shft + p` and type *keyshor*.


## The user interface

Reveal or hide the LH panel w `Ctrl + B`. The options in the panel are:
1. File Explorer: `Ctrl + Shft + e` for explorer
2. Search: `Ctrl + Shft + h` or `Ctrl + Shft + f` for find
3. Git (source code managerment): `Ctrl + Shft + g` for git
4. Debug: `Ctrl + Shft + d`
5. Xtensions `Ctrl + Shft + x`


## Split panes & split tabs

You can split editor in different ways. Shortcut is `Ctrl + \`.

To open a file in the second (RH) pane hold down `Alt` while clicking on it.

To switch panes or tabs press `Ctrl + a number` where 1 will choose the active tab in the leftmost pane. `Ctrl + 2`. If you type a number 1 greater than the number of panes a new empty pane will be opened.

`Shft + Alt + 0` will toggle between horizontal and vertical split.

**NEW (Sept 21)** You can split a page into two views in the same tab (rather than starting a new group). Simply right click a files tab and choose *Split in group* or use the keyboard shortcut `Ctrl + K, Ctrl + Shft + \`. This key combo acts as a toggle so pressing it again will undo the split tab. The split is vertical by default but can be changed to horizontal using the *Toggle Layout* button that appears top left.

You can also split a file into an existing group

## Zen mode

This gets rid of all the clutter and user interface enabling you to focus on the one file and nothing else.

The shortcut is `Ctrl + k, Ctrl + z`. To leave zen mode you can use the same key combo again or just press `Esc` twice.

This shortcut didn’t work for me straight off (though the double `Esc` did). I had to go into the *preferences: open keyboard shortcuts*. Although it appeared to be there typing it in again got it working.

The file opens up as a narrowed, centred strip for easy reading. To adjust the width just click and drag one edge in or out to get the desired width.

Many of the regular shortcust work in zen mode.

To switch to a different file in zen mode use `Ctrl + p` and scroll through the open files. Toggle the CLI via `Ctrl + '`.

You can open the side panels on the left using the standard shortcuts ( `Ctrl + Shft + ` `e` for explorer/file tree, `f` for find or search, `g` for git, `d` for debug, and `x` for extentions. All are closed using `Ctrl + b`.

You also have access to the command palette using the regular `Ctrl + Shft + p` or `f1`.



## Mousewheel Zooming

This can also be set in the workspace setting by ticking the box.

**Ctrl + mousewheel**

Amazing this is not a default setting as it is on Atom. Search for **wheel** in preferences and tick the box.

You can also set this via the *setting.json* file:

`
"editor.mouseWheelZoom": true
`

You can also zoom in and out of the whole UI using `Ctrl + +` and `Ctrl + -`.

## Autosaving

Autosave can be found in the UI preferences. Search for *autosave*.

Can also be set in the setting.json file. Add the following. The number is in milliseconds so add what you want. 1 minute is 60,000.
`
"files.autoSave": "afterDelay",
"files.autoSaveDelay": 1000
`

It’s also possible to set autosave from the settings panel. closing "fdl iels.

**files.autoSave**: Can have the values:

1. **off** - to disable auto save.****
2. **afterDelay** - to save files after a configured delay (default 1000 ms).
3. **onFocusChange** - to save files when focus moves out of the editor of the dirty file.
4. **onWindowChange** - to save files when the focus moves out of the VS Code window.
5. **files.autoSaveDelay** - Configures the delay in milliseconds when files.autoSave is configured to afterDelay. The default is 1000 ms.

## Beautifying aka formatting

Use `Alt + Shft + f` as the default. Installing Beautify allows use of a *.jsbeautifyrc* file to be added to the root of a project directory. This allows many customizations including stopping the automatic hard wrapping that happens with beautify.

A popular replacement for *Beautify* is *Prettier* which is added as an **extension**.


## Using VS Code from the Command Line

- Open the integrated CLI with `Ctrl + '`. (The offical docs say `Ctrl + backtick` but this does not work for me.)
- You can also open a non integrated, external terminal using  `Ctrl + Shft + c`
- Open a second terminal and split the interface in 2: `Ctrl + Shft + 5`
- Open VS Code just by typing: `code`
- Open a new Code window: `code -n`
- Open the current dir in the most recently used VS Code window `code -r`
- Open the current dir in a new window `code .`
- You can see a full list of CLI commands by typing `code --help`

## Settings

There are a huge number of different settings in VS Code and they are accessible through the settings panel. To access this use the shortcut `Ctrl + ,`. You can also access this through the `Ctrl + Shft + p` or `f1` key and type *preferences* or *settings*. You can have multiple settings tabs open at the same time if you open them in different panels. For intance want to look at different settings simultaneously.

There are two types of setting *User* and *Workspace*. The user settings are global meaning they are general and apply to every instance of VS Code. The *workspace* settings apply only to the specific project you are working on and will override the *User* settings. These are stored in the route of your project in `.vscode/setting.json`.

The *Workspace* settings are particularly useful for trying things out. It's easy to get lost and forget what settings you've changed but the workspace settings are instantly accessible. From a terminal just type `code .vscode/settings.json` to view the workspace specific settings in VS Code. You can even set extensions to only run in certain workspaces.

### settings.json syntax

Many options are set using drop down, check boxes and input fields. However some rely on editing the `settings.json` directly. Those that do have a little link at the bottom of the description, `Edit in settings.json`. This takes you to the *User* settings file, `settings.json`. (On Windows this can be also be found by typing `%appdata%/Code/User` from the Windows key start box).

The syntax of key value pairs looks like this:

```json
{"workbench.colorCustomizations": {
    "window.activeBorder": "#2780f5",
    "window.inactiveBorder": "#cccccc"
},
"workbench.tree.indent": 12
}
```


## Extensions

These are some extensions I use:

- **Markdown All in One** - a  great markdown add on
- **Bookmarks** helps quickly find specific lines in a document.
- **File Utils** quickly change files and folders, move, rename, dupiclate, delete
- **VS Code Icons** some nice icons for VS Code
- **Prettier** code formatter
- **Formatting Toggle** single click to change
- **Beautify** - allows customizing which the normal formatter does not.
- **Beautify css/sass/scss/less** - for css files
- **HTMLtagWrap** - press `alt + w` after selecting some text to add html tags. A slightly quicker alternative to using the built in `Emmet: Wrap with abbrevation`. 

Language support extensions:

- **Hugo Language & Syntax Support** for Hugo SSG.
- **YAML** Yaml Ain't Markup Language
- **Better TOML** for Tom's Obvious Minimal Language
- **SVG** code support, autocomplete, minify and preview (only through sidebar explorer though)
- **Svelte for VS Code** for Svelte
- **Stylelint** for CSS, SCSS, Less, Markdown, Svelte, HTML, JS etc.. You need to create a config file or it (built in one is not recommended) AND disable built in linters for CSS, SCSS and Less.

Some old ones I rarely use now

- **Code Spell Checker** - hmm, not sure about this has it has lots of false positives, like css code highlighting (minmax is not a word! duh!)
- **Open in Browser** - `alt + b` to open page in default browser

## Links
- [Zell’s keyboard shortcuts](https://zellwk.com/blog/useful-vscode-keyboard-shortcuts/)
