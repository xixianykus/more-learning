---
title: "Links"
date: 2019-11-06
toc: true
css:
- "ol {
        column-width: 350px;
        column-gap: 100px;
        padding: 1em;
    }"
- "li {
        break-inside: avoid;
    }"
---

These categories might also be useful for tags on this blog.

[Techstack](https://tech-stack.tools/) is a whole website of different dev tools listed by category.

<!-- ### Tutorials

1. CSS
2. HTML
3. JS
4. Graphic Design
5. VS Code
6. VS Code plug-ins
7. SVG
8. Git
9. Hugo
10. Static Site CMS's
11. Illustrator
12. Photoshop

## Resources

1. CSS
2. Photos
3. Vectors
4. Fonts
5. Online Services -->

## Blogs

1. [Tania Rascia](https://www.taniarascia.com/)
2. [Stuff & Nonsense](https://stuffandnonsense.co.uk/) is designer Andy site.
3. [Gift Egwuenu](https://www.giftegwuenu.com/) is a Nigerian frontend dev from Nigeria
4. [dev.to](https://dev.to/) is an open blog for all things dev. Do a search for [Hugo](https://dev.to/search?q=hugo) and there's several articles.
5. [SpeckyBoy](https://speckyboy.com/) big site covering all aspects of design and dev.


---

## Tutorial Sites

### General

These cover a variety of topics:

1. [Dev tips for Designers](https://www.youtube.com/user/DevTipsForDesigners) 
2. [Another great Youtube channel](https://www.youtube.com/travisneilson) by [Travis Neilson](http://travisneilson.com/)
3. [A Shot of Code](https://www.youtube.com/channel/UC0HsZmiuGCRpKUHR_owGuxA) is a Youtube channel


### CSS

1. [Kevin Powell Youtube channel](https://www.youtube.com/channel/UCJZv4d5rbIKd4QHMPkcABCw) good clear information on html/js/css/svg/bootstrap.
2. [A Visual Guide to CSS3 Flexbox Properties](https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties) by scotch.io is a great tool to play with.
3. [Freefrontend's list of CSS and JS articles](https://freefrontend.com/frontend-stuff-4/#articles-html-css)
5. [MDN Youtube channel](https://www.youtube.com/channel/UCh5UlGiu9d6LegIeUCW4N1w) w Jen Simmons.
6. [CSS-IRL.info](https://css-irl.info/) - CSS in real life, informative blog on CSS.
7. [CSS Grid Cheatsheet](https://yoksel.github.io/grid-cheatsheet/) is an excellent resource where you can play with some of the more complicated grid properties.

{{< links >}}

### Git

1. [Using Submodules](https://www.vogella.com/tutorials/GitSubmodules/article.html)
2. [Tower](https://www.git-tower.com/) WSIWYG interface for Git.
3. [W3 Docs guide to git](https://www.w3docs.com/learn-git.html)
4. [Official Git Guide](https://git-scm.com/docs)
5. [Bitbucket](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud) has a good guide to Git.
6. [Initial Commit](https://www.initialcommit.com/)



## Resources

1. [CSS 3 Patterns Gallery](https://leaverou.github.io/css3patterns/) great selection of pure CSS background patterns.
2. [CSS Separator Generator](https://wweb.dev/resources/css-separator-generator) - Great resource for generating header sections. At wweb.dev
3. [CSS Gradient Generator](https://www.outpan.com/app/0fa2baf437/css-gradient-generator) generates CSS or SCSS.
4. [Wweb.dev bumper list](https://wweb.dev/resources/creative-backgrounds) of background generators.
5. [Free SVG illustrations](https://wweb.dev/resources/free-svg-illustrations) - another great list from wweb.dev
6. [Particles.js](https://vincentgarreau.com/particles.js/) lightweight JS library for animated, interactive backgrounds and custom CSS.
7. [Palette List](https://www.palettelist.com/) a way to get colour schemes.
8. [gra.dient.art](https://gra.dient.art/) visual tool to create multilayer CSS images with gradients and shapes.


### CSS Slideshow

1. [Code Convey List](https://codeconvey.com/category/css-slideshow/) though you need to disable adblockers.

### Vector graphics

1. [Undraw.co](https://undraw.co/illustrations) a good site for open source svg images.
2. [Get Wave](https://getwaves.io/) is a simple online svg wave generator. (Also there is [blobmaker](https://www.blobmaker.app/))

### Templates

1. [ColorLib's list of top templates](https://colorlib.com/wp/fullscreen-website-templates/)


## Hosting
1. [Netlify](https://www.netlify.com/)
2. [Render](https://render.com/) - like Netlify, 100Gb data pcm for free.
3. [Vercel](https://vercel.com/) (formerly Zeit Now) is similar to Netlify.
4. [Statically](https://statically.io/) free CDN service to host static files like CSS, images etc.
5. [Cloudinary](https://cloudinary.com/) hosting for assets (images, video etc.)


### Static Site Generators

1. [Nift](https://www.nift.cc/) claimed to be the fastest on the block.
2. [Hugo](https://gohugo.io/)

### General

1. [Lunr Search](https://lunrjs.com/) to add to web sites.

### Hugo

1. [Kodify](https://kodify.net/hugo-static-site-tutorials/) comprehensive guide.
2. [Luna Software Hugo Tuts](https://code.luasoftware.com/tags/hugo/)
3. [Bryce Wray](https://brycewray.com/)
4. [Regis Philibert](https://www.regisphilibert.com/) has a great series of posts on Hugo.
5. [The New Dynamic](https://www.thenewdynamic.com/) more Hugo and closely aligned with the above.



