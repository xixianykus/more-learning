---
title: "To Do"
date: 2019-10-28T23:10:31Z
draft: true
---

So this is list of things to do to get this site ready....

1. Install prism js with the following languages:
   1. js
   2. css
   3. html
   4. git
   5. md
2. Sort the formatting of the top menu
3. Sus out how to make the page full height (body min-height? stretches the header and footer so maybe give them fixed height in px)
4. Figure out the menu system: how's it gonna work? Tags / Categories etc.