---
title: "Home"
date: 2019-11-06T12:36:07Z
draft: false
---

This is my web site with notes about Hugo static site generator, CSS, Bash, SVG, image editing and, well ... just look under tags.

My newer stuff on CSS is at [CSS Geek](https://cssgeek.netlify.app/), newer stuff on git is at [Git Quest](https://gitquest.gq) and likewise newer stuff on Hugo is at [Learn Hugo](https://www.learnhugo.ml/). So this is more general *other* web dev and design related stuff now although, just to add to the chaos, I also have put a bunch of stuff on [blank try](https://blank-try.netlify.app/) too. So as they say, "too many web sites spoil the broth" or something like that but what can I say? I like making web sites and then I end up using some of them. In my defence I actually have a ton more sites sitting around on my hard drive that have never made it beyond localhost and are keeping well out of everyone's way.

On this site there is also a bit more CSS in the [playground section](/playground/) on [subgrid](/playground/subgrid/), and [currentColor](playground/css-current-color/) though stuff here might end up as full post eventually. This is/was a space for me to experiment with HTML and CSS though I mostly use Codepen for that these days. There's also the [about](/about/) page too.

The site currently has {{< how_many_pages >}} pages.

