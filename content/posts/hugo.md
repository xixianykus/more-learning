---
title: Old Hugo post
date: 2019-05-05
categories: ["Development", "hugo"]
tags: ["hugo"]
draft: true
summary: "Short and outdated post on basic Hugo usage"
---

Hugo is good because it’s extremely small and fast. However I’ve lost interest a little because:

1. The server seems to crash all to easily
2. Sometimes things just don’t work. eg. I added some files to the site from the CLI and not only did they fail to show up on the listing page but so did everything else. Frustrating because it’s inexplicable. Maybe a bug, maybe I need to learn more about it.

Maybe worth adding it’s not used by GitHub & CloudCannon, Jekyll is, which could be useful for potential clients who want to edit their site (Prose.io works better with Jekyll).

# Basics

So here are the basics for getting started:

1. **Installation** It can go anywhere just make sure you add *hugo.exe* to the system path.
2. **hugo new site &lt;*folder/path*> to create a new site with a bunch of folders. (Leave out the folder or path name if you're already in where you want the site to be built.) This is very quick.
3. **Add a theme** Either download a zip file and unpack into the the themes folder OR get the GitHub link and use git clone *&lt;full.link.git>* themes/*&lt;themename>*. NB. When using git you may need to create the themes sub-folder by adding the name to the path.
4. All files are created with frontmatter and files are set to *draft* mode meaning they won’t appear on the site. To see these add -D to the server syntax: <code>hugo server -D</code>

## Links
1. [Official quickstart guide](https://gohugo.io/getting-started/quick-start/)
2. [TutsPlus.com guide](https://code.tutsplus.com/tutorials/make-creating-websites-fun-again-with-hugo-the-static-website-generator-written-in-go--cms-27319) is a very basic get started using a theme guide. Not that useful.
3. [Mike Dane’s Video series](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3) is currently the best, most comprehensive, beginner friendly guide out there.
4. [Julian Knight’s pages on Hugo](https://it.knightnet.org.uk/kb/hugo/) - he made the switch from Jekyll.
5. [Chris Stayte's Youtube series](https://www.youtube.com/user/BossSlayer95/search?query=hugo)
6. [Slow Start for Beginners](https://discourse.gohugo.io/t/hugo-101-slowstart-for-beginners/18383)
7. [Make a Hugo blog from Scratch](https://zwbetz.com/make-a-hugo-blog-from-scratch/) by zwbetz.com
8. [Convert Your plain old HTML site to Hugo](https://zwbetz.com/convert-your-plain-old-html-site-to-hugo/) - another good start from zwbetz.com
9. [All the Zwbetz Hugo posts](https://zwbetz.com/tags/hugo/)
10. 3. [blogdown: Creating websites with R Markdown](https://bookdown.org/yihui/blogdown/) is an online book about R Markdown.  Lots of detail, as only a book can, including a [whole chapter on Hugo](https://bookdown.org/yihui/blogdown/hugo.html).
