---
title: Free stuff
description: There’s lot’s of free stuff out there. Here’s a list of some of it.
date: 2019-05-04
categories: ["Development"]
tags: ["freebies", "resources"]
draft: true
summary: "Unfinished page linking to free resources"
---

(See also [25. Online Services](page025.html))

There are different places to find free web hosting etc. Still need to find about:
1. email
2. forms



### Hosting
1. [Github Pages](http://www.github.com) has free web space for static pages. There is also Jekyll built in to the hosting.
2. [GitLab](https://www.gitlab.org) check URL
3. [Bitbucket](https://www.bitbucket.com) check url
4. [Netlify] (https://www.netlify.com/) free hosting
5. [CloudCannon](https://cloudcannon.com/) free hosting w Jekyll

### CMS & SSG
1. Jekyll
2. Hugo
3. Grav

### Tools
1. Atom / Visual Code Studio / Notepad++ text editors
2. Ruby for SASS & Jekyll

### Assets (photos, vectors, icons, fonts, backgrounds)

### Domain Names
1. [Freenom akd dot.tk](https://my.freenom.com/domains.php)
2. [Using dot.tk on Github](https://stackoverflow.com/questions/44081863/how-i-can-using-a-dot-tk-domain-with-github-pages)

## Links