---
title: "Hugo Theme Review"
date: 2019-11-03T21:29:54Z
draft: true
tags: ["hugo", "private", ]
categories: ["tutorial", "static sites", "hugo"]
summary: "Just links to themes I like the look of."
---

Some cool themes I have not tried are:

1. [Navigator Hugo](https://themes.gohugo.io/navigator-hugo/) which is a cool design.
2. [Infinity Hugo](https://themes.gohugo.io/theme/infinity-hugo/) nice design with cool animations though a one page design which can presumably be changed.