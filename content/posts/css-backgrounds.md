---
title: CSS Backgrounds
date: 2019-06-25
categories: ["Development", "design", "css"]
tags: ["CSS"]
draft: false
summary: "All about CSS backgrounds and gradients."
--- 

- [Background properties](#background-properties)
- [background](#background)
- [CSS colours](#css-colours)
- [Multiple Backgrounds](#multiple-backgrounds)
- [Background-size](#background-size)
- [background-attachment](#background-attachment)
- [Gradient backgrounds](#gradient-backgrounds)
- [background-clip](#background-clip)
- [Links](#links)

## Background properties

```css
background:                                   /* general shorthand */
background-color: rgba(255, 255, 128, .5);    /* sets the colour */
background-image: url(images/hearts.png);     /* can also be used with gradients */
background-repeat: repeat-x;                  /* repeat, no-repeat, repeat-y */
background-attachment: local;                 /* fixed, scroll */
background-position: centre top;              /* -200px 100px; */
background-size: cover;                       /* contain, 50%, 215px 115px, auto (default) */
background-clip: padding-box;                 /* border-box, content-box, text */
background-origin: border-box;                /* padding-box, content-box, text */
```

## background

The property is a shorthand that sets the following properties in a single declaration:

1. `background-clip, `
2. `background-color`
3. `background-image`
4. `background-origin`
5. `background-position`
6. `background-repeat`
7. `background-size`
8. `background-attachment`




## CSS colours

rgb colours can be from 0 to 255 OR in percentages:

```background: rgb(25%, 56%, 12%);```

BUT YOU CANNOT MIX THE TWO in one value. So: ```rgb(234, 28%, 138)``` would be wrong.



### Alpha values

The **alpha value** can be expressed as a decimal from 0 to 1 or a percentage:

```background-color: rgba(241, 173, 89, 0.35);```

```background-color: rgba(241, 173, 89, 35%);```




### HSL (hue, saturation, luminance)

Hue in ```hsl``` is part of the colour wheel and can be in degrees, radians, gradients or even turns. But degrees is simplest and doesn't need stating in the declaration.

```background: hsl(324, 94%, 23%)```



## Multiple Backgrounds

When multiple backgrounds are used there is a stacking order of the different layers. The first element listed is on the top and the last on the bottom:

```css
background: 
   url(number.png) 600px 10px no-repeat,  
   url(thingy.png) 10px 10px no-repeat,   
   url(Paper-4.png);
```

This is opposite to the stacking order in HTML. 

## Background-size

If left unspecified the image will be displayed as the size it is.

```css
background-size: 400px 50% no-repeat;
```
The first value is width (x axis) the second is height (y axis).

If you only use one value: ` background-size: 400px;` then the second is set to  `auto`. This will preserve the aspect ratio of the image.

Two keyword values are `cover` and `contain`.

Both fill the space whilst preserving the image aspect ratio.

`css background: cover;` will stretch to cover the whole element. If required either left/right sides or top/bottom will be clipped as required.

`background: contain;` is similar but without the clipping. If the image has a different aspect ratio to it's container spaces will allow the background-color to show at either the L/R sides or top / bottom sides.




## background-attachment

This can take one of three values: ```scroll``` (default), ```fixed``` or ```local```

There are two views to consider with ```background-attachment```: the main view (browser window), and the local view. 

The ```local``` view was invented because the ```scroll``` value can act like ```fixed``` background in elements other than the ```body```. It scrolls both with the main view and the local view. 



<div class="attachlocal">Set to <code>background-attachment: local</code></div>





## Gradient backgrounds

You can use CSS gradients to produce different kinds of backgrounds.

```css
  background-color: rgb(250, 250, 250);
  background-repeat: no-repeat;
  background-size: 100% 990px;
  background-image: linear-gradient(170deg, rgba(2,0,36,1) 13%, rgba(61,110,161,1) 56%, rgba(250,250,250,1) 56%);
  ```

  The background-size values means the background is full width (100%) and extends for 990px from the top.

  The fact that both the second and third rgba values are at 56% means there will be a sharp colour change there. And because the gradient is angled at 170 degrees it means this line will not be horizontal.

  <div class="bgbox grad1"></div>

  Gradients really deserve their own page but currently come in 4/5 types (the last is only partially supported at present).

  1. linear-gradient
  2. radial-gradient
  3. repeating-linear-gradient
  4. repeating-radial-gradient
  5. conic-gradient




  ## background-clip

  This is the property that allows one to add gradients to text. 
  
  The four common values for `background-clip` are:
```css
  1. border-box
  2. padding-box
  3. content-box
  4. text
```

  Extends out all the way to the element’s border.

  <div class="bgbox clipbox border">This is set to: border</div>


  Extends out to the element’s padding.
  
  <div class="bgbox clipbox padding">This is set to: padding</div>
  
  Extends out all the way to the element’s content without it’s padding.
  
  <div class="bgbox clipbox content">This is set to: content</div>
  
  This is clipped to the element’s text but there are a couple of other steps. If you clip the background to the text you won't see it because the background image is blocked by the elements colour. So the first thing is to set the text to `color: transparent`. In this case it was still blocked by the text-shadow so I set the text to `text-shadow: none`. Obviously you only need to do this if your text has `text-shadow` set in the first place.

  <div class="bgbox clipbox text">This is set to: text</div>


## Links

  1. [MDN background-clip](https://developer.mozilla.org/en-US/docs/Web/CSS/background-clip)
  2. [CssGradient.io](https://cssgradient.io/) great way to create gradient code.
  3. [Colorzilla Online Gradient Editor](https://www.colorzilla.com/gradient-editor/) has very sophisticated editable gradient examples with both CSS and SASS code.
  4. [Webgradients](https://webgradients.com/) shows gradient examples full screen.
  5. [CSS Tricks on Gradients](https://css-tricks.com/css3-gradients/) provides a good introductory overview.


  <style>
     .attachlocal {
         background-attachment: local; 
         background-image: url(/img/graffiti-bigbird.jpg); 
         height: 250px; 
         width: 60%; 
         background-color: red;
     }

    .bgbox {
         width: 40%;
         height: 215px;
    }
    .grad1 {
         background-color: rgb(250, 250, 250);
         background-size: 100% 70% no-repeat;
         background-image: linear-gradient(170deg, rgba(2,0,36,1) 13%, rgba(61,110,161,1) 56%, rgba(250,250,250,1) 56%);
         border: solid 1px #ccc;
    }
    .clipbox {
         width: 500px;
         height: 368px;
         padding: 1em;
         /* box-sizing: content-box; */
         background-image: url(/img/graffiti-bigbird.jpg);
         border: dotted 14px #000;
         color: red;
         font-family: verdana;
         font-weight: bold;
         font-size: 52px;
         text-shadow: white 3px 3px;
         padding: auto;
         margin-bottom: 1em;
         display: flex;
         justify-content: center;
    }
    .border {
         background-clip: border-box;
    }
    .padding {
         background-clip: padding-box;
    }
    .content {
         background-clip: content-box;
    }
    .text {
         background-clip: text;
         color: transparent;
         text-shadow: none;
    }
  </style>