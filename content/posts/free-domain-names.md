---
title: "Free Domain Names"
linktitle: "Domain Names"
date: 2019-11-03T07:51:05Z
draft: false
tags: ["free stuff"]
categories: ["tutorial", "static sites"]
summary: "Not much yet, but how to renew .tk names is useful."
---

For the most part you have to pay for domain names and you can get good deals at sites like [namecheap.com](https://www.namecheap.com/) and [porkbun.com](https://www.porkbun.com)

However Freenom ([www.dot.tk]) supplies free domain names with the following top level domains (TLDs):

1. .tk is the TLD for Tokelau (a territory of New Zealand)
2. .cf is the TLD for the Central African Republic
3. .ga is the TLD for Gabon
4. .gq is the TLD for Equatorial Guinea
5. .ml is the TLD for Mali



## Renewing a .tk domain

You have to wait until 14 days before domain expires before you can renew it. Then you just login to freenom.com click on the first menu item *services* and choose 

![Freenom menu](img/freenom-menu.png "Click on renew domains")


## What's wrong with freenom free names?

There are numerous reports of people having their domain revoked without warning. This usually happens when a site has built up considerable traffic. The name is then sold to advertisers.

However for low traffic sites it shouldn't be a problem. Just set yourself a reminder to renew within the 14 day period.


## Alternatives

If you want a cheap domain name, rather than a free dodgy one, try

1. [Namecheap.com](https://www.namecheap.com)
2. [Porkbun.com](https://www.porkbun.com)
3. [Purely.domains](https://purely.domains/) are good for UK domain names.

These offer good services at low cost.

## Links
1. [Freenom](http://www.freenom.com/)
2. [Why You Shouldn't use .tk domains](https://www.techelex.org/why-not-use-dot-tk-domain/)