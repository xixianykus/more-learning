---
title: Projects
date: 2019-05-14
draft: true
tags: ["private"]
summary: "Some project ideas to learn new things."
---

See [Questions](/questions/) too.

Here are some task oriented projects that will help learn new stuff.

Some things to tryout

- If you upload a site by drag'n'drop to Netlify how easy is it to update? Netlify CLI ?
- Update a site on GitHub / GitLab then how to update the change locally. (Learn Enough git guide pt4)
- If you update your site via Netlify CMS will that update the git repo too?
- **BIG ONE** Create a blog site w Netlify CMS and Hugo via GitLab w VS Code

## Hugo specific

- How to create an uploadable gallery?
- Batman site
- Cafe #9 site
- guy site
  

## The BIG ONE
- create logo
- find a site to copy
- define different site sections (front page, page, blog page)
- add SSG code (Jekyll or Hugo)
- upload to GitHub or GitLab
- add content manager, prose.io or netlify-cms (can they create new pages?)


## Questions
These questions could be answered by trying different things

1. How does it work when a project is configured as an SSG? This applies to both:
   1. GitLab
   2. GitHub w Jekyll
   3. Netlify - there’s a box to fill in the details
   
       And which bits of an SSG do you need to upload fpr the above?
2. What’s the best way / or service to use forms on a static site? Netlify has it’s own forms with a limit (does that include spam attempts?)
3. Using DNS on Netlify - is better to use Netlify’s DNS or link to the Freenom ones? Can the Freenom ones still support https?
4. How does:
   1.  prose.io work with a Jekyll site on GitHub?
   2.  netlify-cms work with a configured SSG?
5. Does GitHub’s Jekyll support offer anything that GitLab can’t match?
6. How is Bitbucket? Does it have any advantages over GitLab and GitHub?

## Further things to learn

Check out the [Frontend developer’s handbook](https://frontendmasters.com/books/front-end-handbook/2019/)

