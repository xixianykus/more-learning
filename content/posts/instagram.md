---
title: "Instagram"
date: 2020-03-09T10:08:00Z
draft: true
tags: ["instagram"]
categories: ["tutorial", "social media"]
summary: "Some info on how to set up an Instagram account annonomously"
---


## Setting up a new account

To do this anonomously can be tricky. If you are using a VPN you will probably run into issues and either fail or be asked to confirm via a text to a mobile phone. Online phone numbers will not work if they've previously been used to set up an IG account.

[One method](https://www.blackhatworld.com/seo/i-cant-create-instagram-accounts-something-went-wrong-try-again-later.873249/page-5) said to work is to use the Facebook Lite app and set up an FB account using that. Then use the FB account to set up the IG account.

## Shadow Bans

IG won't even admit to shadow banning people so it's hard to make much sense of it. If you use a banned tag you may be shadow banned. To see if any of your last posts use banned tags you can use the free shadowban tester at [triberr.com/](https://triberr.com/). This will check your last 10 posts.

Fixes claimed to work include:

1. waiting it out (might take a couple of months though). 
2. Writing repeatedly to IG about it (don't use the word shadowban though).
3. Logging in then out of your account.
4. Changing your password (can work if you are banned from commenting)


## Adding paragraphs to posts

This is problematic for some reason. The latest fix by IG is to allow this with a single carriage return between paragraphs.

The older method was use a punctuation on each line (typically a full stop) or the special spacing characters that can be found online (less messy).