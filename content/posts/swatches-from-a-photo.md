---
title: "Swatches From a Photo"
date: 2023-06-01T05:32:40+01:00
draft: false
tags: ["photoshop"]
categories: ["design", "photoshop"]
summary: "How to create a unique set of swatches from a photo."
---

NB. This works for Photoshop 2019. The process has changed slightly in later versions of Photoshop.

## Find a photo

First find a photo with a pleasing range of colours you like. It could be anything from a monochrome image to something highly contrasting. You may wish to tweek the colours at this point by increasing contrast, saturation or something else.


## Indexed color mode

Open the image in Photoshop and change the colour mode to indexed colour. 

![original photo](photo.jpg)

`image > mode > indexed color..`

A dialogue will pop up with a range of choices. Reduce the number of colours to a reasonable number. Err on the high side because you can always delete unwanted colours afterwards.

![indexed color dialogue](indexed-color.png)

Your image will now look something like this:

![indexed color dialogue](indexed-color-image.png)

Next go to `image > mode > color table`. From here you can choose `Save` to save the color table. This will save them in the `.ACT` format for color tables. NB. This is differnent to the swatches format which is `.ACO`. Close the color table dialogue box.

## Load the new swatches

Open your swatches panel and click `Load swatches` from the flyout menu. We need to change the file type from `.ACO` to `.ACT` to be able to see our color table file. Just change the file type dropdown to `.ACT` - its located just after the file name. Choose the file and you have swatches based on the colour of your photo.

## Refining your selection

If you have a high number of swatches you might want to delete some. This is done from the Preset Manager. Again load your `ACT` file using the method above to *see* the file first. You can then select swatches by clicking and `Ctrl` and `Shft` clicking them to select multiple swatches then clicking on `Delete`.


## Using the mosiac filter

Another way to create colours from a photo is to apply the mosiac filter to the photo, `Filter > Pixelate > Mosiac`. This will reduce the number of colours and you can easily add colours to the swatches panel. Use the eyedropper tool to select a colour then press the new swatch icon at the bottom of the swatches panel. To skip the name dialogue popping up when creating a new swatch hold down `Alt` key when clicking the new swatch icon.


You can customize the colours further by changing the saturation or lightness/darkness with a hue / saturation adjustment layer before selecting your colours.

![Mosaic filter applied](mosaic.png)