---
title: "Principles of Design"
date: 2021-02-24T17:24:09Z
draft: false
tags: ["graphic design", video]
categories: ["design"]
summary: "A Satori graphics video on the principles of designer"
---

Here are a couple of good vids by Satori graphics. The first explains the importance of the principles, that is why you should know and use them and how they benefit designers.

The second is a brief explanation of 6 rules of layout design.

{{< yt 7d_usbbF2ZU >}}


The 12 principles mentioned are:

1. Balance
2. Contrast
3. Heirarchy
4. Emphasis
5. Proximity
6. Repetion
7. Rythm
8. White space
9. Pattern
10. Movement
11. Alignment
12. Unity

Secondary principles include typography and colour.


Another good video which goes into more detail on **6 Golden Rules of Layout Design You MUST OBEY** is this one.

{{< yt EFf9jBs2yfU >}}

The six principles described are

1. Negative space
2. Proximity
3. Repetition
4. Contrast
5. Alignment - not everything needs to be aligned but choose thoughtfully element/s which are not.
6. Focal points are elements in a composition that capture OR hold a viewers attention

... and don't forget heirarchy.



Links
1. [Six main principles for Graphic design](https://www.wearetrident.co.uk/six-main-principles-graphic-design/)
2. [8 Principles of Design to help you create awesome graphics](https://blog.adobespark.com/2020/10/17/8-basic-design-principles-to-help-you-create-better-graphics/) by Adobe Spark
3. [Design 101 - 8 graphic design basics](https://99designs.co.uk/blog/tips/graphic-design-basics/) Lots of info from 99 Designs