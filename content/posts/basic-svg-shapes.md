---
title: "Basic SVG Shapes"
date: 2022-10-06T07:09:50+01:00
draft: false
tags: ["svg"]
categories: ["tutorial", SVG, Vector]
summary: "How to create simple SVG shapes: circles, rectangles, lines, polygons, polylines and ellipses."
toc: true
css: 
- "#basic-code + p {border: solid 5px currentColor; padding: 1em; margin-top: 1em}"
- ".svg {\n    float: right; \n    margin: 0 0 0.5em 0.5em; \n    outline: dotted 1px #fff9; \n    max-width: 100%\n    }"
- ".cb, h2 {\n    clear:both\n    }"
- "@keyframes spin {\n to {transform: rotate(1turn);}\n   }"
- ".daisy.svg {\n    animation: spin 3s linear infinite both;\n    outline: none;\n    }"
- "#daisy-centre {\n    opacity: 0; \n    transition: all 2s; \n    transform-origin: center center; \n    fill: darkred\n    }"
- ".daisy:hover #daisy-centre {\n    opacity: 1; \n    transform: scale(2.5); \n    fill: white;\n    }"
- ".c-3 {column-count: 3}"
---

Since most SVG images are likely to be created in image editing software such Illustrator or Inkscape learning how to create basic SVG shapes might seem pointless. If SVG's are created and exported from image editing software is there any point to learning how to create SVG's by coding directly? 

There are several good reasons to learn about SVG's this way.

1. It gives a a good insight into the inner workings of SVGs and you'll end up with more power and deeper understanding of the format.
2. Creating shapes this way is not hard to learn. Just like learning a bit of extra HTML.
3. For some things it can be quicker than opening an image editor, creating, exporting and optimizing. These shapes maybe basie but by combining several shapes  you can quickly build up [relatively complex images](#simple-line-use-case).


## Basic code


**IMPORTANT:** The code below is for inline SVG's, that is code written directly into your HTML page. To create an SVG file, eg. image.svg, you need to tell the browser this is an XML file by adding `xmlns="http://www.w3.org/2000/svg"` to the first `<svg>` tag. If you are doing an icon that has `xlink:href` attributes, you should also add the `xlink namespace`.  
So:  
`<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ....`


All of these will use the same basic SVG code: opening and closing SVG tags with a height and a width.

```svg
<svg width="400" height="400">
  <circle> ... code ... </circle>
</svg>
```

As SVG is an XML language all tags must be closed: You can use `<circle></circle>` however it's less verbose simply to use a closing slash:

```svg
<circle ...code... />
```

## Circle

<svg class="svg" width="200" height="200"><circle r="180" /></svg>A circle is created with a circle tag. To give it size use the radius attribute `r="180"`. Pixels are used by default.

```svg
<svg class="svg"  width="200" height="200">
  <circle r="180" />
</svg>
```

With no `fill` defined black is the default colour. 

(A light border is added with CSS just to show the bounds of the SVG)
{.cb}


<svg class="svg"  width="200" height="200"> <circle r="90" cx="100" cy="100" fill="orangered" /> </svg>Here centre attributes, `cx` and `cy` are used to position the centre of the circle and a fill attribute is used to colour the shape.

```svg
<svg class="svg"  width="200" height="200">
  <circle r="90" cx="100" cy="100" fill="orangered" />
</svg>
```

## Rectangles


<svg class="svg"  width="200" height="200"> <rect x="10" y="10" width="180" height="90" fill="cadetblue" /> </svg>At first learning how to create rectangles with SVG might not seem worth it as it's easy to create the same with a `<div>` and some CSS. However they're useful when using multiple shapes in a single SVG.

```svg
<svg class="svg"  width="200" height="200">
  <rect x="10" y="10" width="180" height="90" fill="cadetblue" />
</svg>
```

Instead of `cx` and `cy` of a `<circle />` the `x` and `y` attributes are used to define the position of the top left corner of the rectangle and `width` and `height` are used to create the shape.

<svg class="svg"  width="200" height="200"> <rect x="10" y="10" rx="15" width="180" height="150" fill="rebeccapurple" /> </svg> You can easily add rounded corners using the `rx` attribute. There's an `ry` attriubute too if you wish to specify a different level ofcurvature on the y axis.

```svg
<svg class="svg"  width="200" height="200">
  <rect x="10" y="10" rx="15" width="180" height="150" fill="rebeccapurple" />
</svg>
```


## Lines


<svg class="svg"  width="200" height="200"> <line x1="25" y1="70" x2="189" y2="189" stroke="black" stroke-width="25" /> <line x1="25" y1="170" x2="189" y2="170" stroke="blue" /> </svg> 
A line in SVG is simply a two points joined with a straight line. This makes the syntax very simple. The first point is defined with `x1` and `y1`, the second with  `x2` and `y2`. A stroke attribute must be used to make the line visible. The thickness can be varied with the `stroke-width` attribute though is not defined it will be 1px.

This example has two lines

```svg
<svg class="svg"  width="200" height="200">
  <line x1="25" y1="70" x2="189" y2="189" stroke="black" stroke-width="25" />
  <line x1="25" y1="170" x2="189" y2="170" stroke="blue" />
</svg>
```

NB. The second element in the code is always rendered *above* the first. The last element will be on top of all preceding elements.

### Simple line use case

<svg class="daisy svg" width="300" height="300">
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(15, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(30, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(45, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(60, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(75, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(90, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(105, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(120, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(135, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(150, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(165, 150, 150)" />
    <circle id="daisy-centre" cx="150" cy="150" r="35" fill="white" />

</svg> Here is an example of a more complex SVG with the `<line />` element. It may look like a lot of code but first one line is drawn horizontally across the centre of the image. The rest of the lines of code are mere duplicates with an added `transform="rotate()"`. Each line has a different angle of rotation.

It's probably no quicker creating an image like this in graphics software than it is just writing and duplicating the lines of code.

The `transform="rotate()"` takes 3 values. The first is the angle of rotation. The second two are the x and y values of the centre of rotation.

The animation is provided by applying some CSS to the svg and it's `circle` element which has an `id` of `daisy-centre`.

```svg
<svg class="svg" width="300" height="300">
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(15, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(30, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(45, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(60, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(75, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(90, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(105, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(120, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(135, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(150, 150, 150)" />
    <line x1="0" y1="150" x2="300" y2="150" stroke="yellow" stroke-width="16" transform="rotate(165, 150, 150)" />
    <circle id="daisy-centre" cx="150" cy="150" r="15" fill="white" />
</svg>
```

NB. This can be written more consisely. See [Grouping](#grouping) below.

The CSS used here is:

```css
/* create the animation */
@keyframes spin {
 to {
    transform: rotate(1turn);
    }
}

/* add the animation the the svg element */
.daisy {
    animation: spin 3s linear infinite both;
    }

/* setup the hover effect for the centre element */
#daisy-centre {
    opacity: 0; 
    transition: all 2s; 
    transform-origin: center center; 
    fill: darkred
    }
.daisy:hover #daisy-centre {
    opacity: 1; 
    transform: scale(2.5); 
    fill: white;
}
```

## Polygons

<svg class="svg"  width="200" height="200"> <polygon points="12,2 12,200 180, 100" /> </svg>
A polygon must have at least 3 points but can have have as many as you like. The points are defined in the `points` attribute. This is a syntax used by *polylines* too. Each point is a comma separated pair for x and y values and each point is separated by a space: `23,189 130,189`

```svg
<svg class="svg"  width="200" height="200">
  <polygon points="12,2 12,200 180, 100" />
</svg>
```

## Polylines

<svg class="svg"  width="100" height="100"> <polyline points="12,2 12,100 80, 50" /> </svg> Polylines are kind of a combination of *lines* and *polygons*. In fact they can create the same shapes as polygons when there is no stroke applied.

```svg
<svg class="svg"  width="100" height="100">
  <polyline points="12,2 12,100 80, 50" />
</svg>
```


<svg class="svg" height="200" width="400"> <polyline points="10,10 350,10 320,180" stroke="red" stroke-width="10" fill="#ccc" /> <polygon points="18,160 80,40 150,140" stroke="blue" stroke-width="10" fill="#ccc" /> </svg>Here are a polyline in red and a polygon with a blue stroke. The difference is the fact that with the *polyline* the path is not closed.

As you are unlikely to want such an effect the polyline is generally just used for lines but for no fill it must be set to: `fill="none"`.

```svg
<svg class="svg" height="200" width="400">
  <polyline points="10,10 350,10 320,180" stroke="red" stroke-width="10" fill="#ccc" />
  <polygon points="18,160 80,40 150,140" stroke="blue" stroke-width="10" fill="#ccc" />
</svg>Here are a polyline in red and a polygon with a blue stroke.
```

## Ellipses

<svg class="svg" width="200" height="200">
  <ellipse cx="100" cy="100" rx="100" ry="50" fill="lightblue" />
  <line x1="100" y1="100" x2="200" y2="100" stroke="#111" stroke-width="2" />
  <line x1="100" y1="100" x2="100" y2="50" stroke="#111" stroke-width="2" />
</svg>

W3 Schools is a great reference for SVG:

> An ellipse is closely related to a circle. The difference is that an ellipse has an x and a y radius that differs from each other, while a circle has equal x and y radius

Just like a circle it has a `cx` and `cy` attributes to locate the centre. The only diffence is a second radius value. The `rx` value is the distance from the centre horizontally. The `ry` value is the distance vertically.

Basic syntax is:

```svg
<svg width="200" height="200">
  <ellipse cx="100" cy="100" rx="200" ry="50" fill="lightblue" />
</svg>
```

## Text

<svg class="svg" width="200" height="200"> <ellipse cx="100" cy="100" rx="100" ry="50" fill="fuchsia" /> <line x1="100" y1="100" x2="200" y2="100" stroke="#fff" stroke-width="2" /> <line x1="100" y1="100" x2="100" y2="50" stroke="#111" stroke-width="2" /> <text x="120" y="120" fill="#fff" font-family="oswald">x axis</text> <text x="110" y="80">y axis</text> </svg>
Simple text can be added using the `<text></text>` tags. Like `rect` they have starting `x` and `y` coordinates. The default fill is black.

```svg
<svg class="svg" width="200" height="200">
  <ellipse cx="100" cy="100" rx="100" ry="50" fill="fuchsia" />
  <line x1="100" y1="100" x2="200" y2="100" stroke="#fff" stroke-width="2" />
  <line x1="100" y1="100" x2="100" y2="50" stroke="#111" stroke-width="2" />
  <text x="120" y="120" fill="#fff">x axis</text>
  <text x="110" y="80">y axis</text>
</svg>
```

The follow attributes can added to `<text>` tags or declared in CSS:

- font-family
- font-style
- font-weight
- font-variant
- font-stretch
- font-size
- font-size-adjust
- kerning
- letter-spacing
- word-spacing
- text-decoration
{.c-3}


## Grouping

<svg class="daisy svg" width="300" height="300"> <g stroke="green" stroke-width="10" fill="none" > <line x1="0" y1="150" x2="300" y2="150" stroke="skyblue" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(15, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(30, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(45, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(60, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(75, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(90, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(105, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(120, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(135, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(150, 150, 150)" /> <line x1="0" y1="150" x2="300" y2="150" transform="rotate(165, 150, 150)" /> <circle cx="150" cy="150" r="140" /> </g> </svg> You can group the elements of an SVG together and then apply fills, strokes and stroke-widths to the whole group. This saves time and code and makes editing simpler and faster too. If you want to change something to one or more elements in the group you can edit the element and the changes will over ride those set in the group. Here the first line has a different colour though it still has the same property value for it's `stroke-width`.

```svg
<svg class="daisy svg" width="300" height="300">
    <g stroke="green" stroke-width="10" fill="none" >
      <line x1="0" y1="150" x2="300" y2="150" stroke="skyblue" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(15, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(30, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(45, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(60, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(75, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(90, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(105, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(120, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(135, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(150, 150, 150)" />
      <line x1="0" y1="150" x2="300" y2="150" transform="rotate(165, 150, 150)" />
    <circle cx="150" cy="150" r="140" />
    </g>
</svg>
```

## Examples

A space to tryout some example SVG's made by hand.

<svg viewBox="0 0 500 300" class="clouds">
  <style>
    ::selection {
      background-color: chocolate;
    }
    .clouds {
      background-color: skyblue;
      width: 100%;
    }
    .circ {
      fill: white;
      /* stroke: blue; */
    }
    .text {
      font-family: jost, 'segoe ui', sans-serif;
      font-weight: 400;
      text-shadow: 1px 1px 3px 10px #666;
          }
          .rain {
            stroke: #1113;
            stroke-dasharray: 5 3 6 2 3 4;
          }
  </style>
  <g class="circ">
    <circle r="50" cx="55" cy="150" />
    <circle r="50" cx="105" cy="150" />
    <circle r="50" cx="155" cy="150" />
    <circle r="50" cx="215" cy="150" />
    <circle r="50" cx="265" cy="150" />
    <!-- upper section -->
    <circle r="40" cx="85" cy="100" />
    <circle r="40" cx="125" cy="90" />
    <circle r="40" cx="140" cy="80" />
    <circle r="40" cx="180" cy="90" />
    <circle r="37" cx="230" cy="110" />
  </g>
  <g class="rain">
    <line x1="75" x2="95" y1="210" y2="290" />
    <line x1="95" x2="115" y1="210" y2="290" />
    <line x1="105" x2="125" y1="210" y2="290" />
    <line x1="115" x2="135" y1="210" y2="290" />
    <line x1="125" x2="145" y1="210" y2="290" />
    <line x1="135" x2="155" y1="210" y2="290" />
    <line x1="145" x2="165" y1="210" y2="290" />
    <line x1="155" x2="175" y1="210" y2="290" />
    <line x1="165" x2="185" y1="210" y2="290" />
    <line x1="175" x2="195" y1="210" y2="290" />
    <line x1="185" x2="205" y1="210" y2="290" />
    <line x1="195" x2="215" y1="210" y2="290" />
    <line x1="205" x2="225" y1="210" y2="290" />
    <line x1="215" x2="235" y1="210" y2="290" />
    <line x1="225" x2="245" y1="210" y2="290" />
    <line x1="235" x2="255" y1="210" y2="290" />
    <line x1="245" x2="265" y1="210" y2="290" />
  </g>
  <text class="text" x="30" y="250">The CSS for this is inside the image</text>
  <text class="text" x="30" y="275">Yep, just a bunch of overlapping white circles.</text>
</svg>

## Recap

| shape      | attributes                  | Description                                                                                         | Example use                                                                   |
| :--------- | :-------------------------- | :-------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------- |
| `circle`   | `r`, `cx`, `cy`             | radius or size, x centre and y centre                                                               | `<circle r="90" cx="100" cy="100" />`                                         |
| `rect`     | `x`, `y`, `width`, `height` | coords of top left starting point<br>width and height from there                                    | `<rect x="10" y="10" width="180" height="90" />`                              |
| `line`     | `x1`, `y1`, `x2`, `y2`      | coords of first point<br> coords of the second point                                                | `<line x1="25" y1="70" x2="189" y2="189" stroke="black" stroke-width="25" />` |
| `polygon`  | `points`                    | Each point separated by a space and x and y separated by a comma                                    | `<polygon points="12,2 12,100 80, 50" />`                                     |
| `polyline` | `points`                    | Like `polygon` but strokes are not joined between end points                                        | `<polyline points="12,2 12,100 80, 50" />`                                    |
| `ellipse`  | `rx`, `ry`, `cx`, `cy`      | Takes two radius values, horizontal and vertical.<br>Like a circle is positioned with centre point. | `<ellipse cx="100" cy="100" rx="200" ry="50" fill="lightblue" />`             |

## Links

[SVG links](/links/#svg)
