---
title: "Cloud Based Fonts"
linktitle: "Cloud Fonts"
date: 2020-07-16T09:10:20+01:00
draft: false
tags: ["fonts", "Google", "CDN"]
categories: 
summary: "A look at cloud based fonts and optimization"
---

Using cloud based fonts has two main advantages. Firstly it's usually faster. You only have to set up a few simple links to the head of your documents and/or the CSS. Secondly it will save your site bandwidth. A third possiblity is that with a good cloud service it could speed up your page load times.

My first experience using cloud base fonts was with the [Open Font Library](https://fontlibrary.org/). These proved great for quite some time. It was easy to get hooked on the quick way of adding new fonts to a web site and I liked the fact they weren't one giant ominiscient mega corporation (ie Google). Then one day my sites wouldn't load and FontLibrary.org was gone. It seemed like it was dead for quite some time and I switched to changing all my fonts over to self hosting. This was quite a lot of boring work.

Open Font Library did return and I have used them since but lately their load times have been slow which stalls page loads. So I decided to use Google fonts for some personal projects just to save time. Since few if anyone would visit these small projects Google would not have many/any visitors to track, apart from me.

Google fonts are even easier to use than Font Library and are unlikely to go down for several weeks at a time. Though of course they might suddenly change their service, make it a paid for service or discontinue it entirely. Such is the risk when using external services. But for small personal projects they did seem like the only other free cloud based option out there. They also have a wide range of fonts (900 I last read) giving a great choice of fonts. However interestingly they are certainly missing many fonts available on Open Font Library including a few favourites of mine.

In a statement Google put out they claimed to not be using the data to track people when visiting sites using their fonts. How reliable such a claim is anyone's guess. Google are not an honest company and were finded hundred's of thousands of pounds for tracking mobile phone users *after* those users had turned off tracking in their phones.

Nevertheless, I somewhat begrudingly, descided to go with Google for my small, personal projects that don't get any visitors. And 900 fonts is more than enough for anyone. However when I launched one new site I was dismayed at the loading time. This was hosted on Netlify so I tried several sites hosted there and it turned out this looked like a Netlify problem. The next day things were better but new site was still slow to load. The first thing I thought of was the fonts so I looked at way of optimizing these and came across a great article on optimizing Google fonts on [Smashing Magazine](https://www.smashingmagazine.com/2019/06/optimizing-google-fonts-performance/).

## Optimizing Google fonts

There was a bunch of stuff there that I was not aware of. 

The more obvious things they mentioned were:

1. Reduce the number of fonts and weights
2. Link all your fonts in one link to reduce HTTP requests

But there were other things I hadn't heard of like prefetching and preconnecting. To enable these is so simple: you just add the following two lines to the head of your pages:

1. `<link rel="dns-prefetch" href="//fonts.googleapis.com">`
2. `<link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>`

Another interesting possibility was to use the *text parameter*. The idea here is that if only want the font for a few letters, typically logo type you can use this to just grab those letters rather than the whole font file. Just add `text=CompanyName` to the end of the link where *CompanyName* is the set of letters you wish to use:

`https://fonts.googleapis.com/css?family=Roboto:700&text=CompanyName`

Make sure to add the font weight too unless you're using the regular weight version of the font which is normally 400 weight.


## Easy Fonts CDN

At the bottom of the Smashing Mag article someone had posted a link to a Stack Overflow post about Easy Fonts CDN. I haven't tried this yet but apparently they use the same set of fonts as Google and have an even easier way of accessing these. You simply choose the font you want and link to the CSS file. They even have a CSS file for the whole lot of fonts if you prefer. From there you use class names in your CSS to use your fonts.








## Links

1. [Smashing Mag](https://www.smashingmagazine.com/2019/06/optimizing-google-fonts-performance/) on Google font optimization
2. [Stack Overflow on Easy Fonts CDN](https://stackoverflow.com/questions/29091014/how-do-i-leverage-browser-caching-for-google-fonts/57073920#57073920)
3. [Easy Fonts CDN](https://pagecdn.com/lib/easyfonts)
4. [Open Font Library](https://fontlibrary.org/)