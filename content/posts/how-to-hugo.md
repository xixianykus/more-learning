---
title: "How to setup a new Hugo site"
linktitle: "New Hugo site"
date: 2019-10-28T17:53:33Z
draft: false
tags: ["hugo"]
categories: ["tutorial", "static sites", "hugo"]
summary: "Terse guide to setting up a Hugo site using the blank theme"
---

So a brief guide on how to do all this.

## Installation of Hugo

This is easy.

1. You can ignore all the stuff about package managers and simply download and unzip the zip file. You can put the contents anywhere. It does not need to go in the programs folder.
2. Then you set the path to the folder where you've put it. In Windows press the Windows key and type **env**. Click on the **environment variables** button. You can add it to either the top box for one user only or the bottom for all users. Either/or then click on path, click *edit* and add the path for hugo.exe.
3. Test your install by typing **hugo version** and/or **hugo help** from the command line.

That’s it done. 

## Creating your first site

From the command line navigate to the folder where your web sites are. Think of a name for the new site, eg *mySite*. Then type:

**hugo new site mySite**

This will create the folder *mySite* with all the files and folder structure to start.

N.B. If you want to add the Hugo site structure to an existing folder (maybe one with a .git folder for example) you need to add **--force** to the command:

**hugo new site myNewSite --force**

## Adding a theme

This is where things can go badly wrong. Lets keep it really simple and add the [blank](https://themes.gohugo.io/blank/) theme.

First you need to initalize git:

`git init`

Then download the theme: 

`git submodule add https://github.com/vimux/blank themes/blank`

Using `submodule` is better than using `git clone` here. When you upload your project the the theme will be missing and instead make use of the url. If the theme is updated then this updated version will be used.

To use the theme you can edit your config file (see below) OR start Hugo with the -t or --theme parameter:

`$ hugo -t "ThemeName"`

## Set up the config file

In the root of the project is a file called **config.toml**. There's not much in the default config file so you need to set up. The first place is to read through the README file with your theme. Go through that to configure your theme and don't forget to add `theme = "themename"` near the top so Hugo knows to use that particular theme.

Another way is to look at the config.toml file that comes with theme and use the settings from there. There isn't one in the blank theme. I suppose it's too simple to need one. Instead there is a **theme.toml** file. Copy the contents and paste into your **/config.toml** file.

Make sure you have a line telling Hugo which theme to use. It goes at, or near, the top:

`theme = "blank"`

## Add content

**hugo new posts/firstpost.md** will create a sub-folder of the content directory and add a file called firstpost.md. This file will contain front matter:
```yaml
---
title: "firstpost"
date: 2019-10-28T17:53:33Z
draft: true
---
```

## Seeing it working

Launch the built in server with:
`hugo server -D`

The `-D` will use draft mode since this post has it's draft set to true. (change to *false* if you want it to show up without launching the server in draft mode.)

Go to your browser and type in **localhost:1313** to see the site.

## Add some CSS

If you go to the *themes/blank/layouts/_default/baseof.html* you can see the main html of your documents. 

The CSS is put in static/css/style.css. You can either change this if you want or just leave it. This folder/file is found in the theme's static folder. In this *blank* theme it is blank. You could edit it there but any update of the theme will overwrite it. So better to create the folder/file under the whole site's /static folder. Edit your CSS from there and you're away.

## Customize the HTML

Like the CSS above rather than change the code in */themes/blank/layouts* folder, instead copy or write from new those folders in the */layouts* folder. Use the same folder heirarchy though.

And so here endeth the first lesson. Obviously much more to know but this is a pretty good base to get you started.

Next up what are all the folders for?

[folders](/posts/hugo-folders/) - like that's gonna work?

## Links
1. [Official quickstart guide](https://gohugo.io/getting-started/quick-start/)
2. [Mike Dane’s Video series]()
3. [Julian Knight’s pages on Hugo](https://it.knightnet.org.uk/kb/hugo/) - he made the switch from Jekyll.
dogworm
