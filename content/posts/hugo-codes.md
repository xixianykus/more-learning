---
title: "Hugo Variables and Functions"
title: Variables & Functions
date: 2019-11-02
draft: false
tags: ["hugo"]
categories: ["tutorial", "static sites", "hugo"]
summary: "Some of Hugo’s more useful variables and functions."
---

Some coding for Hugo

## Variables

Variables can be stored and declared in the config file, layout files and page frontmatter.

But they can only be used inside the template files, those inside `/layouts/`

1. `{{ .Site.Title }}` - title of the web site
2. `{{ .Site.BaseURL }}` - for the home page etc.
3. `{{ .Date }}` - 2019-11-02T18:57:57Z
4. `{{ .URL }}` - produces the url of the page

## Custom Variables in frontmatter - `.Params`

Variables can be declared in the front matter. For example:

```
---
cities: london
---
```

To access these use `.Params`

`{{ .Params.cities }}`

These variable can be used in html and css files (but how?)

### Custom variables elsewhere

To create a custom variable in a template use the `:=`

`{{ $newVariableName := "Geese in shoes" }}`

## Full list

The full list of variables are at: [www.gohugo.io/variables](https://gohugo.io/variables/)

## Functions

Declared in a similar way to variables there are many built in functions.

The basic syntax is: `{{ function funcName1 param1 param2 }}`


An important one is: 

``````go-html-template
{{ range .Pages }}

{{ end }}
```

So **range** is the function and **.Pages** the parameter (which refers to all of the pages in the current section) and something can be added between too like another variable like **.Title**

```{{ range .Pages }}
  {{ .Title }}<br>
{{ end }}```

The full list of functions are at: [www.gohugo.io/functions](https://gohugo.io/functions/)