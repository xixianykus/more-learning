---
title: "Random CSS 1"
date: 2019-12-28T07:42:27Z
draft: false
tags: ["css"]
categories: ["tutorial", "css"]
summary: "min-content, stacking orders, object-fit, bg clip, transform-origin, transform-box"
---

- [min-content](#min-content)
- [Stacking orders](#stacking-orders)
  - [z-index](#z-index)
  - [HTML](#html)
  - [SVG](#svg)
  - [CSS Multiple backgrounds](#css-multiple-backgrounds)
- [object-fit](#object-fit)
- [Pointer Events](#pointer-events)
- [::selection](#selection)
- [scroll behaviour](#scroll-behaviour)
- [Quick dark mode](#quick-dark-mode)
- [background-clip](#background-clip)
- [transform-origin and transform-box](#transform-origin-and-transform-box)
- [Links](#links)

## min-content

`min-content` is a value which came from CSS Grid spec possibly. But whatever it's use goes beyond that.

```css
h1 {
    width: min-content;
}
```

This will make sure that width of the h1 element will be only as wide as it's longest word, or part word if there is hyphenation.

```html
<figure>
    <img src="img/myimage.jpg>
    <figcaption>Some words about my image</figcaption>
</figure>
```

In the above code you typically don't want the words to extend wider than the image. By setting the width of the figure element to `min-content` nothing can be wider than the widest object which is likely to be the image.

THere is also `max-content` which does the opposite, extending text as wide as possible. This seems less useful as it's typically a default for most elements.

In CSS grid use these .... tbc.


## Stacking orders

### z-index

REMINDER: Setting the layering order using `z-index` properties only works if the element is not set to the default `position: static`.

Without a `z-index` declaration the layering order is different in HTML, CSS multiple backgrounds and SVG. More on [MDN docs](https://developer.mozilla.org/en-US/docs/Web/CSS/z-index).

### HTML

With HTML the first element in the DOM is at the bottom and will be covered by subsequent elements.

```html
<div class="html">
    <div class="pink">top in DOM</div>
    <div class="green">2nd in DOM</div>
    <div class="blue">last in DOM</div>
</div>
```

Will give this stacking order.

<div class="html">
    <div class="pink">top in DOM</div>
    <div class="green">2nd in DOM</div>
    <div class="blue">last in DOM</div>
</div>

### SVG

This SVG has the same stacking order as the HTML above with pink listed first, blue last.

```svg
<svg>
    <circle cx="75" cy="75" r="70" fill="blueviolet"></circle>
    <circle cx="100" cy="100" r="70" fill="yellowgreen"></circle>
    <circle cx="125" cy="125" r="70" fill="#2b74e2"></circle>
</svg>
```
So again pink came first and blue was last:

<svg>
    <circle cx="75" cy="75" r="70" fill="blueviolet"></circle>
    <circle cx="100" cy="100" r="70" fill="yellowgreen"></circle>
    <circle cx="125" cy="125" r="70" fill="#2b74e2"></circle>
</svg>

### CSS Multiple backgrounds

In CSS the order is the opposite of HTML and SVG. The first image listed is the one on the top of the stack. Here the red circle is written first to make it appear on the top of the bird image.

```css
    .css {
    background-image: url(../../img/redcircle.svg),
                      url(../../img/graffiti-bigbird.jpg);
}
```
gives

<div class="css"></div>


## object-fit

This is property it typically used for images or video and defines how they fit their containing elements. 

```css
    img {
        object-fit: contain;
    }
```

`object-fit` can take the following values:

| value      | what it does                                                                                             |
| ---------- | -------------------------------------------------------------------------------------------------------- |
| contain    | As background contain, it fills the element with the image without changing the aspect ratio or clipping |
| cover      | Similar to contain but clips the image if necessary to completely fill the element                       |
| fill       | Like cover in that it fills the space but rather than clipping the image it stretches it to fit          |
| none       | no resizing is done                                                                                      |
| scale-down | Uses whichever is smaller: contain or none                                                               |
  

Here's an example of `object-fit: contain`

<div class="of"><img src="../../img/graffiti-bigbird.jpg"></div>

Here's the CSS. Note width and height are different from the actual dimensions of this image which is 500 x 386.

```css
img {
    background-color: #ccc;
    width: 800px;
    height: 380px;
    object-fit: contain;
}
```

See more about *object-fit* [here](/playground/object-fit/)


## Pointer Events

```css
button {
    pointer-events: all;
}
```

Useful to disable a button or other element in animations when fading out to `opacity: 0;`

## ::selection

The selection pseudo selector targets text when it is selected by the user. Useful if you want to change the background and/or font colour.

## scroll behaviour

Scroll behaviour is useful for internal page links. When a link jumps to place further down the page the auto scrolling is smoother.

```css
html {
    scroll-behaviour: smooth
}
```

## Quick dark mode

You can invert all the colours on the site with:

```css
html {
    filter: invert(1) hue-rotate(180deg);
}
```

The *hue-rotate* will correct the colour change after the invert. 


## background-clip

When you clip a background to the text you must also reduce the fill of the text to 0:

```css
h1 {
    background: linear-gradient(to right, blue, fuchsia);
    backround-clip: text;
    -webkit-text-fill-color: transparent;
    display: inline-block;
}
```

Despite the -webkit prefix this works in all browsers 😀.

The `display: inline-block;` makes the gradient fit the length of the text.

<div class="text">A linear gradient</div>


## transform-origin and transform-box

`Transform-origin` is useful to set the position of the centre of rotation. However if animating just one part of a larger svg you might need `transform-box` too:

```css
.wheel {
    animation: spin 1s infinite linear;
    transform-origin: center;
    transform-box: fill-box;
}
```

The `transform-box` is widely supported. It can take the following values:

- transform-box: content-box;
- transform-box: border-box;
- transform-box: fill-box;
- transform-box: stroke-box;
- transform-box: view-box;

See more on [DevEd's tutorial](https://youtu.be/gWai7fYp9PY?t=788) or [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/transform-box).



## Links
1. [MDN on object-fit](https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit)





<style>
    .html,
    .css,
    svg {
        position: relative;
        width: 400px;
        height: 200px;
        background-color: #ccc;
    }

    .html>* {
        position: absolute;
        width: 180px;
        height: 115px;
        color: whitesmoke;
        padding: 0.4rem;
    }
    .pink {
        background-color: blueviolet;
    }
    .green {
        background-color: yellowgreen;
        top: 45px;
        left: 80px;
    }
    .blue {
        background-color: #2b74e2;
        top: 80px;
        left: 170px;
    }

    .css {
    background-color: #ccc;
    background-image: url(../../img/redcircle.svg), url(../../img/graffiti-bigbird.jpg);
}

    .of img {
        background-color: #ccc;
        width: 800px;
        height: 380px;
        object-fit: contain;
    }

.text {
    background-image: linear-gradient(to right, blue, fuchsia);
    /* background-clip: text; */
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    display: inline-block;
    font-weight: bold;
    font-size: 4em;
    text-transform: uppercase;

}

table {
    background-color: #777;
}

tr:nth-of-type(even) {
    background-color: #999;
}

td,
th {
    padding: 6px;
}
</style>