---
title: "Home and Single pages"
linktitle: "Hugo home page"
date: 2019-10-30T18:36:38Z
draft: false
tags: ["hugo", "templates", video]
categories: ["Development", "templates", hugo]
summary: "Brief notes on the home page template in Hugo plus video."
---


There are two types of page in Hugo: list pages and single pages. These live in the `layouts/_default` folder.

A special kind of single page is the home page. This has it's own template (by default it is a list page).

This lives just above the other templates in `layouts/` and is called `index.html`. The content for this is stored in a file in the root of the content folder `content/_index.md`.

## Other directory specific templates

To create template specific to a certain kind first keep all those files in the same `/content/sub-folder`

Then in the layouts folder create a folder with the same name: `/layouts/sub-folder`

{{< yt ut1xtRZ1QOA >}}