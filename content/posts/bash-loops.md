---
title: "Bash Loops"
date: 2023-05-28T07:34:53+01:00
draft: false
tags: ["bash", terminal, shell]
categories: ["develpment"]
summary: "How to create a simple for loop with bash to change the file name extension of multiple files."
---

There are several kinds of loops in bash: `for`, `while` and `until`. Here we'll be using a `for` loop to change the file name extension of multiple files.

When you write a command beginning with `for` bash expects several lines: its creating a short script on the command line. After each line you press `Enter` which starts a new line. When you're ready to run the list of commands you start another new line and type `done`. Pressing enter will then execute the script.


Here is the basic code:

```bash
for file in *.html
do
  mv "$file" "${file%.html}.txt"
done
```

So `for` begins writing out the for loop. The next word `file` could be anything you like. It's declaring a new variable called *file*.

The key work `in` tells bash where to look. In this case all files ending with the `.html` extension.

The next part begins with `do` and this is what we want bash to do with each *file*.

To rename a file we use the `mv` or move command. We recall our `file` variable by prefixing it with a `$` sign. The quotes are optional. If there are no spaces in the file names they're not required. You can use any command here, for instance if working within a git repo you might prefer `git mv`.

> The percent-sign-within-bracket construct strips characters off the end.
>
> There is also a hash-within-bracket construct that strips characters off the beginning.

So after the percentage sign is the `.html`. This is bit is stripped away. And outside the brackets is the new extension applied to every file.

Finally once the *script* is written out type `done` and press enter to execute it. A quick `ls` will confirm the files are renamed as expected.


## An alternative

Here is an alternative that does not require the system to have the rename program.

```bash
for file in *.html; do
    mv "$file" "$(basename "$file" .html).txt"
done
```

## Links

1. [Stack Overflow question](https://stackoverflow.com/questions/1224766/how-do-i-rename-the-extension-for-a-bunch-of-files/1225236#1225236)
2. [How to Geek Bash for loops](https://www.howtogeek.com/815778/bash-for-loops-examples/)