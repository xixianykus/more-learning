---
title: "Hugo Folders"
date: 2019-10-28
draft: false
tags: ["hugo"]
categories: ["tutorial", "static sites", "hugo"]
summary: "Hugo folders are laid out in a certain way for specific functions. This is what each does."
---

First thing to notice is that if you put a hyphen in the file name when Hugo converts that to a title the hyphen is replaced as a blank space.

Right onwards. This post is about the folder structure in Hugo. What are they all for? What do they do?

The structure, alphabetically, is:

- archetypes
- content
- data
- layouts
- resources
- static
- themes
- *config.toml* (file)

## archetypes

When you create a new page using the **hugo** cli files in here contain whatever code you want to be inserted. So rather than starting with a blank page you can start with a frontmatter block for instance. This is only for use with content pages.

## content
This is where all content goes. For a blog start with a folder called *posts* and put all your blog posts in there. Putting files in the root of this folder, like say an index.md for home page, will typically mess everything up and lead to disappearing pages.

## data
This is for....

## layouts
Is where the template files go. If you are using a theme this can remain empty. But if you want to change something copy the file/s here and modify that version.

Typically there are two folders: `_default` and `partials`

## resources
This is for....

## static
Here is where files that are not to be processed by Hugo go. CSS and JS files are good example. They will of course be read by Hugo when it runs but they won’t be changed in anyway. So they're considered *static*.

## themes
This is obviously where themes go. If you're not using a theme you can delete this folder.

The name of the theme's specific folder is used in the config.toml `theme = "myTheme"` to tell Hugo which theme to use.

## config.toml file
This configuration file for Hugo. Can contain anything from just a few lines to many.


