---
title: Online Services
date: 2019-05-01
categories: ["Development"]
tags: ["hosting", "freebies"]
draft: false
summary: "List of online services for web hosting and dev"
---

There's so much stuff online it would be easy get carried away here.

This page is **specifically** for services that exist online that may be useful.

## CMS services
These are directly used for web websites
1. Netlify CMS
2. [Prose.io](http://prose.io/#about) CMS designed to work with Jekyll on GitHub. Advanced support for markdown.
3. [Contentful](https://www.contentful.com/) works w Gatsby SSG. Free up to a point.
4. Forrestry.io - not even totally sure what this does. Another CMS?

## Webhosting
- [Infinity Free](https://infinityfree.net/)
- [GitHub](https://github.com/) Huge git site. Now owned by Microsoft.
- [GitLab](https://gitlab.com/) Similar to GitHub but open source.
- [Netlify](https://netlify.com/) Excellent service, 100gb of bandwidth per month.
- [Surge](https://surge.sh/ "Surge is free to use. Upgrade to bolster it with server-side features.") - *Static Webpublishing for Front-End Developers* 
- [Amazon S3](https://davidwalsh.name/hosting-website-amazon-s3)
- [Google Firebase](https://firebase.google.com/docs/hosting/)
- [Neocities](https://neocities.org/) Not sure how this works though it mentions *Jekyll, Hugo* and *Gatsby*
- [Updog](https://updog.co/) works with either Google Drive or Dropbox for static sites. Free, **Up to 500 requests per month**.
- [Freehostia](https://freehostia.com/) free hosting with one-click Wordpress etc. though you need a paid for domain name (not necessarily from them).
- [Design Rope’s list of static site hosts from 2017](https://designrope.com/toolbox/static-web-hosting/)
- [techradar’s list of freehosting](https://www.techradar.com/best/best-free-web-hosting)

## Other
- [Mapbox](http://mapbox.com/) makes it “incredibly easy” to embed maps in a web site. Free for up to 50,000 views per month.
