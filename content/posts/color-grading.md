---
title: "Colour Grading"
date: 2020-03-10T07:10:12Z
draft: false
tags: ["photoshop", video]
categories: ["tutorial", "static sites", "photoshop"]
summary: "How to apply colour grading to an image in Photoshop"
---


There are many different ways to apply colour grading to an image. Each method requires some practice but typically they include adjustment layers of some kind: curves, selective color, color balance etc.

A commonly used combination is blue/teal in the shadows and orange for the highlights/skin. This is because these colours are opposite each other on the colour wheel and so increase contrast and make an image pop.

Typically you start with a neutral image first.


## Selective Color

This technique is from the the excellent [Photoshop Training Channel](https://photoshoptrainingchannel.com/color-tone-photoshop/)

1. Add a selective color adjustment layer
2. Work on blacks first (he added 16% to the black slider for a contrasty look)
3. Still in the black decrease yellow to add blue, decrease magenta to add green and increase cyan.
4. Repeat with the neutrals (he left the magenta slider here and just changed the cyan and yellow sliders).
5. To affect skin tones / highlights go to the Red channel. Decrease cyan to add red, increase magenta a little (eg. 4%), and increase yellows (eg. 15%)
6. Combine the two layers into a smart object then add a Camera RAW filter.
7. Use vibrance to increase saturation while protecting skin tones and already oversaturated colours.
8. He also increased clarity (contast in the midtones) and grain from the fx tab for a film look, and a vignette.

{{< yt pHHupVt_irc >}}



## Colour correction with Curves

Colour grading works much better when the image is colour corrected first.

1. Add a curves adjustment layer
2. Hold down `Alt` and drag the bottom left slider to the R. First parts to go black are the darkest.
3. Repeat with the bottom R slider for the white point. Choose a point where this is some detail. That is not somewhere where the colour is blown out.
4. Use the white eye dropper and click on it.
5. Finally choose the neutral slider to pick somewhere that is a neutral colour, ie. a grey.

The quick way to do this is again in a Curves adjustment layer.

1. Alt + click on the Auto button.
2. Choose `Find Dark & Light Colors`.
3. Use the neutral eye dropper to find a neutral grey.



## Reducing saturation

This can be done with a hue/sat layer. Apply to the whole image or just a part.


## Other methods

include using color lookup tables (LUT) adjument layers

Add gradient map adjustment layer, choose a gradient from photographic presets and switch to overlay/soft light/color blend mode.