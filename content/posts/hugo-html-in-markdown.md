---
title: "Hugo HTML in Markdown"
linktitle: HTML in Markdown
date: 2019-12-26T21:08:19Z
draft: false
tags: ["hugo", "markdown", "html", "goldmark"]
categories: ["hugo"]
summary: "Hugo's Goldmark markdown engine doesn’t render HTML by default. Also: Smartypants for smart quotes."
---

Writing content in markdown is a far nicer experience than writing in HTML code and it's much easier for us humans to read back too. Another benefit is the ability to use code fences to create blocks of code with syntax highlighting in a range of different colour schemes. If a CMS is used then markdown limits the content so that if a hacker gained access via the CMS damage would be limited. They could simply add script tags for instance.

Despite these benefits markdown only provides a subset of HTML and there are times when having more control to the page is what you need. Fortunately there are a bunch of different ways you can make this happen. Which ones you choose will depend on what you want to do and who is using your site.

- CSS selectors
- Adding HTML attributes
- Allowing HTML in markdown
- HTML pages
- Shortcodes
- Render hooks

## CSS selectors

CSS selectors are extremely powerful these days and getting more so all the time. With the right CSS you can select almost anything on the page. Furthermore if you add a unique class or ID to the body element of each page you can target any page in your site from a global CSS file.

```css
body.my-page h2:first-of-type + p {
  /* some css */
}
```

Furthermore all title tags come with unique ID's so can be targetted with `#another-heading`.

##  Adding HTML attributes

You can add HTML attributes including class and ID to block level elements when set in the config file.

```toml
[markup.goldmark.parser.attribute]
  block = true
  title = true
```

You can do this to title elements by default but you have to set `block = true` for other block level elements.

Then to add a class or ID tag you simply add it curlies on the first new line after the element.

```md
Adding class and ID attributes to this markdown paragraph like this.
{.big-text #unique}
```

For headings the attributes go on the same line:

```md
## My h2 Heading {.extra-big .red}
```

These won't work if you have set up render hooks for headings (below).

## Shortcodes

Shortcodes are made for inserting HTML (as well as with Hugo's templating language) into markdown files. You have to create them first but that's easy to do. Create a subdirectory of the `layouts` folder called shortcodes.

```bash
mkdir layouts/shortcodes
```

Create an HTML file in this directory and name it something appropriate: for example `new-section.html`. You can then write whatever HTML you like and insert it into your markdown page using the shortcode notation:

```go-html-template
{{＜ new-section ＞}}
```

There's [much more](https://learnhugo.ml/posts/shortcodes/) to shortcodes than just this but for simple HTML blocks this is very easy.


## Render hooks

Hugo's render hooks allow you to specify exactly how certain HTML elements are rendered.

The elements are:

- headings
- images
- links
- code blocks
- images for RSS
- 

To create render hooks you first create a folder to store them in. If you want these applied to the default layouts of your site create a subfolder of `layouts/_default` called `_markup`:

```bash
mkdir layouts/_default/_markup
```

In this `_markup` folder create an HTML file for each of the above elements you wish to customize.


```bash
layouts/
└── _default/
    └── _markup/
        ├── render-heading.html
        ├── render-image.html
        ├── render-link.html
        ├── render-codeblock.html
        ├── render-image.rss.xml
        └── render-codeblock-bash.html
```

Here's an example of the code in `layouts/_default/_markup/render-heading.html` in this site. The purpose is to add a link to the end of every h2 element.

```go-html-template
{{ if eq .Level 2 }}
  <h{{ .Level }} id="{{ .Anchor | safeURL }}">{{ .Text | safeHTML }}&nbsp;<a class="heading-link" href="#{{ .Anchor | safeURL }}">🔗</a></h{{ .Level }}>
{{ else }}
  <h{{ .Level }} id="{{ .Anchor | safeURL }}">{{ .Text | safeHTML }}</h{{ .Level }}>
{{ end }}
```

## Goldmark Renderer

From Hugo v0.60 there is a new markdown renderer called [Goldmark](https://github.com/yuin/goldmark/#goldmark). Settings for this can be set and changed in the site config file. One important setting to note is that the default setting to allow HTML in markdown is set to false. That is it's not allowed.

To change this use the following variable `unsafe` in the config.toml file to `true`:

```toml {linenos=false}
[markup]
    [markup.goldmark.renderer]
      unsafe = false
```

## Smartypants

[Smartypants](https://daringfireball.net/projects/smartypants/) is another useful feature that automatically changes normal 'quotes' into "smart quotes". This feature is turned on by default (line 10 below: `typographer = true`)

```toml {linenos=false}
[markup]
  [markup.goldmark]
    [markup.goldmark.extensions]
      definitionList = true
      footnote = true
      linkify = true
      strikethrough = true
      table = true
      taskList = true
      typographer = true
```





## Links

1. [Goldmark on GitHub](https://github.com/yuin/goldmark/#goldmark)
2. [Smartypants](https://daringfireball.net/projects/smartypants/)
3. [Hugo docs Goldmark](https://gohugo.io/getting-started/configuration-markup#goldmark)