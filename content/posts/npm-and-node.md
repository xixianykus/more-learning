---
title: npm and node.js
date: 2019-04-11
draft: false
categories: ["Development"]
tags: ["build tools", "package managers", "node.js", "npm", "webpack"]
summary: "Get started with node and modern build tools"
---

![node.js logo](https://miro.medium.com/max/530/1*xeF1flp1zDLLJ4j7rDQ6-Q.png)

I was thinking of learning and writing something about this but there is an excellent article written by Peter Jang:

**[Modern Javascript Explained for Dinosaurs](https://medium.com/the-node-js-collection/modern-javascript-explained-for-dinosaurs-f695e9747b70)**

This covers pretty much everything in an easy to read way including the following topics:

1. Old skool JS
2. dependencies
3. JS *require* vs *import*
4. package.json
5. Browserify
6. Transpiling
7. CoffeeScript
8. babel
9. TypeScript
10. webpack
11. webpack dev server

If that sounds like a list of things you have either never heard of or have no clue about this article is for you.

<img src="https://miro.medium.com/max/2738/1*ee_ivxNTKgIJTjmEMC4-dg.png" style="max-width: 80%;">

So until I think of something extra to say about it I’ll leave it at that.

## Links
1. [An Absolute Beginner's Guide to Using npm](https://nodesource.com/blog/an-absolute-beginners-guide-to-using-npm/)
2. [Setting up Hugo with Webpack](https://dev.to/kazushikonosu/use-webpack-with-hugo-to-manage-assets-for-your-static-website-2172)
3. [Zero config Webpack 4](https://www.youtube.com/watch?v=_HubKvRRXaU) by A Shot of Code.

