---
title: "New CSS"
date: 2020-07-05T11:18:26+01:00
draft: true
tags: ["css"]
categories: ["tutorial", "css"]
summary: "Random CSS inc. ::marker, round, strokes for type and blend modes."
---

- [Using the pseudo class ::marker](#using-the-pseudo-class-marker)
- [Perfect fitting background tiles](#perfect-fitting-background-tiles)
- [Stroke type](#stroke-type)
- [Blend Modes](#blend-modes)
- [Links](#links)

So some notes on some recent (ish) CSS.

## Using the pseudo class ::marker

This is for bulleted lists and the marker refers to the bullet enabling you to target the bullet independently of the rest of the list item. For example:

```css
li::marker {
    color: blue;
}
```

## Perfect fitting background tiles

So `round` is a new and useful value for the background-repeat property. What does it do? Well it squashes or stretches a repeating background image so it fits perfectly into it's container. That is no chopping off part of the image at the edges. It does this by squishing or stretching the image to get a perfect fit.

Not much more to say. An example:

```css
background-image: url(images/heart.jpg);
background-repeat: round;
```

More CSS backgrounds [here](../css-backgrounds).


## Stroke type

There is a new set of properties that allow type to be stroked or filled. This is not an official w3C standard (yet) but [it seems it works](https://www.caniuse.com/#search=text-stroke) in most browsers including mobile. This means they prefixed with `-webkit-`. You don't need to use multiple declarations like `-moz-` etc.

If you want to have a stroke only you need to make the type color transparent:

```css
h1 {
    color: transparent;
    -webkit-text-stroke: 0.2em #f549c3;
    text-stroke: 0.2em #f549c3;
}
```

You could instead use:

```css
h1 {
    color: transparent;
    -webkit-text-stroke-width: 1.6px;
    -webkit-text-stroke-color: #f549c3;
    text-stroke-width: 1.6px;
    text-stroke-color: #f549c3;
}
```

Currently works in all major browsers, desktop and mobile, apart from IE and Opera Mini.

N.B. Bear in mind if you set to color to transparent nothing will show up in browsers that can't use stroke. The answer is `-webkit-text-fill-color:`. Using that to set the colour after the `color` setting for the font will override it only on the supporting browsers.

```css
h1 {
    color: black;
    -webkit-text-fill-color: white;
    -webkit-text-stroke: 0.2em #f549c3;
    text-stroke: 0.2em #f549c3;
}
```


## Blend Modes

You can now use CSS to change blend modes using 

```css
mix-blend-mode: multiply;
background-blend-mode: overlay;
```

This is slightly confusing. To blend something with it's background or underlying element use `mix-blend-mode`. 

Use `background-blend-mode` to make everything above use that blend mode with the background. Requires a background image, I've read.

These a well supported on HTML elements apart from IE. They also work on SVGs but not with Safari or mobile browsers (apart from Firefox).


## Links

1. [CSS Tricks post](https://css-tricks.com/adding-stroke-to-web-text/) on `text-stroke`