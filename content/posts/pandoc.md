---
title: "Pandoc"
date: 2021-07-25T08:35:41+01:00
draft: false
tags: [markdown, html, cli, shell]
categories: ["development", apps]
summary: "How to use Pandoc file converter"
---

So this is hopefully a brief (and unfinished) look at Pandoc, a file conversion tool, that lets you convert files from one form, like HTML, to another, say Markdown.

## Is Pandoc installed?

Open a command line tool and type:

```bash
pandoc -v
```

## Convert a file

With your terminal app go to the folder where your file is. One way Pandoc works is by *guessing* what you want from the file extensions.

So:

```bash
pandoc filename.html -o filename.md
```

This will create a new markdown file in the same directory. The `-o` switch means the *output* goes to the new file instead of just the terminal. Without this the output is just printed to the terminal window (which is useful to see what you get first).

In the above example if `filename.md` doesn't exist then Pandoc will create it.

## Convert just the contents

... and print to the screen (ie. your terminal app).

To do this use the flags `-f` which stands for *from* and `-t` which means *to*.

```bash
pandoc -f html -t markdown filename.html
```

This will convert the contents of filename.html to markdown and print them to the screen.

Using bash the output can easily be directed to a file.

```bash
pandoc -f html -t markdown filename.html > newfile.md
```

Again `newfile.md` is created in the process if it doesn't exist already.


## Workding with frontmatter

If your files contain frontmatter then Pandoc will put this all on one line which is certainly no good if you're using YAML.

For example a file with the following frontmatter..

```yaml
---
title: Impact
date: 2023-06-05T11:47:36+01:00
---
```

is turned into this..

```
\-\-- title: Impact date: 2023-06-05T11:47:36+01:00 \-\--
```

In YAML line breaks are a key part of the language so this is rendered useless in the above case. Whilst it's possible to use a find and replace to put the the hyphens on their own lines you cannot do that with multiline YAML data.



## :TLDR

1. How to preserve frontmatter in directories
2. batch processing

## Links
1. [Pandoc User Guide](https://pandoc.org/MANUAL.html) and [pandoc.org](https://pandoc.org/)