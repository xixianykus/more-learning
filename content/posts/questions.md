---
title: "Questions"
date: 2019-10-28T17:41:46Z
draft: true
tags: ["hugo", "private"]
categories: ["hugo", "tutorial", "static sites"]
summary: "Questions that need further looking into as the basis for future posts."
---

### Render Hooks in Hugo

Hugo 0.62 introduced render hooks with the new Goldmark markdown parser. These allow things previously done with shortcodes to be done without. What can be done? How does it work? See [Hugo docs info](https://gohugo.io/getting-started/configuration-markup/#markdown-render-hooks) and [Hugo 0.62 release notes](https://gohugo.io/news/0.62.0-relnotes/).

### Hugo image processing

Hugo can resize and crop images. But where do they go? How does it work? Hugo uses [Smartcrop](https://github.com/muesli/smartcrop).


### How do I create a menu system?

How to create a menu in HUGO config.toml file.

### Where does my CSS go in Hugo?

Because CSS files don't change when building a site, just like image files, these can go in the static folder. Typically there might be a static/css folder with css files in it. When using a theme look in the relevent layouts to see what has been set in the head of the document.

**If a file has draft set to true does the date change when this becomes a live/public file?**

