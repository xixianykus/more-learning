---
title: Netlify & GitLab
date: 2019-05-10
categories: ["Development"]
tags: ["git", gitlab", "netlify", "hosting", "CDN"]
draft: false
summary: Some brief notes on Netlify & GitLab
---

### Netlify

Netlify is a CDN that specialises in static sites. You can drag and drop a folder directly into the interface on their website OR link to repo on GitHub, GitLab or Bitbucket. When you push to the repo the Netlify site is updated automatically.

#### Netlify servers
The server list looks something like this though will change for each project.

```
  dns1.p01.nsone.net
  dns2.p01.nsone.net
  dns3.p01.nsone.net
  dns4.p01.nsone.net
```

## Hugo Version

To check whether Netlify or whereever is using the correct version of Hugo to build your site add `{{ hugo.Generator }}` to the head section of your document. When deployed look at the source code to see which version it used.

If it's a different version you can specify which version you want to use on the Netlify website. Look under *Build &amp; deploy* for *Environment > Environment variables*. In there create a new variable (key) called `HUGO_VERSION` and give it the value of whichever version you wish to use.

### Using a Netlify config file

You can also specify the version number in a `netlify.toml` config file in the root of your project.

Add the following to the file:

```toml
[context.production.environment]
  HUGO_VERSION = "0.70"
```

## Setting up a static site on GitLab

There are two basic ways to do this. 
1. If you have no files on your local machine
2. If you have a site already on your local machine

### No files yet method

1. On GitLab.com create a new repo.
2. On you local machine create or navigate to your project folder
3. *git init* if a new folder.
4. git clone from the server.
5. add files to the staging area (index).
6. commit these changes
7.  **git remote add origin** to

### With files on your local machine

So if you already have a site running locally:

1. On Gitlab.com create a new repo for your site. Make sure there are no files, like the README, created when you do this.
2. If your local site needs it make it a git repo: **git init**
3. Connect it to the Gitlab.com repo: **git remote add origin git@gitlab.com:username/mySite.git**
4. Add files to the staging area **git add .**
5. Commit the changes: **git commit -m "Message about commit"**
6. Upload the site to your Gitlab repo **git push -u origin master**

After that if you want to update the site just use **git push**. Depending on set up you'll probably need a password for your encryption key.

Very useful [GitLab doc](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#create-a-project-from-scratch)


## GitLab instructions

### Git global setup

```cli
git config --global user.name "Xixi Anykus"
git config --global user.email "mcsplott@inbox.lv"
```

### Create a new repository

```git
git clone git@gitlab.com:xixianykus/mySite.git
cd mySite
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Push an existing folder

```
cd existing_folder
git init
git remote add origin git@gitlab.com:xixianykus/mySite.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

### Push an existing Git repository

```
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:xixianykus/mySite.git
git push -u origin --all
git push -u origin --tags
```

## Links
1. [Chris Stayte’s Netlify vids](https://www.youtube.com/user/BossSlayer95/search?query=netlify)
2. [Hugo Cloud deployment with Netlify](https://it.knightnet.org.uk/kb/hugo/deploy-with-netlify/)
3. [Netlify’s awesome build process](https://www.thechens.org/post/netlify-awesome-build-process/) from thechens.org
