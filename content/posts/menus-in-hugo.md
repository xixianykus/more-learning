---
title: "Menus in Hugo"
date: 2020-06-05T08:24:57+01:00
draft: false
tags: ["hugo", video]
toc: true
categories: ["tutorial", "static sites"]
summary: "Video on 3 ways to create menus in Hugo"
---

You can have as many menus as you like in Hugo. A menu consists of a menu object and some template code which typically uses Hugo's `range` function to loop through the menu's entries or items.

These menu objects consist of [Hugo's default properties](https://gohugo.io/content-management/menus/#properties-front-matter) (`name`, `URL`, `weight`, `parent`, `pre`, `post`, `title` and `weight`). There is also `params` which let you add your own custom properties to the menu object.

Menu objects can be created in several different ways. You can add entries to the frontmatter of pages you want in the menu. You can use Hugo's default list of the site's section pages by setting `sectionPagesMenu` in the site's config file. And you can construct a menu in the config file.

However a menu object is created they're generally accessed in the same way using Hugo's `{{ range }}` function in a template file somewhere.

## 1. Frontmatter menu

This is for small sites. Think of a name for your menu. Here I've called it *menuname*

1. Add `menu: menuname` to the front matter of the pages you want in your menu.
2. Create a file in the `/layouts/partials/` folder. For example `nav.html`.
3. Add the following to `nav.html`

```go-html-template
<ul>
    {{ range .Site.Menus.newmenu }}

    <li><a href="{{ .URL }}">{{ .Name }}</a></li>

    {{ end }}
</ul>
```

Now each time you add `menu: menuname` to the frontmatter of a page it will appear in that menu.

Note that you use `{{ .URL }}` and not `{{ .Permalink }}`. Thats because we're looping over a menu object NOT the site's pages or subset of those pages.

The default behaviour is to display the title of the page for the menu item. If you want to change this change the frontmatter too:

The default order the menu is laid out is alphabetical. However you can completely customize this by adding weight values to the frontmatter menu objects:

```yaml
menu:
  menuname:
    weight: 10
```

The page containing the above frontmatter will appear after any pages with a weight less than 10 but before any pages whose weight is above 10 or have now weight value set.

The lower numbers will appear before higher number entries. And weighted entries will appear before pages that don't have a weight value set.

If you wish to add your section (aka list) pages you need to make sure you have a file called `_index.md` in each section. The underscore denotes a list page. You can then edit the frontmatter in these.

This way of making menus is good for small web sites with just a few pages. But for larger website the following method is recommended.

## 2. sectionPagesMenu

This second method invovles using a preconfigured a menu object in the config file. This consists of the sites section pages (top level list pages) in the site. However you can add other pages to this by using method 1 above and using the same menu name. So in the example below the name is `mainmenu`.

*video: 11m 35s*

1. In the config file add the line `sectionPagesMenu = "mainmenu"`
2. Create a partial `nav.html` with the following code:

```go-html-template
<ul>
    {{ range .Site.Menus.mainmenu }}
    <li><a href="{{ .URL }}">{{ .Name }}</a></li>
    {{ end }}
</ul>
```
3. Add this partial into your template (or another partial) using `{{ partial "nav" . }}` DON'T FORGET THAT DOT.
4. This will only add top level *section/list* pages. For any other pages add `menu: mainmen` to the page's YAML frontmatter. It won't automatically create links to *tags* and *categories*.

## 3. Config menu

A menu, or multiple menus, can be created in a sites config file. The good thing about creating a menu object here is it's easy to edit because it's all in one file rather than scattered on different pages when created in frontmatter pages.

Here's an example of a menu called *main* in toml:

```toml
[[menu.main]]
  name = "About"
  identifier = "About"
  url = "/About/"
  pre = '<svg class="icon icon-about"><use xlink:href="#icon-about"></use></svg>'
  weight = 5
[[menu.main]]
  name = "Posts"
  identifier = "posts"
  url = "/posts/"
  pre = '<svg class="icon icon-posts"><use xlink:href="#icon-posts"></use></svg>'
  weight = 10
[[menu.main]]
  name = "Links"
  identifier = "links"
  url = "/links/"
  pre = '<svg class="icon icon-links"><use xlink:href="#icon-links"></use></svg>'
  weight = 50
  ```

Note that the order of the menu items is not really important since the menu will be ordered according to weight (or alphabetically if the menu's items are not weighted).

## 4. Adding a class of active

Here's how to make a menu item have a class of `active` for the current page using the built in methods `.HasMenuCurrent` and `.IsMenuCurrent` (from the [Hugo docs](https://gohugo.io/templates/menu-templates/#example)).

```go-html-template
<ul>
    {{ $currentPage := . }}
    {{ range .Site.Menus.lazy }}
    <a class="sidebar-nav-item{{if or ($currentPage.IsMenuCurrent "lazy" .) ($currentPage.HasMenuCurrent "lazy" .) }} active{{end}}" href="{{ .URL }}" title="{{ .Title }}">{{ .Name }}</a>
    {{ end }}
</ul>
```


{{< yt E6bhmixcR5k >}}