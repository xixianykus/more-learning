---
title: Markdown
description: Some brief notes on markdown syntax
date: 2019-05-04
categories: ["Development"]
tags: ["markdown"]
draft: false
summary: "Old post on Markdown with some useful info on some of the more difficult things to remember."
---

- [Markdown or HTML ?](#markdown-or-html-)
  - [Horizontal lines](#horizontal-lines)
- [A horizontal line can be made from 3 hyphens, or underscores BUT make sure there's a line space before otherwise they will be interpreted as an h2 heading.](#a-horizontal-line-can-be-made-from-3-hyphens-or-underscores-but-make-sure-theres-a-line-space-before-otherwise-they-will-be-interpreted-as-an-h2-heading)
  - [Line breaks](#line-breaks)
  - [Paragraphs inside list items](#paragraphs-inside-list-items)
  - [Links](#links)
  - [Images](#images)
  - [Bold and Italics](#bold-and-italics)
  - [Code Blocks](#code-blocks)
  - [Tables](#tables)
  - [ID tags on headings](#id-tags-on-headings)
    - [h4 with id {#custom-id}](#h4-with-id-custom-id)
  - [Strike through](#strike-through)
  - [Task List](#task-list)
- [Links](#links-1)



## Markdown or HTML ?

See also [Markdown All in One](../markdown-all-in-one/)

This page was created in markdown. No HTML is added though one problem is using the &lt; bracket. Only the LH of these is a problem so to avoid it I’ve used &amp;lt; in it’s place.

Markdown is pretty powerful and you can use HTML tags inside and .md file too.
### Horizontal lines
A horizontal line can be made from 3 hyphens, or underscores BUT make sure there's a line space before otherwise they will be interpreted as an h2 heading.
---

### Line breaks
To create a line break ```<br>``` end a line with at least two space then a carriage return.

This line is short   
so is this one.

### Paragraphs inside list items
1. To add a paragraph to a list item

      like this all you have to do is: double return (like normal paragraph), then

      indent the start line of the paragraph by four spaces. Easy really.

      ~~Except it’s not working.~~

2. Then start the line for the next list item.

      try again.

### Links
The rule is square brackets and description first, then round brackets with the url second.

```markdown
[My Website](https://mywebsite.com/)
```

You can add a title attribute too like this:

```markdown
[My Website](https://mywebsite.com/ "The best website ever!")
```

For links you use repeatedly you can define them like this:

```markdown
[1]: http://google.com/        "Google"
[2]: http://search.yahoo.com/  "Yahoo Search"
[3]: http://search.msn.com/    "MSN Search"
```

Then when you want to add a link just write out:

```markdown
[Google][1], [MSN Search][3] etc.
```

### Images
Like links you can write them inline or use a reference.

For inline:

```markdown
![alt text](/path/to/image.jpg "title goes here")
```

The reference style is first defined like this:

```markdown
[id]: /path/to/img.jpg "Title"
```

Then used like so:

```markdown
![alt text][id]
```


### Bold and Italics

Markdown converts to **strong** and *em* tags in the html. There are several options like * or underscores _ . Used singly as the start and end of phrase produces *italics*. Used twice, ie. ** or __ will produce bold text with &lt;strong> tags.

### Code Blocks
A code block is a block of text wrapped in both a &lt;pre> tag and &gt;code> tag. To create one you simply indent the the line by 4 spaces (or one tab) and everything is rendered pre & code. For example:

```css
header p {
    text-transform: uppercase;
    color: #556;
    font-size: 140%;
    font-family: robotoBold;
    margin-top: 0; margin-bottom: 3em;
}
```

This makes it really easy to cut‘n’paste code and insert it like above.

It even works with HTML an interprets converts the angle brackets so &amp;lt; and &amp;gt;

```html
<ul>
 <li><a href="../dot.css/flex1.html">CSS.Geek on Flexbox</a></li>
 <li><a href="https://www.flexbox.io">Wes Bos Flexbox course</a></li>
 <li>something</li>
</ul>
```

You can also use these tiny little buggers: `` (which can often be found on the key above the tab key)

Three before and three after will allow code to be viewed but without the indents:

```css
.html5logo {
      position: fixed;
      width:38px;
      left: calc(661px + 50% + 1.7rem);
      top: 150px;
}
```

You can also use them inline ```<html>```.

### Tables
These can be made up of pipes and hyphens. This table:


| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |


Made up from this:

```markdown
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |
```
### ID tags on headings

#### h4 with id {#custom-id}

produced by:

```markdown
#### h4 with id {#custom-id}
```

### Strike through

Just made up of ~ 2 tilde symbols before and after:

```markdown
~~~Well that's wrong~~~
```

~~Well that's wrong~~

### Task List

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

```markdown
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
```


## Links
1. [Good starter](https://daringfireball.net/projects/markdown/) -- Comprehensive and well explained.
2. [www.markdownguide.org](https://www.markdownguide.org/cheat-sheet) with free [e-book](https://www.markdownguide.org/book)
3. [blogdown: Creating websites with R Markdown](https://bookdown.org/yihui/blogdown/) is an online book about R Markdown.  Lots of detail, as only a book can, including good info about the static site generator Hugo.
