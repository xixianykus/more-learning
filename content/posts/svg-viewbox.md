---
title: "SVG Viewbox"
date: 2019-12-26T19:25:19Z
draft: false
tags: ["svg", video]
categories: ["tutorial", "svg"]
summary: "Understanding SVG's viewport and viewBox, why they're important. With a video by Kevin Powell."
css:
- ".vp {outline: dotted 2px var(--mainFontCol); fill: #930;}"
- ".fr {float: right; margin: 0 0 1em 1em; }"
- ".sz-300 {width: 300px; height: 300px}"
- ".rz {resize: both; overflow: auto; width: min(100%, 500px); margin: 0;}"
- "h2, .cb {clear: both}"
---

<svg class="vp fr sz-300">
    <circle r="50"  cx="0" cy="0"  />
</svg>

There are 3 concepts to understand here: *viewport*, *viewbox* and *canvas*.

The canvas essentially goes on for infinity. Most of the circle here is hidden. But it's still on the *canvas*. Even if it was not visible at all in the *viewport* it would still be on the canvas somewhere.

```svg
<svg width="300" height="300">
    <circle r="50" cx="0" cy="0"   />
</svg>
```

<svg viewBox="-100 -100 500 500" class="vp fr sz-300"> <circle r="50"  cx="0" cy="0" /> </svg>
Here the position of the circle hasn't changed. It's centre is still at postion `0 0`. But the position of the viewBox has changed. It now starts at `-100 -100`. This is like sliding the *viewport* up and left over the canvas to expose a slighty different section.

```svg
<svg viewBox="-100 -100 500 500" class="vp fr sz-300">
    <circle r="50" cx="0" cy="0"   />
</svg>
```


## The viewport

This is like the browser's viewport. It's what is visible. If you add a border to your `<svg>` element that would define the boundry of the *viewport*. If you set width and height dimensions on your SVG that is defining the size of the viewport. The viewport is what we can see on the canvas.

Note that if no dimensions are given for the size of the SVG the default size, 305 x 155 pixels, is used.

## The viewBox

<svg viewBox="0 0 50 50" class="vp fr sz-300"> <circle r="50" /> </svg>
This is usually set to the same dimensions as the viewport. It controls what's inside the viewable area, that is the viewbox. Like the `rect` element the first 2 numbers are the starting coordinates and the second pair are width and height. Changing the width and height of the *viewBox* zooms the image in or out. When the viewBox width and height are greater than the viewport size (set with the width and height attributes on the `<svg>` element) this effectively zooms out the image because more of the canvas area is visible.

All this means that if the viewBox width and height are NOT the same as the `<svg>` element's width and height the image will be zoomed in or out.

If the viewBox coordinates are not set to 0 0 then the viewport has been moved over the canvas. Changing these coordinates is equivalent to panning around the canvas.

## Flexible images

<div class="rz fr"> <svg class="vp cb" viewBox="0 0 300 300" style="width:100%" height="100%"> <circle r="140" cx="150" cy="150" fill="#1e72bb" /> </svg> </div> 

For images that are a fixed size you don't need to use the `viewBox` attribute at all. However for the modern web with it's multitude of different screen sizes flexible images are a key component of responsive design. Their dimensions might be defined in percentages or `ems` meaning the size in pixels cannot be know and will vary from one device to the next. As long as the viewBox dimensions are set so that the image is neither zoomed in or out you can change the *viewport* dimensions to anything and the image will scale up or down to fill it.


Here is a flexible image with a width of 100%. You can resize it's containing `<div>` by dragging on the lower right corner of it's box.

```svg
<div style="width: 500px">
  <svg class="vp cb" viewBox="0 0 300 300" style="width:100%" height="100%">
    <circle r="140" cx="150" cy="150" fill="#1e72bb" />
  </svg>
</div>
```


Here is a ...
```svg
<svg viewBox="0 0 50 50" width="300" height="300">
    <circle r="50" />
</svg>
```
But here the they're not equal. The centre of the circle begins at 0 0, as does the `viewBox`. But the viewBox is only stretching out 50 pixels[^1] in height and width. This has the effect of making the circle appear much larger in the viewport.

 







## Video

Kevin Powell’s excellent video on the SVG *viewbox* and *viewport*.

{{< yt TBYJ2V1jAlA >}}

This is part 3 of a 5 part series on SVG



## More Videos

The other videos in this series are good too and cover the following.


### Part 1

This looks at the difference between raster and vector graphics and the advantage of SVG's. It also looks at 3 ways of adding them: using an img tag, embedding them in the HTML and storing them as a symbol and referencing them with `href` attribute. (NB. The vid says Safari doesn't support `href` without `x:link`, but Safari gained full support in 2019).

{{< yt ZJSCl6XEdP8 >}}

### Part 2

How to code you're own SVG's. It begins with simple circles using `r`, `rx` and `ry`, how to add strokes with `stroke` and `stroke-width` and goes on to create rectangles with curved corners using `rx` and `ry`, lines and polygons using `rect`, `line` and `polygon`

{{< yt 9Y4P3FvZ5bg >}}

### Part 4

THe first half is about creating SVG's using Illustrator. It show's the importance using and naming layers for later use, then goes on to exporting with `Export As`, `Presentation attributes` and `Show code` and cleaning up SVG's with [SVGOMG](https://jakearchibald.github.io/svgomg/)

[link to video on Youtube](https://youtu.be/cWh0de8IhX4)

{{< yt 9Yf4P3FvZ5bg >}}

### Part 5

The fifth and final part of this series, and by far the longest at 51 minutes, shows how to animate SVG's using the Greensocks Animation JavaScript library.


{{< yt 4XJ8yS0zpa4 >}}


[^1]: Units are not necessarily pixels here.