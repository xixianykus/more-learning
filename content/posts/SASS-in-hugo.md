---
title: "SASS in Hugo"
date: "2020-05-29"
draft: false
tags: ["hugo", "css", ]
categories: ["hugo"]
summary: "How to use SASS using Hugo's built in compiler."
---

NOTE: Current versions of Hugo used lib SASS which has not been maintained. See [here](https://learnhugo.ml/posts/sass/#lib-sass-and-dart-sass) for more details.

Hugo comes with a built in SASS compiler. To use this feature is pretty simple. You need to do the following.

1. Create folders and an .scss file for your SASS to exist in
2. Add the function to compile CSS from the SASS in the head of your document template/s
3. Add the link to the compiled CSS in the `<link>` tag.

## 1. Create folders

The default folder in the root of your project is the *assets* folder. Unlike the *static* folder which Hugo doesn't touch files in the the *assets* folder IS processed by Hugo. This is best to call this assets as that is what Hugo expects. However if you need to call it something else you can add this line to your config.toml file

 ```toml
 assetsDir = "pathto/folder"
 ```

 Next create a subfolder of *assets* to keep your SASS files in. Call it what you like but *sass* or *scss* are typical names. Finally create a SASS file in there with a bit of css so you can test this all working.

 ```css
 body {
     background: blue;
 }
 ```

 ## 2. Add the SASS function

 Next in the HTML `<head>` section of your document template/s we need to add a Hugo pipes function to compile the SASS into CSS.

 ```go-html-template
 {{ $style := resources.Get "sass/main.scss" | toCSS }}
 ```

 The first part `$style` is the name of a new function. The `:=` means we are defining what that function does. 

 Then `resources.Get` is the function to tell Hugo which resources we want. The path doesn't include the *assets* folder because Hugo knows that is the default folder where all the resources are. But we do need to tell Hugo the path to the file we intend to use: `"sass/main.scss"`. After that we use the pipe symbol (usually is the uppercase version of the back slash key on most keyboards.) The pipe is saying take this file (that is our `sass/main.scss`) and send it to this function, to the right of the pipe. And that function is `toCSS` which converts the file from scss or sass to css.

 ## 3. Add this piped CSS file to the `<link>` tag

Like so:

 ```go-html-template
 <link rel="stylesheet" href="{{ $style.Permalink }}">
 ```

 ## Add more functions easily

 Now you have that set up it's extremely easy to add more functions. 

 One useful function when using the live server is *fingerprint*. Sometimes when making changes to the CSS file the browser does not notice the changes and instead uses it's cached version of the css file. To avoid this the Hugo *fingerprint* function takes a hash of the file each time it is saved. Then it adds this hash string to the file name. The browser sees it as a new file so won't use the older cached version because it has a different name.

 To use the *fingerprint* function simple add it to the pipeline with `| fingerprint`

 ```go-html-template
 {{ $style := resources.Get "sass/main.scss" | toCSS | fingerprint }}
 ```

 Another relevant function here is `minify`

 ## Using with Netlify

 To make sure Netlify is building your site with the correct version of Hugo (that is the same version as you are using on your computer) log in to your Netlify account and on your web site settings go to the *Build &amp; Deploy* section then *Environment* and *Environment variables*. Here you set up the build version you wish to use by creating a *key* called: `HUGO_VERSION` and giving the version number as the value.

 If you want to see which version of Hugo you are using from your CLI type `hugo version`.

 It should return a line like this:

 ```bash
 Hugo Static Site Generator v0.70.0/extended windows/amd64 BuildDate: unknown
 ```

 ### Which version is Netlify using?

 It's a good idea to add the version number in a meta tag to your site templates. Simply add the line `{{ hugo.Generator }}` to the `<head>` section of your template. Then when you look at the live version on your browser simply hit `Ctrl + U` to see the source code and find out which version Netlify (or any other provider) used.

 ```html
 <meta name="generator" content="Hugo 0.54.0" />
 ```

### Setting the version in the config file

You can set the version in the config file too under the `[module]` section. However I'm unsure whether this will affect Netlify builds as this method is not described in their documentation. Here is the code for a TOML config file:

 ```toml
 [module]
  [module.hugoVersion]
    extended = true
    max = ""
    min = "0.70.0"
```

For Netlify you can create a `netlify.toml` file in the root of your project and add the following code:

```toml
[context.production.environment]
  HUGO_VERSION = "0.53"
```

