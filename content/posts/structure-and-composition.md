---
title: "Structure & Composition"
date: 2021-03-02T07:09:01Z
draft: false
tags: ["graphic design", "tips"]
categories: ["design"]
summary: "10 tips by the artist Autumn Whitehurst from the May 2003 issue of Computer Arts magazine."
---

{{< figure src="/img/bjork-autumn-whitehurst-1.jpg" caption="Bjork by Autumn Whitehurst" class="autumn">}}


New York based artist, Autumn Whitehurst describes herself as a metaphorical thinker, favouring communicative art to abstract pieces. Her imagery is well known for it's muted colours, areas of detail, and rich textures. Compostion is of utmost importance to a successful illustration, as Whitehurst explains by sharing her techniques. 

1. The **composition** is as significant as any other decision you make in giving polish to your illustrations. After all it'll dictate how your illustration is perceived. Your approach doen't need to be strictly logical, but when you're stuck as for where to place what, it helps to sit back and break down exactly what it is you're trying to achieve. My first concern is usually how I might successfully convey a particular mood.
2. Consider what will be the most important way to give your image emphasis. It helps to think about the elements of design. The chief goal is to get you're idea across, without cluttering the image with what's not going to contribute to a tight execution. If an element is unnecessary cut it out it out.
3. How will you **guide the viewer's eye** around the image from one element to the next? Once you've decided which portions of the image will be most relevant, consider the level of detail and how to use it effectively. If the illustration is suffering from too much visual clutter, I streamline my ideas until I'm down to the bare bones.
4. **Negative space** is always as important as the positive space; it can be used to give depth to other compositional elements. Like colour it will divide the foreground and background. It can also be used to create chaos or make the illustration sing.
5. **Colour** gives body to ideas. You can use it to give the image conviction and grab people's attention. Sometimes it's useful for me to look at the illustration in black and white, so that I can clearly see the contrast and nuances. You can squeeze the most out of your use of colour by considering not only the hue of your choice, but also its value and saturation, and then use them to give your work a certain pitch or tone.
6. **Mass generates weight.** Colour and negative space have their own mass, as does line weight. Consider how you'd like the ingredients of your ideas to sit and what should flow where. You can also use line weight to transform something of importance into a 'punctuation mark', and thus easily make it the focus of your image.
7. Every decision you make will contribute to the overall **balance** in the illustration. There is symmetrical and asymetrical balance. Working asymetrically is more challenging but lends you more freedom. Balance won't necessarily sedate your work, but does ensure a more satisfying finish. It also helps if you think about the proportions of elements.
8. If you're in search of **tension and movement**, consider the way you juxtapose the various parts in your image. Portions can become hunted elements, or, by placing things suggestively, you can make things glide across the page or dribble down to the bottom. It's useful to move elements around in their initial stages to get an idea or how each piece relates to another, then get up and walk away from it before returning with fresh eyes.
9. **Consider using the 'X'** - a traditional but effective method of seeing clearly how the components of your image relate to one another. Look at what you've done when it's nearly complete and connect all elements with X's of various sizes to help you see your work more objectively.
10. If your intentions are other than being excessive it saves time to be economical in your approach and not overwhelm yourself. I've found that instictive hunches are usually the freshest and then I push and pull a little here and there. Use you're programs capabilities to keep things loose and mallable. Utilize layers, masks, effects, filters and more so that if you change direction in your image it's easy to undo.



{{< figure  src="/img/autumn-whithurst-pink-bubbles.jpg" class="autumn" caption="Pink bubble image by Autumn Whitehurst">}}

![Autumn Whitehust polar bears image](/img/autumn-whitehurst-polar-bears.jpg "Polar bears image by Autumn Whitehurst")
  
    
![Autumn Whitehust smile image](/img/autumn-whitehurst-smile.jpg "Smile image by Autumn Whitehurst")


## Links

1. [Interview with Autumn Whitehurst 2019](https://www.visualcollaborative.com/autumn_whitehurst_new_york/)


<style>
ol>li {
    margin-bottom: 1em;
}
.autumn {
    width: 100%;
}
.autumn>img {
    width: 100%;
}
figcaption>p {
    margin-top: 0;
    text-align: right;
    font-style: italic;
}
img {
    max-width: 100%;
}
</style>