---
title: "Advanced Bash"
date: 2019-10-31T12:49:35Z
draft: false
categories: ["Development"]
tags: ["bash", "terminal", "shell"]
summary: "How to use sed and check character encoding. See Basic Bash for more general bash stuff."
---


**NOTE:** `sed` does not work on empty files. You also cannot insert lines where there are none. So if you try to insert a line on line 7 when the file only has 3 lines it won't work. And `sed` gives no clue that nothing happened either.*

## Using sed 

Sed is short for *stream editor*. It can write lines and more to files directly from the CLI. This is basically an editor without an interface. Everything is done from the command line.

You can use `sed` to write and extract text from files.

To write a line first type `sed -i` then open quotes. The `-i` means edit **in** place. Without this the file won't be changed.

In the quotes first use a dollar sign `$` which means write to the end of the file. Then a space and then an a. You should have `sed -i '$ a`. after that you type your text and close the quote. Finally you add filename and path if needed to direct bash where to write it.

You can add the same line to multiple files at once using wild cards:

`sed -i '$ a My new text here' post/filename.txt`

### Insert text at the beginning

Note the `1i` at the beginning of the quoted string. This tells sed to go to the first line and *insert* the text there. 

`sed -i '1iSome text to insert' filename.txt`

In this example any text on the first line will be pushed down one line, as will all following lines.

You can choose any line you want. Just change the number, `sed i '5iThis will target line 5' file.txt`.

### Multiple files

This is extremely useful and very easy. Just use wildcards:

```bash
sed -i '3iSome text for line 3' *.html
```

### S for 'substitute'

Another thing sed can do is find and replace or **Substitute** . If we wanted to substitute *this* for *that* but only the fourth occurance on each line we would use:

`sed -i 's/this/that/4' filename.txt`

There's a bunch of other stuff **sed** can do...

See the four lessons on : [yourownlinux.com](http://www.yourownlinux.com/2015/04/sed-command-in-linux-append-and-insert-lines-to-file.html)

## Character Encoding

To find the encoding of a file or files use `file -i myfile.md`. You can use wildcards and get a listing of a whole directory: `file -i content/directory/*.md`

```
**$ file -i content/dir1/*.md**
content/dir1/number-one.md:   text/plain; charset=us-ascii
content/dir1/number-three.md: text/plain; charset=us-ascii
content/dir1/number-two.md:   text/plain; charset=us-ascii
```

To change the encoding use `iconv`.


## Variables

Variables can be stored using the following syntax:

```bash
my_var='some text here'
```

And this can be recalled using:

```bash
echo $my_var
```

Variable names can contain letters (case sensitive) and underscores only. Quotes are not needed unless there are spaces.


## Links

1. [Variables](https://learn-bash.org/variables.html) from learn-bash.org.
