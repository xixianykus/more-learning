---
title: "VS Code Snippets"
date: 2023-01-03T17:09:40Z
draft: false
tags: ["VS Code"]
categories: ["apps", "development"]
summary: "Getting started with VS Code's snippets"
---


These can be set 
- globally (active in any document)
- by language use (eg Markdown, HTML, CS etc.)
- by specific project

To open the dialogue press `F1` or `Ctrl + Shft + P` and type *Configure User Snippets*.

You can choose to either create a *global* snippet, a *language based* snippet or one that works in just the current project. Choose one and you'll be taken to a JSON file


Here is my snippet for inserting a code fence for Hugo code in markdown files.

```json {linenos=inline, hl_lines=[5]}
"go-html-template": {
	"prefix": "ght",
	"body": [
		"```go-html-template",
		"$0",
		"```"
	],
	"description": "code fence for Hugo"
}
```

Note that line 5 shows the `$0` which is where the cursor will be.

The prefix is what you use to activate the snippet.

## Snippet Generator

A free [snippet generator app](https://snippet-generator.app/) is available online for quickly creating snippets.

## Browsing snippets

As above just open the snippet file for any given language. 

This won't show built in snippets but there is an extension called [Snippets Ranger](https://marketplace.visualstudio.com/items?itemName=robole.snippets-ranger).


## More snippets

See the [VS Code marketplace](https://marketplace.visualstudio.com/search?target=VSCode&category=Snippets&sortBy=Installs) for a bunch of pre-made snippets.


## Lots more..

Hopefully the above is enough to get you started but there is a lot more to snippets than just this.

See the bumper article on FreeCodeCamp: [The Definitive Guide to Snippets in VS Code](https://www.freecodecamp.org/news/definitive-guide-to-snippets-visual-studio-code/)

