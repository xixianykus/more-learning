---
title: "Add a unique class for each page"
linktitle: Unique class name
date: 2019-12-27T13:01:51Z
draft: false
tags: ["hugo", "css"]
categories: ["hugo", "css", "tutorial"]
summary: "How to add a unique class name to the body tag of every page on the site automatically."
---

Being able to target elements specific to certain pages is easily done if each page has a unique body class tag:

```html
<body class="my-unique-class-name>
```

This can be done using the `{{ .Name }}` variable which uses the name of the file before the dot and extention. However it may require some modification if you're using dashes or another symbol in your filenames.

Using dashes for spaces is useful because the file name is used as the default for the title tag and typically the h1 tag of a page thus saving some time. The dashes are converted to spaces which is good for title tags and page headings but no good for CSS class names. Here each space will 

You can add this to your template which typically might be `baseof.html`. 

```go
<body class="{{ .Name }}">
```

This will work so long as there are no hyphens in the filename.

If there are then it will generate something like this:

```html
<body class="my unique class name">
```

This page now has four different class names, not what we want at all.

The key is to use Hugo's replace function like this:

```go
<body class="{{ replace .Name " " "-" }}">
```

This tells Hugo to replace spaces in .Name with hyphens.

```html
<body class="my-unique-class-name>
```

Now you have a unique page class name that can be targetted in your global CSS file without affecting other pages.

It would be good to do the same with the folder name, eg. /posts so you could target all of your post pages with the same CSS. If anyone knows how let me know.
