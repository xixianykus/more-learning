---
title: "CSS Filters"
date: 2020-07-05T18:49:11+01:00
draft: false
tags: ["css"]
categories: ["tutorial", "static sites"]
summary: "The code for CSS filters"
---


You can add multiple filters together but adding a filter a second time doesn't work very well so it's best to clear all first. The previous values remain in the button greyed out for reference.

<form>
  <input type="text">
  <input type="submit" name="submit" id="submit" value="Use this image">
  <label for="submit"></label>
</form>

<div><img id="blend-mode-image" src="/img/graffiti-bigbird.jpg" alt="random image"></div>

<div class="button-box">  
  <button id="grayscale">grayscale(<span id="gs-value"></span>)</button>
  <button id="blur">blur(<span id="blur-value"></span>)</button>
  <button id="brightness">brightness(<span id="brightness-value"></span>)</button>
  <button id="contrast">contrast(<span id="contrast-value"></span>)</button>
  <button id="hue">hue-rotate(<span id="hue-value"></span>)</button>
  <button id="invert">invert(<span id="invert-value"></span>)</button>
  <button id="opacity">opacity(<span id="opacity-value"></span>)</button>
  <button id="saturate">saturate(<span id="saturate-value"></span>)</button>
  <button id="sepia">sepia(<span id="sepia-value"></span>)</button>
  <button id="clear">clear</button>
</div>

The basic syntax is:

```css
img {
  filter: grayscale(100%);
}
```

The possible values other than grayscale are:

- none
- blur() uses `px` scale
- brightness() &ndash; 100% is default and doesn't affect the image.
- contrast() &ndash; 100% is default and doesn't affect the image.
- drop-shadow() &ndash; see below
- grayscale() &ndash; 100% is full black and white
- hue-rotate() &ndash; uses `deg`
- invert()
- opacity()
- saturate()
- sepia()
- url() this is a link to an xml file


### Drop Shadow
The drop-shadow filter takes a list of values. The first four all take pixel (px) values. Color takes any CSS color value.

N.B. The first two are obligatory.

1. horizontal offset (px)
2. vertical offset (px)
3. blur (px)
4. spread (px)
5. color

So for instance:

```css
img {
  filter: drop-shadow(8px 8px 10px gray);
}
```

### Percent values

Using `saturate(0%)` and `grayscale(100%)` gives the same result. However `saturate` can have values way over 100% for garish saturation.

The sepia filter goes up to 100% and may require the saturation level brought down by the saturate filter.

To use multiple filters separate each with a space:

```css
img {
    width: 100%;
    filter: contrast(280%) brightness(80%) blur(1px) saturate(0%);
}
```

### None

The value of `filter: none` is useful to reset everything from a list of filters like above, for instance in a hover state.


<script>
  let bmi = document.getElementById('blend-mode-image');

  let span = document.querySelectorAll('.button-box button');


  const grayscaleBtn = document.getElementById('grayscale');
  const gsValue = document.getElementById('gs-value');
  const blurBtn = document.getElementById('blur');
  const blValue = document.getElementById('blur-value');
  const brightnessBtn = document.getElementById('brightness');
  const brValue = document.getElementById('brightness-value');
  const contrastBtn = document.getElementById('contrast');
  const ctValue = document.getElementById('contrast-value');
  const hueBtn = document.getElementById('hue');
  const huValue = document.getElementById('hue-value');
  const invertBtn = document.getElementById('invert');
  const invValue = document.getElementById('invert-value');
  const opacityBtn = document.getElementById('opacity');
  const opValue = document.getElementById('opacity-value');
  const saturateBtn = document.getElementById('saturate');
  const satValue = document.getElementById('saturate-value');
  const sepiaBtn = document.getElementById('sepia');
  const sepValue = document.getElementById('sepia-value');
  const clearBtn = document.getElementById('clear');

grayscaleBtn.addEventListener('click', (e) => {
  let grayValue = prompt('Choose a value between 0 and 1. 1 is fully black and white');
  gsValue.textContent = grayValue;
  bmi.style.filter += `grayscale(${grayValue})`
  e.target.classList.remove('dim');  // span is a selector for all the buttons
})

blurBtn.addEventListener('click', (e) => {
  let blurValue = prompt('Choose a value between 0 and 1 and 100.');
  blValue.textContent = blurValue + 'px';
  bmi.style.filter += `blur(${blurValue}px)`;
  e.target.classList.remove('dim');  // span is a selector for all the buttons
})

brightnessBtn.addEventListener('click', (e) => {
  let brightnessValue = prompt('Choose a percentage amount where 100 is normal.');
  brValue.textContent = brightnessValue + '%';
  bmi.style.filter += `brightness(${brightnessValue}%)`
  e.target.classList.remove('dim'); 
});

contrastBtn.addEventListener('click', (e) => {
  let contrastValue = prompt('Choose a percentage amount where 100 is normal.');
  ctValue.textContent = contrastValue + '%';
  bmi.style.filter += `contrast(${contrastValue}%)`
  e.target.classList.remove('dim'); 
});

hueBtn.addEventListener('click', (e) => {
  let hueValue = prompt('Choose a percentage amount where 100 is normal.');
  huValue.textContent = hueValue + 'deg';
  bmi.style.filter += `hue-rotate(${hueValue}deg)`
  e.target.classList.remove('dim'); 
});

invertBtn.addEventListener('click', (e) => {
  let invertValue = prompt('Choose a percentage amount where 100 is fully inverted and 50 is mid grey.');
  invValue.textContent = invertValue + '%';
  bmi.style.filter += `invert(${invertValue}%)`
  e.target.classList.remove('dim'); 
});

opacityBtn.addEventListener('click', (e) => {
  let opacityValue = prompt('Choose a percentage amount where 100 is normal.');
  opValue.textContent = opacityValue + '%';
  bmi.style.filter += `opacity(${opacityValue}%)`
  e.target.classList.remove('dim'); 
});

saturateBtn.addEventListener('click', (e) => {
  let saturateValue = prompt('Choose a percentage amount where 100 is normal.');
  satValue.textContent = saturateValue + '%';
  bmi.style.filter += `saturate(${saturateValue}%)`
  e.target.classList.remove('dim'); 
});

sepiaBtn.addEventListener('click', (e) => {
  let sepiaValue = prompt('Choose a percentage amount where 100 is fully sepia.');
  sepValue.textContent = sepiaValue + '%';
  bmi.style.filter += `sepia(${sepiaValue}%)`
  e.target.classList.remove('dim'); 
});

clearBtn.addEventListener('click', (e) => {
  bmi.style = ``
  span.forEach((item) => {
  item.classList.add('dim');
  })
  
});

  // bmi.style.filter = 'grayscale(1)';

</script>

<style>
  #blend-mode-image {
    width: 100%;
  }
.button-box {
  margin-block: 1em;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(220px, 1fr));
  gap: .5em;
}
button,
input[type=submit] {
  border: none;
  padding: 0.25em 1em;
  background-color: var(--linkCol);
  color: #fff;
  font-size: 0.9em;
  border-radius: 5px;
  transition: 0.4s;
}
button:hover {
  cursor: pointer;
  background-color: crimson;
}
button#clear {
  background-color: #090;
}
.dim span {
  opacity: 50%;
}
form {
  margin-block: 1em;
  display:none;
}
  </style>