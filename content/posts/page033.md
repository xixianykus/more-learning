---
title: gulp
date: 2019-08-29
categories: ["Development"]
tags: ["build tools"]
draft: true
summary: "Just a list of links on Gulp"
---



## Links
1. [Getting Started with Gulp](https://travismaynard.com/writing/getting-started-with-gulp) good article by Travis M
2. [Codetype Gulp for Beginners](https://setcodetype.com/articles/gulp-tutorial-for-beginners/)
3. [A Beginners Guide to the Task Runner Gulp](https://andy-carter.com/blog/a-beginners-guide-to-the-task-runner-gulp) by Andy Carter
4. [Learn Gulp](https://www.tutorialspoint.com/gulp/index.htm) for Absolute beginners, Tutorials Point.
5. [Grunt is not weird and hard](https://24ways.org/2013/grunt-is-not-weird-and-hard/) although about Grunt it's a very clear and concise guide to what these things do and how they work.