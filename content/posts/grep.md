---
title: Grep
linktitle: Grep
date: 2022-03-04T08:08:34Z
categories: ["Development"]
tags: ["bash", "shell", "terminal"]
draft: false
summary: "The Bash command for searching files"
css:
- "td:first-child {color: #1E90FF;font-family: 'fira code', 'courier new', monospace; font-weight: 500; font-size: 0.8em;}"
---

This is from the Bash help `grep --help` and with only very minor editing...

**Usage:** `grep [OPTION]... PATTERNS [FILE]...`  

Searches for *patterns* (words or strings) in a file or files. For instance:  

`grep -i 'hello world' menu.html main.css`  

This will search for the string *hello world* in the two files ignoring the case.

*Patterns* can contain multiple patterns separated by newlines.  
  
### Pattern selection and interpretation

| Options               | action                                        |
| :-------------------- | :-------------------------------------------- |
| -E, --extended-regexp | PATTERNS are extended regular expressions     |
| -F, --fixed-strings   | PATTERNS are strings                          |
| -G, --basic-regexp    | PATTERNS are basic regular expressions        |
| -P, --perl-regexp     | PATTERNS are Perl regular expressions         |
| -e, --regexp=PATTERNS | use PATTERNS for matching                     |
| -f, --file=FILE       | take PATTERNS from FILE                       |
| -i, --ignore-case     | ignore case distinctions in patterns and data |
| --no-ignore-case      | do not ignore case distinctions (default)     |
| -w, --word-regexp     | match only whole words                        |
| -x, --line-regexp     | match only whole lines                        |
| -z, --null-data       | a data line ends in 0 byte, not newline       |
  
### Miscellaneous

| Options            | action                               |
| :----------------- | :----------------------------------- |
| -s, --no-messages  | suppress error messages              |
| -v, --invert-match | select non-matching lines            |
| -V, --version      | display version information and exit |
| --help             | display this help text and exit      |
  
### Output control  

| Options                     | action                                             |
| :-------------------------- | :------------------------------------------------- |
| -m, --max-count=NUM         | stop after NUM selected lines                      |
| -b, --byte-offset           | print the byte offset with output lines            |
| -n, --line-number           | print line number with output lines                |
| --line-buffered             | flush output on every line                         |
| -H, --with-filename         | print file name with output lines                  |
| -h, --no-filename           | suppress the file name prefix on output            |
| --label=LABEL               | use LABEL as the standard input file name prefix   |
| -o, --only-matching         | show only nonempty parts of lines that match       |
| -q, --quiet, --silent       | suppress all normal output                         |
| --binary-files=TYPE         | assume that binary files are TYPE;                 |
|                             | TYPE is 'binary', 'text', or 'without-match'       |
| -a, --text                  | equivalent to --binary-files=text                  |
| -I                          | equivalent to --binary-files=without-match         |
| -d, --directories=ACTION    | how to handle directories;                         |
|                             | ACTION is 'read', 'recurse', or 'skip'             |
| -D, --devices=ACTION        | how to handle devices, FIFOs and sockets;          |
|                             | ACTION is 'read' or 'skip'                         |
| -r, --recursive             | like --directories=recurse                         |
| -R, --dereference-recursive | likewise, but follow all symlinks                  |
| --include=GLOB              | search only files that match GLOB (a file pattern) |
| --exclude=GLOB              | skip files that match GLOB                         |
| --exclude-from=FILE         | skip files that match any file pattern from FILE   |
| --exclude-dir=GLOB          | skip directories that match GLOB                   |
| -L, --files-without-match   | print only names of FILEs with no selected lines   |
| -l, --files-with-matches    | print only names of FILEs with selected lines      |
| -c, --count                 | print only a count of selected lines per FILE      |
| -T, --initial-tab           | make tabs line up (if needed)                      |
| -Z, --null                  | print 0 byte after FILE name                       |
  
### Context control

| Options                  | action                                            |
| :----------------------- | :------------------------------------------------ |
| -B, --before-context=NUM | print NUM lines of leading context                |
| -A, --after-context=NUM  | print NUM lines of trailing context               |
| -C, --context=NUM        | print NUM lines of output context                 |
| -NUM                     | same as --context=NUM                             |
| --color[=WHEN],          |                                                   |
| --colour[=WHEN]          | use markers to highlight the matching strings;    |
|                          | WHEN is 'always', 'never', or 'auto'              |
| -U, --binary             | do not strip CR characters at EOL (MSDOS/Windows) |
  
When FILE is '-', read standard input.  With no FILE, read '.' if  
recursive, '-' otherwise.  With fewer than two FILEs, assume -h.  
Exit status is 0 if any line is selected, 1 otherwise;  
if any error occurs and -q is not given, the exit status is 2.  
  

## Links

- [GNU grep home page](https://www.gnu.org/software/grep/)
- [General help using GNU software](https://www.gnu.org/gethelp/)
