---
title: Adobe Illustrator
date: 2019-05-17
categories: ["Vector", "Design"]
tags: ["Adobe", "Illustrator"]
draft: false
summary: "Brief reminders for Adobe Illustrator inc. Artboards, Isolation mode, Transforms, gradient annotator etc."
---

## Artboards

1. Think of them as bits of paper
2. They can be different sizes and shapes (though always rectangles)
3. You can have up to 100 of them
4. Shortcut edit artboards:
   1.  ```Ctrl + Alt + 0``` to show all artboards
   2.  ```Shft + o``` to edit / rearrange
   3.  ```Esc``` to exit artboard editing mode
5. You can also use the *Artboards panel* below the layers icon to rename, add, delete artboards.
6. To shift from one artboard to the next: ```Shft + PgUp``` or ```Shft + PgDn```



## SHORT CUTS

1. ```,``` (comma) and ```.``` (period) are fill colour or apply the current gradient gradient. They also bring up the colour and gradient palettes.
2. ```/``` will remove any fill so it's transparent
3. ```Ctrl + Shft + o``` create outlines from text
4. ```Ctrl + Sheft + f9``` toggles the pathfinder palette.
5. ```Ctrl + 2``` lock selected objects
6. ```Ctrl + 3``` hide selected objects
7. ```Ctrl + 5``` make guides and lock guides
8. ```Ctrl + G``` and ```Ctrl + Shft + G``` Group and ungroup
9. ```Ctrl + Alt + B``` creates a blend between 2 objects selected. Double click on the blend tool to adjust the number of blends.
10. ```Ctrl + Shft + B``` make outlines.
11. 

### Tools
1. ```V and A``` : selection and direct selection tools
2. ```Shft + V``` ```is the Perspective moVe tool (aka perspcective selection tool)
3. ```Ctrl + Shft + i``` toggles the perspective grid. The perspective grid tool is accessed with Perspective Grid tool
4. ```P``` is Pen tool. ```P``` then ```-``` makes the delete anchor point tool
5. ```T``` for text and ```Shft + T``` for the Touch Type Tool
6. ```Shft + w``` for the stroke Width tool (```W``` is for the blend tool)
7. ```E``` is transform
8. ```O``` is reflect - ‘o’ because if bisected by a line it's the same on all sides.

## ISOLATION MODE

This is a great way to edit a specific object without having to lock other objects down.

1. Double click on an object to enter isolation
2. Double click outside the object to exit isolation mode
3. You can also see where you are by the grey bar below the tool menu.



## Transforming

### Transform Again

**Ctrl + D** is Transform again: repeats last transform. VERY useful, especially combined with *copy*.

1. To open a dialogue box:
   1. select object
   2. choose tool eg, 'R' for rotate.
   3. press enter
2. To copy from the dialogue box ```Alt + Enter```

### TRANSFORM EACH
1. **Ctrl + Shft + Alt + D** to open dialoge box.
2. Select 'random' to mix things up a bit.

3. Select one instance of what you want to transform by going to select > same > symbol instance (or most appropriate).

### ROTATING

1. Select object
2. press R for rotate
3. if you want the rotate point somewhere other than the centre click it first, then drag your object around
4. To open the dialogue box press R for rotate then hit enter or double click the rotate tool.
5. **IMPORTANT** If you want the dialogue box but without the centre point set just *hold down Alt* and then click where you want your centre of rotation.   
   
   This works in other transforms like scale (s), reflect (o).

### REFLECTING
1. Shortcut is O - ‘o’ because if bisected by a line it's the same on all sides.

## GRADIENTS

### Gradient Annotator

1. Only works on a gradient or fill
2. Simply press **g**
3. Works on linear or radial gradients. More sophisticated on radials though.


## Cutting and erasing

The **eraser** will erase anything it touches forming new paths on either side. If you want to erase just one object select it first.

The **scissors** cuts paths when you click on a path.

The **knife** only works on closed paths. If a path is open it will be converted to a closed path when using the knife. The knife is used for cutting out blocks from a shape. Click and drag through a shape and you have two shapes.



## Links
1. [A Practical Guide To SVG And Design Tools](https://www.smashingmagazine.com/2019/05/svg-design-tools-practical-guide/)
2. [Flat colours](https://flatuicolors.com/) copy into a file for instant access to a flat colour scheme.
3. [Laura Coyle](https://www.youtube.com/channel/UCJvIT9BmwPyqfPahsMMfN6g)
4. [Jeremy Mura](https://www.youtube.com/channel/UCsKxAGJpOi-tsJK18MQ9Fbg) - Youtube - Graphic designer from Austrlia / logo, branding, graphic design, freelancing
5. [Tutvid](https://tutvid.com/) - graphic designer, nice and fast and to the point
6. [Brad Colbow](https://www.youtube.com/user/thebradcolbow/search?query=illustrator) - lots of stuff not just Illustrator. He is an illustrator / artist. (eg. How to pick colours/ contrast)
7. [Teela Cunningham](https://www.youtube.com/channel/UC7LfMHlnCxEJWZ-m5BN7_ig)
8. [Yes I'm a Designer](https://www.youtube.com/channel/UCT_of6HCtVZFpnnnLUeAGYA) - good and up to date Youtube channel on Illlustrator and design generally.
9. [Spoon Graphics](https://blog.spoongraphics.co.uk/ - comprehensive site with tutorials/free stuff/ articles etc.
10. [Graphic Extras](https://www.youtube.com/channel/UCjEw2WIz2dp1Y_z9_LJ2-aQ}) - up to date, Ilustrator, Photoshop, Affinity designer [graphicxtras.com](graphicxtras.com)
11. [T&T Tutorials](https://www.youtube.com/channel/UCQzyK-fyhqUKUUgY3S50gpw) - Good vector dedicated channel w vids from 4m to 50m. Did a good 42m vid about 3D (Memphis design)
12. [Yes I'm a Designer](https://www.youtube.com/channel/UCT_of6HCtVZFpnnnLUeAGYA) - Illustrator, PS, Indesign, general illustration, quick tips.
13. [Ste Bradbury](https://www.youtube.com/channel/UCVvcyoY6dBJA2jasVk6lHtw) - Illustrator, PS, Typography





