---
title: "Base Templates"
date: 2019-10-31T01:37:43Z
draft: true
categories: ["Development", "hugo"]
tags: ["hugo", "templates"]
summary: "A brief look at Hugo’s base templates, partials and blocks"

---

A base template is an overarching template for your entire web site. The name and location is: `layouts/_default/baseof.html`.

Typically they will contain the `<!DOCTYPE html>`, `<html>`, `<head>` and `<body>` tags.

Here is an example. In this case the `<head>` section is stored in a different file, `layouts/partials/head.html` and pulled in using `{{ partial "head" }}`.

```go-html-template
<!DOCTYPE html>
<html lang="en">
    {{ partial "head" . }}
<body>
	{{ partial "header" . }}
	{{ block "main" . }}{{ end }}
	{{ partial "footer" . }}
</body>
</html>
```

Whereas a *partial* simply pulls in code from an HTML file a *block* is a little different. This too is stored in an html file but this time it is in one of the special page templates, typically a list or section page (`list.html`), a single page (`single.html`) and the home page (`index.html`). In each case the code will likely be different.

```go
{{ define "main" }}
    <-- Some HTML code -->
{{ end }}
```

Note the word *main* used here is typically used in Hugo but can actually be anything like. Whatever word you use just make sure it matches that used in your page template file (eg. list.html, single.html or index.html).

So whereas a *partial* is the same code in every instance using a *block* means that particular piece of code is different

