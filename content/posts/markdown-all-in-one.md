---
title: Markdown All in One
date: 2019-05-13
categories: ["Development"]
tags: ["markdown", "vs code"]
draft: false
summary: "How to use the wonderful VS Code extention for markdown editing."
---

- [Commands](#commands)
- [Keyboard shortcuts](#keyboard-shortcuts)
- [Tables](#tables)
- [Images](#images)
- [Settings](#settings)
- [Links](#links)
- [Conclusion](#conclusion)
- [Links](#links-1)
  

---

Markdown All in One is a great extension for VS Code.

## Commands
To access these in VS Code use ```Ctrl + Shft + P``` then type:

- Markdown: Create Table of Contents
- Markdown: Update Table of Contents
- Markdown: Toggle code span
- Markdown: Print current document to HTML
- Markdown: Toggle math environment
- Markdown: Toggle unordered list

## Keyboard shortcuts

- ```Ctrl + B``` - Toggle bold, similar to word processors. If the cursor is somewhere in the word it will make that word bold.
- ```Ctrl + I``` - Toggle italic
- ```Ctrl + Shift + ]``` - Toggle heading (uplevel)
- ```Ctrl + Shift + [``` - Toggle heading (downlevel)
- ```Ctrl + M``` - Toggle math environment
- ```Alt + C``` - Check/Uncheck task list item
- ```Ctrl + Shift + V``` - Toggle preview
- ```Ctrl + K V``` - Toggle preview to side

## Tables

You can tidy up table formatting very quickly by simply pressing ```Alt + Shft + f```.

## Images
Will autocomplete the filename 

```![MyImage](My...)```

## Settings

The settings can be changed in ```File > Preferences > Settings``` (or **Ctrl + ,**), then click on **extentions** and then *Markdown All in One*

| Name                                             | Default  |                                                   Description |
| :----------------------------------------------- | :------- | ------------------------------------------------------------: |
| markdown.extension.italic.indicator              | *        |                                Use * or _ to wrap italic text |
| markdown.extension.list.indentationSize          | adaptive | Use different indentation size for ordered and unordered list |
| markdown.extension.orderedList.autoRenumber      | true     |                            Auto fix list markers as you edits |
| markdown.extension.orderedList.marker            | ordered  |                  Or one: always use 1. as ordered list marker |
| markdown.extension.preview.autoShowPreviewToSide | false    |      Automatically show preview when opening a Markdown file. |
| markdown.extension.print.absoluteImgPath         | true     |                           Convert image path to absolute path |
| markdown.extension.print.imgToBase64             | false    |                Convert images to base64 when printing to HTML |
| markdown.extension.print.onFileSave              | false    |                                    Print to HTML on file save |
| markdown.extension.showExplorer                  | true     |                           Show outline view in explorer panel |
| markdown.extension.syntax.decorations            | true     |               Add decorations to strikethrough and code spans |
| markdown.extension.syntax.plainTheme             | false    |                                      A distraction-free theme |
| markdown.extension.toc.githubCompatibility       | false    |                                          GitHub compatibility |
| markdown.extension.toc.levels                    | 1..6	  |  Control the heading levels to show in the table of contents. |
| markdown.extension.toc.orderedList               | false    |                    Use ordered list in the table of contents. |
| markdown.extension.toc.plaintext                 | false    |                                              Just plain text. |
| markdown.extension.toc.tabSize                   | auto     |        Control the indentation size of TOC (auto or a number) |
| markdown.extension.toc.unorderedList.marker      | -        |   Use -, * or + in the table of contents (for unordered list) |
| markdown.extension.toc.updateOnSave              | true     |           Automatically update the table of contents on save. |

## Links

To add a hyperlink first copy the link to the clipboard. Then select the word or phrase you wish to use for the link. Press ```Ctrl + V```. Done!


## Conclusion

Markdown All in One works really, really well. Clearly not to do w my install. Maybe just certain files?

## Links
1. [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)