---
title: "Sean Tucker Contrast"
date: 2020-05-10T16:00:42+01:00
draft: false
tags: ["photoshop", video]
categories: ["photoshop"]
summary: "Sean Tucker show 5 ways to increase contrast in PS"
---

Photographer Sean Tucker adds contrast to his images using 5 methods...

This is for colour images....

1. **Extra fine detail**. First he uses a high pass layer. Get the strength so you can see some black outlines, not white. Change to overlay mode. Reduce opacity if needed.
2. **Fine edge contrast** layer:
   1. Add a black and white layer. For skin he lowers the red and ups the yellow. Colours will vary depending on your image. It will probably look unnatural but that's OK. (He actually stamps a new merged layer and add black and white from the menu options rather than an adjustment layer.)
   2. Change blend mode to soft light.
   3. Reduce opacity to zero and work back up. Typically he sets this at 25 -35% opacity. 
3. Add a colour balance layer.
   1. In the shadows increase blue and cyan
   2. In the midtones add yellow and red
4. Add a curves layer in luminence mode. Adjust 3 dots on the grid intersection for lows, mids and highs. Check the transition between the different tones.
5. Add a levels layer. This layer is for checking the contrast.
   1. Hold down alt and increase the slider values to see when things blow out.

For black and white images he does the same but adds and extra black and white adjustment layer above the first one.

{{< yt 21XE799yxHo >}}