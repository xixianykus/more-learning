---
title: GitLab
date: 2019-07-15
draft: true
categories: ["Development"]
tags: ["gitlab", "git"]
summary: "Setting up on GitLab git repository"
---

## Some things to remember about GitLab

When setting up you might want to use GitBash for ssh commands

Here is a good overview:

https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

Also:  
http://stackoverflow.com/questions/30451098/ddg#30454016

[Gitlab SSH ReadME](https://gitlab.com/help/ssh/README)

Start by creating a repo on GitLab. Then you can get detail of your url to use

You have two keys both with the same password: *mypassword* and both with the same title: mcsplott@inbox.lv

The second, later one has this fingerprint:  <!-- f1:ee:cc:78:39:0e:fc:dc:c6:87:41:e6:33:72:e9:65 -->

The address to use for ssh was: git@gitlab.com:xixianykus/testproj.git

Lots of info including specific git commands on your projects page: https://gitlab.com/xixianykus/testproj

Some details from that page:

```git
git config --global user.name "Xixi Anykus"
git config --global user.email "mcsplott@inbox.lv"
```

### To create a new repository:

```git
git clone git@gitlab.com:xixianykus/testproj.git
cd testproj
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### To push an existing folder:

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:xixianykus/testproj.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

## Links
1. [Empty](https://xixianykus.gitlab.io/empty/page2.html)