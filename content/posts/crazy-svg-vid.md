---
title: "Crazy SVG Vid"
date: 2019-11-06T08:53:07Z
draft: false
tags: ["svg", "video"]
categories: ["SVG", video]
summary: "Amusing animated video on SVG by Heydon Pickering"
---

Great introductory video on inline SVG that's as funny as it is informative by talented British designer/developer [Heydon Pickering](http://www.heydonworks.com/). This is part of a [series](https://www.youtube.com/playlist?list=PL2sukhHU1gzbJgEodn1haQ2HtfA_rdoge) called *Making Future Interfaces*

{{< yt 1CDTw_UpQoQ >}}