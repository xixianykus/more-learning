---
title: jQuery
date: 2019-08-21
categories: ["Development"]
tags: ["javascript"]
draft: true
summary: "Brief, unfinished intro to Jquery"
---

- [Event handlers](#event-handlers)
- [Links](#links)

### What it is

JQuery is a way to use JavaScript in an easier fashion. It is what we refer to as a framework—you’re not writing JavaScript directly, you are using a tool that makes JavaScript easier to implement. JavaScript is a complete scripting language; however, JQuery is an application programming interface or API.  There is a difference between writing straight JavaScript by hand and JQuery. You can think of JQuery as a library that provides resources to help with JavaScript coding. The JQuery code interfaces with JavaScript and allows you to write entirely different and easier syntax to do the same thing.
[source](https://coderseye.com/tutorials/jquery/) 

### The Code
For every time you need to write:

```js
$(document.ready)(function() {

});
```

What this does is to call jQuery with the dollar sign and then say wait until the document has loaded before running the script. Put this inside ```<script>``` tags and put the code between the two lines above.

The shorthand way (that most people use) omits ```(document.ready)```:
```js
$(function() {


});
```

You can’t use the hyphen in jQuery because it means minus as in ```font-weight```

If you need a hyphen use quotes, single or double, to surround it:

```js
$(function()) {
    ("#mainpage").css({color: 'red', 'font-weight':'bold'})
});
```

The above code is like CSS. The first bit ```('#mainpage')``` is the selector and the second bit is the method, in this case the css method.

You can select using all/most of the css selection methods from class and id to all the other pseudo elements etc..

## Event handlers
The *click* is an event handler
```js
$("label").click(function(){
  $("p.lorem").css({
    fontWeight: 'bold',
    animation:'strobe .1s 100'
  });

});
```        

Other methods to attach event handlers are:
- dblclick() which means double click
- hover()
- mouseenter()
- mouseleave()
  
Keyboard events
- keypress() press any key excluding special keys like ```Alt, Ctrl, Shft, Esc``` etc.
- keydown() press any key
- keyup() when a key is released.
  
Form events include:
- change()
- focus()
- blur()
- submit()

There are document/window methods
- ready()
- resize()
- scroll()


## Links
1. [Tutorial Republic](https://www.tutorialrepublic.com/jquery-tutorial/)