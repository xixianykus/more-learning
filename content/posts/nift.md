---
title: "Nift"
date: 2019-11-06T23:42:47Z
draft: true
tags: ["static site generators"]
categories: ["tutorial", "static sites"]
summary: "A page for notes on Nift"
---

So this is a new generator that there doesn't appear to be much information on right now. Claims to be very fast, even faster than Hugo.

![Nift](../../img/nift.png)

