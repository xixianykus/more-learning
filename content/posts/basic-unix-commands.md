---
title: Basic Bash for Beginners
linktitle: Basic Bash
date: 2019-05-15
categories: ["Development"]
tags: ["bash", "shell", "terminal"]
draft: false
summary: "Basic bash commands in the terminal."
css:
- ".language-txt {line-height: 1.2}"
---


- [](#)
- [Changing drive](#changing-drive)
- [Navigating](#navigating)
- [See a tree view](#see-a-tree-view)
  - [Windows tree](#windows-tree)
  - [Using Linux tree](#using-linux-tree)
- [Create and delete files and directories](#create-and-delete-files-and-directories)
- [Copying and moving](#copying-and-moving)
- [Copying and pasting from the clipboard](#copying-and-pasting-from-the-clipboard)
- [File contents](#file-contents)
- [Editing multiple files](#editing-multiple-files)
- [Aliases](#aliases)
- [Editing files](#editing-files)
- [More stuff](#more-stuff)
  - [File Compression](#file-compression)
- [Directories](#directories)
- [Finding things](#finding-things)
- [About your (electronic) self](#about-your-electronic-self)
- [Your PC](#your-pc)
- [Miscellaneous tools](#miscellaneous-tools)
- [Links](#links)
[Origing doc](http://mally.stanford.edu/~sr/computing/basic-unix.html)

Note: This page is an edit of a page on UNIX commands. Most of the commands work with git-bash or Linux under Windows Subsystem for Linux.

## Shell shortcuts

To clear the screen you can type `clear` though a quicker method is simply `Ctrl + L`.

If you make a typo, the easiest thing to do is hit `Ctrl + U` to cancel the whole line. 

If you want to stop a running process use `Ctrl + C` to kill it. If that doesn't work try `Ctrl + D`.

To close the shell use `Ctrl + D` instead of typing `exit` or `logout`.

Use the up and down arrow keys to navigate through previous commands.

**N.B. Bash is case-sensitive.**

## Changing drive
This is for Bash in Windows:

```cd /d/my-folder``` will change to the *d:* drive and the *my-folder* directory.

When typing in a command you can press ```TAB``` to have it autocompleted.

## Navigating

In BASH two of the primary navigation commands are `ls` to list the contents of a directory and `cd` to change directory.

In bash some shortcuts to remember are: 

| Command   | What it does                                                                                                                                                                                         |
| :-------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `ls`      | lists your files in the current directory.                                                                                                                                                           |
| `ls -l`   | lists your files in 'long format', which contains lots of useful information, e.g. the exact size of the file, who owns the file and who has the right to look at it, and when it was last modified. |
| `ls -a`   | lists all files, including hidden files, ie. those whose filenames begin in a dot.                                                                                                                   |
| `ls -R`   | lists *Recursively* so sub directories as well.                                                                                                                                                      |
| `ls -1`   | just one item per line.                                                                                                                                                                              |
| `cd`      | is change directory. You need a space immediately after `cd` so `cd ..` NOT `cd..`. You can also change drives: `cd /d/pictures/` switches to the `d` drive then the pictures directory.             |
| `cd /`    | will take you to the home directory of bash.                                                                                                                                                         |
| `start .` | (in Windows or `open .` for Linux). Opens the file explorer in the current directory.                                                                                                                |
| `pwd`     | *Print Working Directory* tells you where are now. Not needed if already written into the prompt.                                                                                                    |

There are a number of useful shortcuts to use with Bash commands:

| Shortcut | What it does                                                                                  |
| :------- | :-------------------------------------------------------------------------------------------- |
| `.`      | the current directory, often not needed. `ls` is the same as `ls .`                           |
| `..`     | the parent directory                                                                          |
| `~`      | the home directory                                                                            |
| `-`      | the previous directory (useful to switch back and forth between two directoriers using `cd _` |
| `./`     | Sometimes required if you need to specify a directory. Means the current directory.           |

## See a tree view

`tree` is a small program that will show you a tree view of folders, subfolders and files either from your current postion or any path. 

However this is not available if using git-bash for Windows. However there are two ways you can use it in git-bash: download the Linux version or use the Windows version.

### Windows tree

You can use Windows `tree` command like this:

```bash
cmd //c tree
```

You can then add this line to your `.bashrc` file to create an alias so you only have to type `tree`:

```bash
alias tree='cmd //c tree'
```

Here is the results from this web site's themes directory.

```txt
T:.
themes
└── blank
    ├── archetypes
    ├── images
    ├── layouts
    │   ├── _default
    │   └── partials
    └── static
        └── css
```


In Windows this app can takes two switches. One of these is `/F` which means files will be listed. But these don't work in git-bash. So you may prefer...

### Using Linux tree

There is a port of the Linux [tree for Windows](http://gnuwin32.sourceforge.net/packages/tree.htm). If you download this and add `tree.exe` to the `c:\Program Files\Git\usr\bin` folder you can use this version of `tree` from git-bash.

This has a ton of options. Type `tree --help` to see them all.

Some I like are `tree -A -C`. The `-A` uses an Ainsi hack which prints lines without gaps and the `-C` add colour and bold fonts to folders making the easier to separate from files.

`-d` means only directories are listed  
`-L 2` will only pick up the first two levels of sub directories.


## Create and delete files and directories
- `touch file1.txt file2.txt file3.txt` will create 3 such files.
- `touch file{01..50}.txt` creates 50 files from `file01.txt` to `file50.txt`. 
  - This is called a *brace expansion*.
  - Make sure there are NO SPACES in the curly *braces*.
  - You can experiment using `echo` instead of `touch` which just writes out to your terminal.
  - This works with letters too: `touch file-{e..z}.html` will create 22 new files starting with `file-e.html` (keep the case consistent, either capitals or lower case.)
  - You can use it with a comma seperated list to save some time: `echo about_{me,you,them,us,we}.html`
  - You can use both together `echo {a..k}{5..9}.txt` will start with files `a5.txt a6.txt a7.txt` ... up to `k9.txt`
  - You can nest: `echo {{A..D},{a..e}}` produces `A B C D a b c d e`
  - You can increment your list. Say you want all the even numbers from 220 to 260. `echo {220..260..2}`. This works with letters too: `echo {c..z..3}` gives *c f i l o r u x*.
  - More [use cases and examples](https://wiki.bash-hackers.org/syntax/expansion/brace#common_use_and_examples) including combining with `for` loops and `printf`.
- `mkdir` &ndash; is used for creating a directory
- `rm` &ndash; will delete or *remove* a file, but not a directory.
- `rmdir` &ndash; will remove a directory ONLY if it is empty.
- `rm -r` &ndash; the `-r` switch stands for *recursive* and will remove a directory and its contents.
- `rm -rf` &ndash; will even remove hidden .git sub directories.

## Copying and moving
- `cp` &ndash; is for copy: `cp sourcefile.txt otherDir`
- `cp -R` &ndash; to copy an entire directory and its contents.
- `mv` &ndash; is for move OR rename
- `cp` &ndash; is for copy

## Copying and pasting from the clipboard

To copy to the clipboard:

- `cat path.md > /dev/clipboard`

And to paste:

- `cat /dev/clipboard > myfile.md`

In Windows and using git bash for Windows using the Windows command will copy to the Windows clipboard:

```bash
echo "wormboards" | clip
```

In WSL just add `.exe` to `clip`

```bash
echo "wormboards" | clip.exe
```

And to read from the clipboard use:

```bash
powershell.exe -command "Get-Clipboard"
```

## Display file contents

- `cat file.txt` or concatenate, will display the contents of the file

## less

The `less` command is very useful particularly for printing out anything longer than a few lines.

Typing `less file.txt` will only display the file contents to fill the size of the terminal window but in a kind of overlay that is scrollable and searchable. To see lower you can either press *enter*, scroll down with the arrow keys, use `PgUp` and `PgDn` keys or even use a mouse wheel. To exit from the *less* screen just press `q` and you return to the prompt without all the data in the terminal window. 

You can *pipe* the contents of any command that will print to the screen to `less` too. For instance to view a bash help file on the list command: `ls --help | less`.

To seach the contents of a `less` screen just press `/` followed by a word or string you're looking for. This searches forwards and highlights lines containing the string. To search backwards use `?` instead.

To see just the lines with the highlighted string use `&` to start the search.

Find out [more about less](https://linuxhandbook.com/search-less-command/).

> However, for serious searches inside file text, you should rely on the [grep command](https://linuxhandbook.com/grep-command-examples/).

## Editing files

There are various ways to edit files in bash but one of the simplest is using the *redirect* symbols `>` and `>>`:

- `echo "Hello World" > file.txt` will **replace** the file's contents with *Hello World*.
- To add to the end of a file use `>>`. For example `echo "beans and bread" `>>` file.txt`
- To get a list of files into a file: `ls > file.txt`. You can specify the directrory: `ls .. > file.txt`

## Editing multiple files

So if you need to edit multiple files in one go, say to add some text to the end of the file you can use the *tee* utility instead of the `>>` redirection operator.

To add the same content to the end of multiple files: `echo "your string of text" | tee -a *.md`. The `-a` means *append* and without this the file contents will be replaced.

However a better way is probably using [`sed` the stream editor](/posts/bash-commands/). 

## Variables

It's very easy to create simple variables in Bash. A variable has two parts, a *key* and a *value*. You simply type both into the command line separated by an equals sign and hit enter. You can then recall the variable at anytime by typing a dollar sign and then the key name.

```bash
key=value
```

To recall you just type `$key` although to see the variable on the command line, rather than executing it as a command, you'd use `echo $key`.

You can also store commands in variable. To do this use backticks around the command:

```bash
wd='This is the `pwd` directory."
```

Although similar to *aliases* below they can be used anywhere in a Bash command.

There is a LOT more to variables than this. There are even some preset variables. Try typing `echo $HOSTNAME` OR `echo $RANDOM`. The latter produces a random number. 

You can also create arrays (lists of variable values) using brackets like so:

```bash
arrayname=("var1" "var2" "something else")
```

Simply using `echo $arrayname` will just produce the first member of the array. To access a specific member use the position along with curlies and square brackets like so:

```bash
echo ${arrayname[2]}
```

The above will retrieve the third member of the array.

But this is drifting into programming with bash which is far from the basic commands this article is meant to be about.


## Aliases


Aliases are similar to variables but are used as shortcuts for longer commands. You don't need a dollar sign at the start but they can only be used as the start of the command line. They can be set at either the user level or the system level.

To see a list of aliases simply type `alias` and hit enter.

To see the underlying definition of an alias use  the `type` command. For example `type ls`:

```bash
ls is aliased to 'ls --color-auto'
```

The syntax to define an alias is the same for both *user* and *system* level aliases. The difference is user level aliases are typed directly on the command line and system level ones are stored in a file. In Windows (and Linux too) this is usually in the `.bashrc` file in the home directory.

The syntax is:

```bash
alias aliasname='aliascommand'
```

This will store an alias accessed with `aliasname` that will produce the stored `aliascommand`. If typed on the command line it will only exist in that particular shell while it is running.

If you need to remove it before you close the Bash shell you can simply use the `unalias` command followed by the alias's name:

```bash
unalias aliascommand
```


### System level aliases

In git-bash for Windows, as Linux, system level aliases are stored a file in the bash home directory. This file is named called `.bashrc`. The shortcut for the home directory is `~`. From Windows the folder is: `/c/users/<your-windows-user-name>/`. From Bash you can see whether the file exists with `ls -a ~`. The `-a` flag is for *all* which makes sure hidden files are shown.

To see the contents of the file just type: `cat ~/.bashrc`.


You can edit the file, add or remove aliases and save it. Then you need to close the terminal and re-open it to access any changes you have made.

Edit this with the following format: `alias gs="git status"`

Here's an example from my `.bashrc` file. Note the second to last line, `bashfile` opens the `.bashrc` file into VS Code straight from the command line for instant editing.

```bash
alias gs='git status'
alias gb='git branch'
alias gc='git checkout'
alias gcm='git commit -m'
alias gm='git merge'
alias hs='hugo server -D'
alias hn='hugo new'
alias hns='hugo new site'
alias hnk='hugo new --kind'
alias bashfile='code /c/users/admin/.bashrc'

# alias elv='npx @11ty/eleventy'
```

## Links or shortcuts in bash

### Symbolic links

These are the equivalent of shortcuts in Windows explorer. A small file links or points to another file or directory. The link is created using the `ln` command. Soft or symbolic links are created using the `-s` flag:

```bash
ln -s path/to/my/file linkname
```

The link will be stored in the current directory though you can keep it where you like. Instead of just writing the name for it write the path like so:

```bash
ln -s path/to/my/file path/to/the/linkname
```

Now the link or shortcut will exist in the `the` directory instead of the current one.

You can delete symlinks the same way as deleting any file, using the bash command `rm linkname`.

### Hard links

Besides symlinks or soft links you can create hard links. With a soft links if the file your link points to is moved the link won't work any more. With hard links you can move the file and the link still works. Hard links work by creating a copy of the file. For this reason symbolic links are usually preferred.

### Windows

In Windows using git-bash things are more complicated. Using `ln -s` appears to work but in fact it simply creates a copy of the file or folder rather than a symlink. Windows has it's own command for creating symlinks: `mklink`. This doesn't work from git-bash though. There maybe ways around this which involve using and elevated terminal (ie. with admin priviliges), or putting Windows in developer mode. This is too complicated here and needs a separate article.



## Editing files
There are several different editors usually built in: Vim, Emacs, Nano. Vim is powerful but Nano is more intuitive.

`nano index.html` will open that file in Nano. If the file doesn't exist nano will create it.

There is a list of commands at the bottom. The `^` symbol means `Ctrl`. It's a toggle switch so press again to close help. You can also use the function keys. `F1` is for help too but in VS Code it works on VS code it brings up that app’s preferences palette.

`Ctrl + o` will save the file and `Ctrl + x` will exit (if not saved will prompt to save and prompt to choose a filename to save too.)

## More stuff

There are many more options, for example to list files by size, by date, recursively etc.

- `more <filename>` &ndash; shows the first part of a file, just as much as will fit on one screen. Just hit the space bar to see more or `q` to quit. You can use `/_pattern>` to search for a pattern.
- `emacs <filename>` &ndash; is an editor that lets you create and edit a file. See the <A HREF="emacs.html">emacs page</A>.
- `mv <filename1> <filename2>` &ndash; moves a file (i.e. gives
it a different name, or moves it into a different directory (see below)
- `cp <filename1> <filename2>` &ndash; copies a file
- `rm <filename>` &ndash; removes a file. It is wise to use the option rm -i, which will ask you for confirmation before actually deleting anything. You can make this your default by making an <A HREF="alias.html">alias</A> in your .cshrc file.
- `diff <filename1> <filename2>` &ndash; compares files, and shows where they differ
- `wc <filename>` &ndash; tells you how many lines, words,
and characters there are in a file
- `chmod _options filename>` &ndash; lets you change the read, write, and execute permissions on your files. The default is that only you can look at them and change them, but you may sometimes want to change these permissions. For example, `chmod o+r filename` will make the file readable for everyone, and `chmod o-r <filename>` will make it unreadable for others again. Note that for someone to be able to actually look at the file the directories it is in need to be at least executable. See <a HREF="http://www-csli.stanford.edu/Help/.help/intro-computer/protection">help protection</a> for more details.

### File Compression

- `gzip <filename>` &ndash; compresses files, so that they take up much less space. Usually text files compress to about half their original size, but it depends very much on the size of the file and the nature of the contents. There are other tools for this purpose, too (e.g. `compress`), but gzip usually gives the highest compression rate. Gzip produces files with the ending '.gz' appended to the original filename.
- `gunzip <filename>` &ndash; uncompresses files compressed by gzip. Using `gunzip -c` (cat) will print the contents to the terminal without creating an unzipped file.
- `gzcat <filename>` &ndash; lets you look at a gzipped file without actually having to `gunzip` it (same as `gunzip -c`) though it's not part of gitbash. You can even print it directly, using `gzcat <filename> | lpr`

## Directories

Directories, like folders on a Macintosh, are used to group files together in a hierarchical structure.

- `mkdir <dirname>` &ndash; make a new directory
- `cd <dirname>` &ndash; change directory. You basically 'go' to another directory, and you will see the files in that directory when you do 'ls'. You always start out in your 'home directory', and you can get back there by typing 'cd' without arguments. 'cd ..' will get you one level up from your current position. You don't have to walk along step by step - you can make big leaps or avoid walking around by specifying <A HREF="pathnames.html">pathnames</A>.
- `pwd` &ndash; tells you where you currently are.


## Finding things

- `find` &ndash; find files anywhere on the system. This can be extremely useful if you've forgotten in which directory you put a file, but do remember the name. In fact, if you use `find -p` you don't even need the full name, just the beginning. This can also be useful for finding other things on the system, e.g. documentation. NB. This is not present with gitbash for Windows.
- `locate` works faster than `find`.
- `grep <string filename(s)>` &ndash; looks for a string inside the files. This can be useful a lot of purposes, e.g. finding the right file among many, figuring out which is the right version of something, and even  doing serious corpus work. grep comes in several varieties (`grep`, `egrep`, and `fgrep`) and has a lot of very flexible options. See [the grep page](../grep/) for more info.

### Find

The `find` command works a little differently to the `ls` command. Used on its own, with no parameters, it lists all the files in the current directory AND any sub-directories. THere are different ways to limit this. To limit the results to just certain types of files you use the `-name` switch followed by the file name fragment or type. To *find* just .txt fileS for instance use:

```bash
find . -name '*.txt'
```

Note in the above I added a dot first to specify the current directory. Although not strictly necessary in this case it could be the path to any directory so worth adding just as a reminder of the first argument. Note also the quotes surrounding the file fragment. These can be single or double quotes.

The search in the above is case sensitive. However to change this simple use `-iname` instead of `-name` for a case *insenitive* search.

You can get an inverse of files using the `-not` flag. So say you want to list all files that are not markdown files?

```bash
find . -not -name '*.md'
```





## About your (electronic) self

- `whoami` &ndash; returns your username. Sounds useless, but isn't. You may need to find out who it is who forgot to log out somewhere, and make sure *you* have logged out.
- `passwd` &ndash; lets you change your password.

## Your PC

- `ps -u <yourusername>` &ndash; lists your processes. Contains lots of information about them, including the process ID, which you need if you have to kill a process. Normally, when you have been kicked out of a dialin session or have otherwise managed to get yourself disconnected abruptly, this list will contain the processes you need to kill. Those may include the shell (tcsh or whatever you're using), and anything you were running, for example emacs or elm. Be careful not to kill your current shell - the one with the number closer to the one of the ps command you're currently running. But if it happens, don't panic. Just try again :) If you're using an X-display you may have to kill some X processes before you can start them again. These will show only when you use `ps -efl`, because they're root processes.
- `kill <PID>` &ndash; kills (ends) the processes with the ID you gave. This works only for your own processes, of course. Get the ID by using `ps`. If the process doesn't 'die' properly, use the option -9. But attempt without that option first, because it doesn't give the process a chance to finish possibly important business before dying. You may need to kill processes for example if your modem connection was interrupted and you didn't get logged out properly, which sometimes happens.
- `du <filename>` &ndash; shows the disk usage of the files and directories in <filename> (without argument the current directory is used). `du -s` gives only a total.
- `last <yourusername>` &ndash; lists your last logins. Can be a useful memory aid for when you were where, how long you've been working for. (Not available on git-bash).




## Miscellaneous tools

- `date` &ndash; shows the current date and time.
- `cal` &ndash; shows a calendar of the current month. Use e.g., 'cal 10 1995' to get that for October 95, or 'cal 1995' to get the whole year. (Does not work in git-bash.)

You can find out more about these commands by looking up their manpages:

`man <command>`   &ndash; shows you the manual page for the command chosen, eg. `man ls`. The manual page is different from the help page. It is not available on git-bash.
<P>

## Links

1. [Bash Cheatsheet](https://devhints.io/bash) is one of many fine cheatsheets.
2. [Tania Rascia’s comprehensive intro](https://www.taniarascia.com/how-to-use-the-command-line-for-apple-macos-and-linux/)
3. [Bash Academy Guide](https://guide.bash.academy/) is much more in depth
4. [Ubuntu's Command Line for Beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
5. [Git for Windows](https://gitforwindows.org/) has Bash built in.
