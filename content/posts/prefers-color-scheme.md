---
title: "Prefers Color Scheme"
date: 2019-12-27T17:03:08Z
draft: false
tags: ["css"]
categories: ["tutorial"]
summary: "Make your site use a dark or light theme depending on the OS setting."
---

This is a CSS media query that detects whether someone's OS is set in dark or light mode. It can be used to change the colour scheme of a web site appropriately.

This is different to having a button where the user can switch to a dark or light scheme. Instead this works invisibly to the user.

**NB. Currently `prefers-color-scheme` is supported in Chrome, Safari, Opera and Firefox but not Edge.**


## How to set Windows 10 to dark mode

In Windows 10 you go to `settings > personalisaton > colors`

{{< figure src="/img/darkmode.png" title="Selecting dark mode in Windows 10" >}}

From there you have a choice of `dark`, `light` or `custom`.

However if you want to test your app or website between your dark and light colour schemes Firefox has light and dark buttons in the dev tools to make switching extremely simple. These are in the bar directly below the *rules* tab which displays the CSS of the selected element.

## The CSS

Here's what the media query could look like using CSS variables:

```css
@media (prefers-color-scheme: dark) {
    :root {
        --background: #393f3f;
        --headFootBg: #71d94b;
        --mainFontCol: #e7e7e7;
        --linkCol: #1095d3;
        --codeBg: #616149;
    }
}
```

`prefers-color-scheme` can currently take 3 values:

1. dark
2. light
3. no-preference

## color-scheme mode meta tag

You can also add `color-scheme` meta tag to the `<head>` section of your pages.

This helps the browser render the background colour immediately, without having to download and parse the whole css file first.

```html
<meta name="color-scheme" content="dark light">
```

This declaration says:

> The page supports both dark and light color schemes, and the page author prefers dark.





## Links
1. [Stuff & Nonsense](https://stuffandnonsense.co.uk/blog/redesigning-your-product-and-website-for-dark-mode) article goes into more depth discussing contrast and typography.