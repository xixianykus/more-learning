---
title: Static Site Generators
linktitle: SSG's
description: All about SSGs
date: 2019-05-12
draft: true
categories: ["apps", "Development"]
tags: ["static site generators"]
summary: "The pros and cons of static site generators"
---

## What are Static Site Generators?

## Why are they becoming so popular

Some pros over typical database driven CMS's like Wordpress
1. Faster load times
2. Can be hosted via CDN often for free
3. Less chance of being hacked

## The JAMstack

> Faster, more secure and cheaper websites & apps are the main promises of the JAMstack. But in reality, the JAMstack's sexiest promise is this: a deliberate, creative, and strategic development experience for modern developers. [Shopify](https://snipcart.com/blog/headless-ecommerce-guide)

## What is a Headless CMS?

>  Unlike the holistic, monolithic approach of traditional platforms (WordPress, Shopify, etc.), decoupled solutions imply a clear separation between backend and frontend. Chopping its head right off. Thus, “headless”. &ndash; [Shopify](https://snipcart.com/blog/headless-ecommerce-guide)

## Links
* [Smashing Mag Article on switching from WP to Hugo](https://www.smashingmagazine.com/2019/05/switch-wordpress-hugo/)
* [How to choose the best SSG](https://snipcart.com/blog/choose-best-static-site-generator) - One of several good articles by Snipcart.com.
* [Building a static CMS](https://carrot.is/coding/static_cms) blog by Carrot.io
* [How we build CMS free websites](https://medium.com/devseed/how-we-build-cms-free-websites-d7e19d94a0ff) Development Seed post on the medium.com
