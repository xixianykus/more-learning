---
title: "SSH"
date: 2021-01-04T10:44:39Z
draft: true
tags: ["bash"]
categories: ["tutorial"]
summary: "How to set up SSH keys"
---

You can set up SSH using Git-Bash (a great Windows command line program that integrates Git version control with the Bash CLI). 

To generate keys use the command `ssh-keygen`.

If you use this on it's own it will ask you:

1. which file to save the key in. eg `Users/admin/.ssh/id_rsa`
2. A passphrase for the key (if you don't want one just press Enter twice)


## Links

1. [ssh-kegen at ssh.com](https://www.ssh.com/ssh/keygen/) is a comprehensive guide to using this Bash command
2. [Setting up SSH on GitLab](https://www.youtube.com/watch?v=mNtQ55quG9M) - video tutorial by Raghav Pal

