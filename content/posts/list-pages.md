---
title: "List Pages"
date: 2019-10-30T10:28:27Z
draft: false
categories: ["Development", "hugo"]
tags: ["hugo", "templates", video]
summary: "Info on Hugo’s list pages, including a video from Mike Dane."
---

There are two main types of content page. *List*, or *section* pages are the type that will list what is in a directrory.

In some cases you only need to point the url to the directory. This works if that is defined in the config file. Look for the `[[ taxonomies ]]`. If you define your list under here there is no need to create anything else. Simply create a menu item with a link to `/tags/` or `series` or whatever term you've created.

Otherwise you can create a list page in the folder you wish to be listed. These are an *index* page prefaced with an underscore: `_index.md`. Use this to override and existing list template file.

To create a new or different list file create an HTML file in `layouts/_default/` folder.

To get the content of a page use Hugo variable {{.Content}}

To get a list of a directory use `{{ range .Pages }}`

This is an opening command so it needs a close as well:
```
{{ range .Pages }}

{{ end }}
```

To get the title use `{{ .Title }}` in the middle of the range function

```
{{ range .Pages }}
{{ .Title }}
{{ end }}
```
Another variable is `{{ .URL }}` which gets the url of the page. Not so useful as if it was a link but you can put this in a link.

`<a href="{{ .URL }}">`


### Getting the Archives Page working

With the above knowledge all I had to do was: `hugo new archives/_index.md`. No content was necessary to get it working.

## The video

This post was notes taken from this video:

{{< yt 8b2YTSMdMps >}}



## Links

[Mike Dane’s Hugo course Tutorial 12](https://youtu.be/8b2YTSMdMps)

piggies
