---
title: "Static Site Generator CMS's"
linktitle: SSG CMS's
date: 2019-11-24T15:35:50Z
draft: false
tags: ["hugo", "static site generators", "CMS"]
categories: ["tutorial", "static sites", "hugo"]
summary: "Brief synopsis of different headless CMS’s and their pros and cons."
---

Just like Static Site Generators there are now a growing number of CMS services for static site.

## Netlify CMS

This seems like one of the best. The best features are:

- You can use it for free with up to 10 users.
- It has a full wsiwyg interface
- You can edit in both markdown and rich text formats
- Works with most static site generators.

The only possible downside is that I think you have to use it with Netlify. However given that Netlify is such an excellent service that's hardly a negative.


## Publii

This is different in that it is both a kind of CMS and a static site generator. The generation of the site works locally via a CMS like interface. Publishing is done via the same interface. 

Looks good though I haven't tried it. It's currently in the early stages of development at around version 0.3. I am not sure how works with multiple authors but presumably it is fine.

## Prose.io

This works by having access to your GitHub repo. It's described as an interface on top of your git repo. You can edit any kind of site but if it's in HTML then that's what you'll edit (though it does have syntax highlighting).

## Forrestry.io

This is similar to Prose in that it edits your git repo but has more sophisticated interface.


Others include Siteleaf and DatoCMS though not so attractive for free use deals.

