---
title: "Hugo archetypes"
date: 2019-10-29T20:36:57Z
draft: false
tags: ["frontmatter", "archetypes", "hugo"]
categories: ["tutorial", "static sites", "Development", "hugo"]
summary: "A brief description of the Hugo archetypes folder, files and syntax. More needed."
---

The folder archetypes holds templates for the front matter. When you create a new page via the command line `hugo new myfile.md` the generated page will have frontmatter included and the files in *archetypes* are where it gets the info for this from.

There is a built *archetype* too so if there are no archetype files Hugo will use that.

The file extension is important. Archetype files like the default one that ends in `.md` are for markdown files. If you want to use an HTML file or any other format you need to create an a file in the archetypes folder ending with the same extension. So `.html` for html files.

The standard file is default.md which covers all markdown pages. However if you want a different set of front matter for say your blog posts you simply create a new markdown file with **the same name** as the folder that your blog posts are kept in. So if you want specific frontmatter for your blogposts that are in the folder `/content/blog/` you create a file called `blog.md` in your archetypes folder and write your frontmatter into that.

The archetypes files are only used when a new file is created, and using the command `hugo new ...`. After that you can edit the content file's frontmatter as you like and it will stay that way.

The values in the archetypes files can be fixed with simple variables like: `author: "Mike"` or `tags: "hugo"` or they can include functions that grab the data from somewhere else like the `date: {{ .date }}` .

