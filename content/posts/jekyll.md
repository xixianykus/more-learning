---
title: Jekyll
description: Notes on how Jekyll works
date: 2019-03-29
categories: ["Development"]
tags: ["Jekyll", "Static Site Generators"]
draft: true
summary: "A quickstart guide to using Jekyll Static Site Generator"
---

## Quickly make a site
This isn’t as quick and easy as it could be. You can either make an themed site or an unthemed *scaffold* one or you could convert an existing static site.

### Non-Jekyll Themed site
1. If you don't want the default *minima* theme, that comes with Jekyll find one on GitHub. Copy the .git url.
2. From the CLI type <code>git clone http://github.com/url-blah.git</code> to download it.
3. From inside the folder run **bundle update** or **bundle update themename**
4. Run **jekyll s** and see if it’s working.

### Themed site
1. **jekyll new myFolderName** to create a new site with the default theme *Minima* within in the folder specified.  To change this default theme use **bundle update themename**. Unfortunately this does not work. The error message is cannot find the gemfile. Then use **bundle exec jekyll serve** (first time only) to view the site at *localhost:4000* Sounds good but hard to edit as all the stuff is in the Ruby folder.
2. To override this first find where the theme is located using **bundle show minima**. This will give the path to the file tree.
3. Copy and paste into Windows explorer to view the folder structure.
4. You can now use the same folders in your site folder and create the same files which will then override those in the ruby folder.
4. **jekyll b** or **jekyll build** to *build* the side prior to uploading. This builds the static site in the folder *_site*

### Unthemed site
2. **jekyll new --blank** creates a ‘new blank Jekyll site scaffold’. When using this option you don’t need to use *bundle exec jekyll serve* as there is no gemfile to process. Just use *jekyll serve* or even *jekyll s*.

### Theme creation
If you want to create a new theme use **jekyll new-theme**. This will create a new theme scaffold. This is for the creation of a new theme to be uploaded to a themes website.

### Jekyll b
When the site is ready to deploy use **jekyll b** or **jekyll build** to create the static site which is in the *_site* folder.

---

## Other notes
1. You *may* need to add a **gemfile** file to the project. And sometimes a *_layouts* folder too.
1. When you first serve a Jekyll project you use the <code>bundle exec jekyll serve</code> rather than just <code>jekyll serve</code>
3. The title in the frontmatter can be used elsewhere, like the h1 tag. It's written <code><h1>{ page.title }</h1></code> *except* with double curly braces. (Using double in this instance just inserts the page title in).
4. If a page needs an **embedded stylesheet** this does not need to be the *head* section of the file. Place it lower in the body instead. Not sure if this works for linking external stylesheets though.
5. <p>There are two types of ... in Liquid. *Output* is denoted by a double set of curly braces. *Tags*, which can be used for logic statements, are denoted by one curly brace and one percent sign.<br><code>&#123;&#123; This is output &#125;&#125;  &#123;% This is a tag %&#125;</code></p>
5. You can use *includes* to insert bits of HTML to existing layout files. These reside in an  *_includes* folder and are written like so:
<code>&#123;$ include header.html $&#125;</code>
6. **Filters** are added to content like this <code>&#123;&#123; page.heading | upcase &#125;&#125;</code>. A filter is denoted by the pipe symbol and the type of filter named afterwards. In this case <code>upcase</code> converts the <code>page.heading</code> text to uppercase.
7. To get Jekyll to process a file you need the yaml frontmatter. This can be empty like:
<p><code class="css">
 ---
 ---</code><br>This is also necessary in SASS files in Jekyll as Jekyll has to told to process this into normal CSS.</p>

## Links
1. [Netlify guide to Jekyll Sites](https://www.netlify.com/blog/2015/10/28/a-step-by-step-guide-jekyll-3.0-on-netlify/) on GitHub is nice a straightforward.
2. [More Julien Knight stuff](https://it.knightnet.org.uk/kb/ghjekyll/)
3. [Shopify Liquid Reference Guide](https://help.shopify.com/en/themes/liquid) - excellent reading to get the hang of *Liquid*, the templating language Jekyll uses. There is also their [full guide on GitHub](https://shopify.github.io/liquid/)
1. [Julien Knight’s list of Jekyll’s default variables](https://it.knightnet.org.uk/kb/ghjekyll/standard-attributes.html) - very useful.
3. [Jekyll docs](https://jekyllrb.com/docs/) - clear and succinct
4. [Jekyll site on GitHub pages](http://jmcglone.com/guides/github-pages/) - Great tutorial, well written and easy to follow.
5. [CloudCannon videos](https://learn.cloudcannon.com/)
5. [Mike Dane's videos](https://www.mikedane.com/static-site-generators/jekyll/)
5. [Liquid](https://shopify.github.io/liquid/basics/introduction/) is the templating engine used by Jekyll.
6. [WebJeda’s Youtube Channel](https://www.youtube.com/channel/UCbOO7d0vVo0kIrkd7m32irg/videos) - Jekyll w Disqus and other apis.
