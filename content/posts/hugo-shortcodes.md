---
title: "Hugo Shortcodes"
date: 2019-12-27T10:17:06Z
draft: false
tags: ["hugo", video]
categories: ["tutorial", "static sites", "hugo"]
summary: "Intro to Hugo’s shortcodes"
---

Shortcodes are used to add code to *markdown* files in a more brief way.

A classic example for instance is to add a Youtube video. Normally the code from Youtube is something like:

```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/ilnfF60SCss" 
    frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
    allowfullscreen>
</iframe>
```

However all this can be reduced to the simpler:

```md
< youtube ilnfF60SCss >
```

**Using the highlight shortcode** doesn't prevent it getting converted to HTML.

{{< highlight html >}}
   {{< yt ilnfF60SCss >}}
{{< /highlight>}}

 
Using astericks:

    *{{**< youtube ilnfF60SCss >*}}*

### Twitter, Vimeo and Instagram
...work the same as Youtube. Instagram has an extra optional parameter, *hidecaption*

You can also add a class name using `class="nameofclass"` and the `title="Name of video"`

Note there is also a bunch of stuff you can do in the config.toml file to [increase a viewers privacy](https://gohugo.io/about/hugo-and-gdpr/)


### The figure element
Another good one is the HTML figure element:

{{< figure src="/img/graffiti-bigbird.jpg" title="Big Bird, Sharrow" >}}

This takes the the parameters *src* and *title*:

`src="/media/spf13.jpg" title="Steve Francia"`


## Two kinds of shortcodes

## Creating shortcodes

You can create your own creating a file in the folder `/layouts/shortcodes/myshortcode.html`


## Links
1. [Create custom shortcodes](https://gohugo.io/templates/shortcode-templates/) from Hugo docs.



<style>
    .language-md {
        white-space: pre;
    }
    .language-md::before {
        content: '{{'
    }
    .language-md::after {
        content: '}}'
    }
    .language-md span {
        display:none;
    }
</style>