---
title: "Thunderbird Source Code"
linktitle: "Tbd Source Code"
date: 2019-10-30T05:57:07Z
draft: false
tags: ["thunderbird", "email"]
categories: ["other", "tips", "apps"]
summary: "How to get the source code from an email in Thunderbird"
---

## Extracting the source code from an email
Sometimes it would be good to get the source code from an email you've receieved. For instance you might want to make it into a web page for reference later.

But it’s tricky getting at the source code from an email message in Thunderbird. Maybe there’s plugin? But you can do it like this.

1. Select the message, right click and choose forward.
2. Delete the message headers.
3. Press  **Ctrl + A** to select everything that’s left
4. Go to the message’s insert menu and choose HTML to bring up another window
5. Press **Ctrl + A** to select all then copy and paste this into your page.
6. Close the email, choose *discard changes*

