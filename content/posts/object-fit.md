---
title: "Object Fit"
date: 2023-04-06T12:09:08
draft: false
categories: ["css"]
tags: ["css"]
summary: "How the CSS properties object-fit and object-position work."
---
<style>
    .wrapper {
        display: grid;
        grid-template-columns: repeat(auto-fit, 350px);
        grid-gap: 25px;
        margin: 2.5em 0;
    }

    .box {
        width: 350px;
        /*  height: calc(420px + 2rem);  */
        background-color: var(--box-bg);
        padding: 0 25px;
    }

    .standard {
        display: none;
    }
    .box img {
        width: 300px;
        height: 300px;
        outline: solid 2px fuchsia;
    }
    .box p {
    margin-top: 0;
    margin-bottom: 2em;
}

    .contain img {
        object-fit: contain;
    }

    .cover img {
        object-fit: cover;
    }

    .fill img {
        object-fit: fill;
    }

    .none img {
        object-fit: none;
    }

    .scale-down img {
        object-fit: scale-down;
    }

    .box h2 {
        margin: 1rem 0;
        font-weight: normal;
    }
    .img-dimensions {
        width: max(50%, 350px);
        height: auto;
    }
</style>

The CSS property `object-fit` was confusing for me at first. This was because I wrongly believed it had something to do with fitting an image inside it’s parent div.

THIS IS NOTHING TO DO WITH THE PARENT CONTAINING ELEMENT.

The `object-fit` property sets how an image fits into it's alloted boundries.


One use for `object-fit` is for cropping the image (or video or some other object). This is best done in combination with the `object-position` property.

Another use would be if you have a gallery of different sized images but there’s a page of equal sized thumbnails, perhaps using CSS grid. Some or all will be inevitably cropped to fit the layout. `object-fit` and `object-position`allows you to choose how to do that with the CSS.

First set the height and width of the image. Normally if these sizes are different from the original aspect ratio the image will distort as in the first example below and also the one set to “fill”.

## Examples

BELOW: The `<img>` element has a pink border and given a height and width of 300px. The actual size of the image is 500 pixels wide by 386 pixels high.

<div class="wrapper">
    <div class="box standard">
        <h2>standard</h2><img src="../../img/graffiti-bigbird.jpg" alt="standard">
    </div>
    <div class="box fill">
        <h2>fill</h2><img src="../../img/graffiti-bigbird.jpg" alt="fill">
        <p>The default. The image is distorted to fill the dimensions.</p>
    </div>
    <div class="box contain">
        <h2>contain</h2><img src="../../img/graffiti-bigbird.jpg" alt="contain">
        <p>The full image is displayed with the correct aspect ratio leaving gaps in the container if necessary.</p>
    </div>
    <div class="box cover">
        <h2>cover</h2><img src="../../img/graffiti-bigbird.jpg" alt="cover">
        <p>The correct aspect ratio is maintained but the container is completely covered. The image is cropped if needed.</p>
    </div>
    <div class="box none">
        <h2>none</h2><img src="../../img/graffiti-bigbird.jpg" alt="none">
        <p>The image is not resized at all and thus cropped if larger than the container.</p>
    </div>
    <div class="box scale-down">
        <h2>scale-down</h2><img src="../../img/graffiti-bigbird.jpg" alt="scale-down">
        <p>Chooses either <code>contain</code> or <code>none</code>, whichever produces the smaller object.</p>
    </div>
</div>

## object-position

Now looking at the above you can see that the image isn't always positioned ideally. This is what `object-position` is for.

The default position is `object-position: center center;`. That is the image is centred within the img box. However this can be changed. Take a look at the `cover` version above. The beak is cut off. Also if using `contain` or `scale down` in a group of thumbnails you might prefer to have them all aligned along their top edge for example.

The `object-position` property takes values similar to `background-position`. It can take words like: `top, right, center, left`, numerical values like `px, %, rems` etc.

```css
img {
    object-position: top right;
}
img {
    /* moves it 25px from the left edge */
    object-position: 25px;
}
img {
    /* moves it 25px from the left and 10% from the top */
    object-position: 25px 10%; 
}
```

Like `background-position` you can also offset it from an edge:

```css
img {
    object-position: right 3em bottom 10px;
    /* or ... */
    object-position: top 0 right 10px;
}
```



<h2>Using &lt;img&gt; width and height in flexible images</h2>

<p>It's now a good time to start using width and height attributes in images that are flexible (defined in their CSS with a width value in percent). The reason is that this tells the browser the aspect ratio of the image so it can paint the frame of the image immediately.</p>

<p><b>CAVEAT:</b> Make sure to use <code>height: auto;</code> in the CSS to prevent the height value of the image tag being used.</p>

```html
<img src="graffiti-bigbird.jpg" height="500" width="386">
```

and the CSS..

```css
img {
    width: 100%;
    height: auto;
}
```

<img src="../../img/graffiti-bigbird.jpg" height="500" width="386" class="img-dimensions">

<h2>Links</h2>
<ol>
    <li><a href="https://www.w3schools.com/csS/css3_object-fit.asp">W3 Schools</a> seems to have the best explanation.</li>
    <li><a href="https://alligator.io/css/cropping-images-object-fit/">Alligator.io</a> on <em>object-fit</em></li>
    <li><a href="https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit">MDN page on object-fit</a></li>
    <li><a href="https://www.youtube.com/watch?v=4-d_SoCHeWE">Improve image loading</a> by Jen Simmons on Mozilla Youtube channel.</li>
</ol>