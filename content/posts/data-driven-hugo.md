---
title: "Data Driven Hugo"
date: 2020-07-05T12:26:15+01:00
draft: false
tags: ["hugo"]
categories: ["hugo"]
summary: "Linking data files to specific pages in Hugo"
---

It is possible to run a Hugo site just from data files stored in the `/data/` folder. There's probably not much point to that unless you already have those files but regardless is how to collect data from the `/data/` folder to be placed on individual pages. Since the code to access template variables are in the templates themselves it's not obvious how to go about accessing data for just one page.

You could put everything in the frontmatter or you can add content to data files in the `/data/` folder. However doing this it's not so easy to link such content to each individual page. The way to access this content is via template variables which are in the template files. So how to link a particular data file to one specific page?

Here is one way of doing it.

First name your data file the same as your page. For `my-blog-post.md` make a corrosponding file in the data directory, `my-blog-post.yaml` for example. You can use TOML or JSON files if you prefer.

Next you access this in your page template by grabbing the file name and appending it to the call for the data file. This is not obvious at all but I found the code on the Hugo forums, tried and it works fine.

```go-html-template
{{ with (index .Site.Data.blogposts (substr $.File.LogicalName 0 -3)) }}
    <div>Synopsis: {{ .Params.synopsis }}</div>
{{ end }}
```

Above the `.Site.Data.blogposts` refers to a sub-folder of the data folder called *blogposts*.

The `index` function according to Hugo docs:

> “&hellip;returns the result of indexing its first argument by the following arguments. Each indexed item must be a map or a slice”

The syntax the docs says is:

```go
index COLLECTION KEYS
```

The next bit `(substr $.File.LogicalName 0 -3)` is extracting the first part of the file name before the dot. So starting from the beginning letter ( which is what the `0` points to) to 3 characters from the end. Those unwanted 3 characters will be the `.md`. To reference a file in the data folder you don't need to give it an extension so that's set to pull the file.

The last part is the `{{ .Params.synopsis }}`. This is a key within the yaml file. This file will typically have multiple key/value pairs (variables) and so these can then be accessed anywhere between the opening `{{ with ... }}` and it's end tag via the keyname alone.

So using this you could store everything in a data file if you wanted to. Not sure how much of an advantage that would be though since you could store it all in the frontmatter too if you wished without the need to create an additional file. But being able to access data specific to certain pages has to be a good thing to know.