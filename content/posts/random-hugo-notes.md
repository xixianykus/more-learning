---
title: "Random Notes on Hugo"
date: 2019-10-28T17:41:32Z
draft: true
categories: ["Development", "static sites", "hugo"]
tags: ["hugo"]
summary: "Just a few scribbles on Hugo."
---

## Easy mistakes to make

The command **hugo** on it's own will create the static site content into a /publish directory. One way of deploying is to simply upload content from the publish directory to a git repo.

> “After running hugo server for local web development, you need to do a final hugo run without the server part of the command to rebuild your site. You may then deploy your site by copying the public/ directory to your production web server.” - [gohugo.io](https://gohugo.io/getting-started/usage/#deploy-your-website)

When using the server use the -D for drafts if you want to see pages that won't yet be published. `hugo server -D`

Don't create a content/index.md file for the homepage. It can ruin everything. Create a `layouts/index.html` file instead. 

When downloading themes use the **git submodule** command rather than **git clone**

### baseurl should be blank

Different from what others day but Chris Stayte reckons the theme won't work if the base url is not empty. So should be: `baseurl` = ""` ([video link](https://youtu.be/hBQlCtfRmqs?t=663)). Though he is talking about uploading to GitHub, BEFORE buying a domain name.

