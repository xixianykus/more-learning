---
title: Logos
date: 2019-05-18
categories: ["Design"]
tags: ["logos"]
draft: false
summary: "A few bullet points plus a bunch of links on creating logos"
---

## Mood boards

1. Mood boards are important so that you don’t start with a blank mind.
2. Collect images from a search engine using the word ‘graphics’.
3. Books, Magazines, TV and other places can be a source of collecting ideas and images.
4. Many specialised logo websites exist:

## Websites

1. [Logo Pond](https://logopond.com/) search by title or tag. Excellent.
2. [Logo Lounge](http://logolounge.com) to search and fully explore you have to pay.
3. [Logospire](http://logospire.com)
4. [Logo Fury](http://logofury.com) is an online logo generator
5. [Logo Moose](http://logomoose.com)
6. [Logofi](http://logofi.com)
7. [Logo Sauce](http://logosauce.com)
8. [Logo Talkz](http://logotalkz.com)
9. [Logo Gallery](http://logogallery.net)
10. [Logo Treasure](http://logotreasure.com)
11. [Designer’s Couch](http://designerscouch.org)
12. [Logottica](http://logottica.com)
13. [The Logo Mix](http://thelogomix.com)
14. [Logo Faves](htpp://logofaves.com)

Also see:
1. [Brands of the World](https://www.brandsoftheworld.com/)

## Mindmapping
1. Start w the company name in the centre
2. Use this and the moodboard for ideas
3. Branch of at random having fun
4. Add doodle images too if they come to you.

## Sketching

1. The most important step
2. Don't worry if you can’t draw - great things can come from the most simple ideas.
3. Tools: pencils, light & dark, rubber, A4 paper pad, pencil sharpner
4. Sketching is 2 stages: 
   1. exploratory, rough sketchs. More about the idea/concept.
   2. choose several of the above for refinement
5. Import sketches to illustrator for tracing or reference.




## Links
1. [Will Paterson Youtube](https://www.youtube.com/user/breakdesignsco)
2. [Mohamed Achraf Youtube](https://www.youtube.com/channel/UCF6WjcZeVqy3MLBpp86eOyw)
3. [Zimri Mayfield Youtube](https://www.youtube.com/channel/UCbqd2YmFeHMwxlj4NcN5zPQ)
4. [Satori Graphics Youtube](https://www.youtube.com/channel/UCoeJKtPJLoIBqWq4o8TDLpA)