---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
tags: ["hugo"]
categories: ["tutorial", "static sites"]
---