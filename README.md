Didn't realise the baseurl played such an important role. Using the Netlify url means some pages won't load as they're using that. Interesting discovery.

Need to look into this as well as other sites. Having 2 urls can be handy but only if they work.


# Things that need fixing

1. The categories page div/h1 is higher than the others for some reason.
2. The tags list is very incomplete and only a limited set of tags displays. Yet the missing tags show up in tag specific pages. If you follow the tag for *vs code* you can find the missing *markdown* tag. There are 10 tags listed. Under categories/development the list is also 10 long. 
3. When adding a summary to the frontmatter of some pages it screws up on the list pages completely. Strangely undoing the changes does not resolve the screw up.

## New

The list.html and single.html now have class names in the <main> tag: class="single" and class="list"

Fixed using `{{ range .Pages.ByPublishDate.Reverse }}` in the list.html page. Taken from the zwbetz tut (from scratch)

Fixed:  
I now need to get the tags sorted alphabetically and just listed as list item, rather than h1 tags. SOLUTION: used {{ range .Pages.ByTitle }} in list.html instead of .Pages.ByPublishDate



FORTUNATELY I did my first commit not long before this.

**HELP** : what is the bastard path to images? Do I need to set them all to absolute urls? If so how? The CSS file link works from the template pages.

ANS: depends where you're linking form. From posts you may have to go up two levels first (up to content, then up to root) `: ../../img/name.svg`

## Previous problems

Same mistake as the other Hugo sites: added a home page, `content/index.md` which screwed everything up. Solution was to change the name to `_index.md`.

It's called universal because I think I tried the universal theme first.


## Wanted ?

All the icons to my places on the internet??

- GitHub
- GitLab
- Deviant Art
- 500px
- Stack Overflow
- CodePen
